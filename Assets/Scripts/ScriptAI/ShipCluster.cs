﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShipCluster {

	public List<SmallShipFacade> elements = new List<SmallShipFacade>();
	//public List<SmallShipClusterElement> elements =  new List<SmallShipClusterElement>() ;

	/** sum of all positions of elements */
	public Vector3 centroidTotal = Vector3.zero;

	/** centroid of cluster */
	public Vector3 centroid = Vector3.zero; 

	public Vector3 velocity = Vector3.zero;

	/** number of cluster members */
	public int numberOfElements = 0;


	public bool inUse = true;

	public Transform marker;

	public ShipCluster(){
		
	}



	public void CalcCentroid(){
		centroidTotal = Vector3.zero;
		numberOfElements = 0;
		
		Vector3 velocityTotal = Vector3.zero;

		foreach (SmallShipFacade ele in elements) {
			if( ele != null)
			{
				numberOfElements++;
				centroidTotal += ele.GetPosition();
				velocityTotal += ele.GetVelocity();
			}
		}

		if(numberOfElements > 0)
		{
			float x = centroidTotal.x / numberOfElements;
			float y = centroidTotal.y / numberOfElements;
			float z = centroidTotal.z / numberOfElements;
			centroid = new Vector3( x , y , z );

			float vx = velocityTotal.x / numberOfElements;
			float vy = velocityTotal.y / numberOfElements;
			float vz = velocityTotal.z / numberOfElements;
			velocity = new Vector3(vx, vy, vz);
		}

		if (marker != null)
			marker.position = centroid;
	}

	public Vector3 GetVelocity() {
		//call after getcentroid
		return velocity;
	}


	public Vector3 GetCentroid()
	{
		return centroid;
	}



	public void AddElement(SmallShipFacade ele ) {
		elements.Add(ele);
		inUse = true;
	}

	public void Clear() {
		inUse = false;
		elements = new List<SmallShipFacade>();
	}

}
