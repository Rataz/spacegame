﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(SmallShipFacade))]


/**
 * Helper script for AIs
 * 
 * common functions for multiple AIs
 * 
 * */
public class ShipUtils : MonoBehaviour {

	private SmallShipFacade shipFacade;
	private Transform tr;

	void Start()
	{
		tr = transform;
		shipFacade = gameObject.GetComponent<SmallShipFacade>();

		//waypoint manager
		WaypointManager wmanag = GameObject.FindObjectOfType<WaypointManager>();

		if (wmanag != null)
		{
			nextWaypoint = wmanag.first;

			if (nextWaypoint != null)
				next_waypoint_position = nextWaypoint.GetPos();
		}
	}


	//CheapTurning, avoid using it
	public void TurnTo(Vector3 vect)
	{
		if (vect.x > 0.1)
		{
			transform.Rotate(0, 40 * Time.deltaTime, 0);
		}
		else if (vect.x < -0.1)
		{
			transform.Rotate(0, -40 * Time.deltaTime, 0);
		}

		if (vect.y > 0.01)
		{
			transform.Rotate(-40 * Time.deltaTime, 0, 0);
		}
		else if (vect.y < -0.01)
		{
			transform.Rotate(40 * Time.deltaTime, 0, 0);
		}
	}


	public void TurnToDirection(Vector3 dir)
	{
		tr.rotation = Quaternion.RotateTowards(tr.rotation, Quaternion.LookRotation(dir), 40 * Time.deltaTime);
	}


	//turn like an airplane
	public void BanknYank(Transform target, float projectileSpeed , float turnRadius)
	{
		Vector3 lead = shipFacade.CalculateLead(target, projectileSpeed);
		Vector3 tar = tr.InverseTransformPoint(lead);

		float upangle = Mathf.Atan2(tar.y, tar.z);

		//60 m/s is star wars bullet speed

		//find position of target projected onto the xy plane of our ship
		tar.z = 0;
		Vector3 worldpos = tr.TransformPoint(tar);  //projected target position

		//if target is within sphere positioned on our xy plane
		//and we are within said sphere
		//then we cannot turn hard enough to get him
		//float dista = (Vector3.Distance(tr.position, worldpos));

		if ((Vector3.Distance(tr.position, worldpos) < turnRadius) &&
			(Vector3.Distance(target.position, worldpos) < turnRadius))
		{
			// cannot turn fast enough
			//so continue forward until outside the sphere, or slow down
		}
		else
		{
			if (tar.x > .005)
			{
				shipFacade.Torque(new Vector3(0, 1, 0));
			}
			else if (tar.x < -.005)
			{
				shipFacade.Torque(new Vector3(0, -1, 0));
			}

			if (upangle > 0.01)
			{
				shipFacade.Torque(new Vector3(-1, 0, 0));
			}
			else if (upangle < -0.01)
			{
				shipFacade.Torque(new Vector3(1, 0, 0));
			}
		}

		/* FOR reference, previous attempt at better manouvering
		Rigidbody targetRb = target.GetComponent<Rigidbody>();
		Vector3 tar = tr.InverseTransformPoint( CalculateLead(target.position, targetRb.velocity , projectileSpeed) );

		//find if is in unreachable area
		float movementSpeed = rb.velocity.magnitude;
		float rotationSpeedInRadians = Mathf.Deg2Rad * ( rotationMultiplier );
		float radius = movementSpeed / rotationSpeedInRadians;
		*/
	}


	/**
	 * turn to some arbitrary point but with a certain Up Vector	 
	*/
	public void BanknYank2(Vector3 tar, Vector3 up)
	{
		float upangle = Mathf.Atan2(tar.y, tar.z);
		tar.z = 0;
		

		//yaw
		if (tar.x > 0.2)
		{
			transform.Rotate(0, 40 * Time.deltaTime, 0);
		}
		else if (tar.x < -0.2)
		{
			transform.Rotate(0, -40 * Time.deltaTime, 0);
		}

		//pitch
		if (upangle > 0.02)
		{
			transform.Rotate(-40 * Time.deltaTime, 0, 0);
		}
		else if (upangle < -0.02)
		{
			transform.Rotate(40 * Time.deltaTime, 0, 0);
		}

		if (tar.x < 0.02 && tar.x > -0.02 && upangle < 0.02 && upangle > -0.02)
		{
			Vector3 rolltarget = Vector3.Cross(tr.forward, Vector3.up);
			Vector3 relativeRoll = tr.InverseTransformDirection(rolltarget);

			if (relativeRoll.y > 0)
			{
				transform.Rotate(0, 0, -40 * Time.deltaTime);
			}
			else
			{
				transform.Rotate(0, 0, 40 * Time.deltaTime);
			}
		}
	}


	//Go to a world space position
	public void BanknYankTo(Vector3 pos)
	{
		Vector3 tar = tr.InverseTransformPoint(pos);

		float upangle = Mathf.Atan2(tar.y, tar.z);

		if (tar.x > .001)
		{
			shipFacade.Torque(new Vector3(0, 0, -1));
		}
		else if (tar.x < -.001)
		{
			shipFacade.Torque(new Vector3(0, 0, 1));
		}

		if (upangle > 0.01)
		{
			shipFacade.Torque(new Vector3(-1, 0, 0));
		}
		else if (upangle < -0.01)
		{
			shipFacade.Torque(new Vector3(1, 0, 0));
		}
	}



	/*
	//check if a position 'pos' is visible within 'angle' off of dead ahead
	*/
	public bool CheckFov(Vector3 targetPos, float angle, Vector3 myPos)
	{
		return TargettingUtils.CheckFov( targetPos, angle , tr.position, tr.forward);
	}



	/*
	 * Check for enemies that are in field of view cone
	 * 
	 * returns null if no target in view
	 * 
	 * Use ship rosters instead??
	 * 
	 * */
	public Transform CheckEnemiesFov(string teamTagEnemy)
	{
		GameObject [] enemies = GameObject.FindGameObjectsWithTag(teamTagEnemy);
		List<Transform> set = new List<Transform>();

		foreach (GameObject g in enemies)
		{
			set.Add(g.transform);
		}

		return TargettingUtils.ClosestInFov(set, tr);
	}




	//find position to go towards to hit target
	public Vector3 CalculateLead(Vector3 targetPosition, Vector3 targetVelocity, float projectileSpeed)
	{
		return TargettingUtils.CalculateLead(tr.position, targetPosition, targetVelocity, projectileSpeed);
	}



	//find position to go towards to hit target
	public Vector3 CalculateLead(Transform tar , float projectileSpeed)
	{
		Rigidbody target = tar.root.GetComponent<Rigidbody>();
		return TargettingUtils.CalculateLead(tr.position, tar.position, target.velocity, projectileSpeed);
	}




	//attacking behavior
	public void SimpleGunAttack(Transform target , float projectileSpeed , float attackFireRange, float fireAngle , ShipWeapon gun)
	{

		Vector3 lead = CalculateLead(target, projectileSpeed) - tr.position;
		Vector3 tarpos = target.position;
		float targetRange = Vector3.Distance(tr.position, target.position);

		if (targetRange < attackFireRange)
		{

			if (Vector3.Angle(tr.forward, lead) < fireAngle)
			{//fire if angle is good
				if (gun)
				{
					gun.Fire();
				}
			}
		}

		BanknYank(target, projectileSpeed, 100);
	}


	//attacking behavior
	public void AttackMissle(Transform target , MissleLauncher misslelauncher , float fireAngle , float missleAttackRange , float projectileSpeed)
	{
		Vector3 tarpos = target.position;
		Vector3 lead = tarpos - tr.position;

		//target is close
		float targetRange = (Vector3.Distance(tarpos, tr.position));

		if (targetRange < missleAttackRange)
		{

			if (Vector3.Angle(tr.forward, lead) < fireAngle)
			{//fire if angle is good
				if (misslelauncher)
				{
					misslelauncher.Fire(target);
				}
			}
		}

		BanknYank(target, projectileSpeed, 100);
	}





	//return vector3.zero if nothing ahead
	public Vector3 CheckAhead( Transform[] castpoints , LayerMask layerMask )
	{
		bool hitSomething = false;
		Vector3 totalDir = Vector3.zero;
		Vector3 avgDir = Vector3.zero;
		int totalhits = 0;

		float castDistance = 30;

		foreach (Transform t in castpoints)
		{
			RaycastHit hit;
			bool didhit = Physics.SphereCast(t.position, 2f, t.forward, out hit, castDistance, layerMask);

			Debug.DrawRay(t.position, t.forward * castDistance, Color.yellow);

			if (didhit)
			{
				totalhits++;
				totalDir += hit.normal;
				hitSomething = true;
			}
		}

		if (hitSomething)
		{
			//Debug.Log("hit soemthing " + gameObject.name);
			//find average normal of all hits.
			avgDir = new Vector3(totalDir.x / totalhits, totalDir.y / totalhits, totalDir.z / totalhits);

			Debug.DrawRay(tr.position, avgDir * 30, Color.red);
		}

		return totalDir;
	}



	/*
	 * Get closest which is in fov and not too close
	 * */
	public Transform InRangeAndFov(GameObject[] enemies , float minRange , float maxRange , float fov, Vector3 myposition)
	{
		Transform target = null;

		if (enemies.Length > 0)
		{
			float mag = 9999f;
			Transform tar;

			foreach (GameObject g in enemies)
			{
				float dist = Vector3.Distance(tr.position, g.transform.position);
				if (dist < mag)
				{
					if (dist > minRange  && dist < maxRange )
					{
						if ( CheckFov(g.transform.position, fov, myposition) )
						{
							mag = Vector3.Distance(tr.position, g.transform.position);
							tar = g.transform;
							target = tar;
						}
						
					}
				}
			}
		}
		return target;
	}


	/**
	 * Tell a target if we are attacking it or not so objectives can be assigned
	 * 
	 * */
	public void TargetInform( Transform previousTarget , Transform newTarget )
	{

		if (previousTarget != null)
		{
			if (previousTarget != newTarget)
			{
				//tell previous target we have broken off
				GroundTarget prevgtar = previousTarget.gameObject.GetComponent<GroundTarget>();
				if (prevgtar)
				{
					prevgtar.NotAttackingYouAnymore(transform);
				}

				//tell target we are attacking it
				GroundTarget gtar = newTarget.gameObject.GetComponent<GroundTarget>();
				if (gtar)
				{
					gtar.AttackingYou(this.gameObject);
				}
			}
		}
		else
		{
			if (newTarget != null)
			{
				//tell target we are attacking it
				GroundTarget gtar = newTarget.gameObject.GetComponent<GroundTarget>();
				if (gtar)
				{
					gtar.AttackingYou(tr.gameObject);
				}
			}

		}
	}







	private Waypoint nextWaypoint;
	private Vector3 next_waypoint_position;

	public void ReturnToHangar()
	{
		if (nextWaypoint != null)
		{
			if (Vector3.Distance(tr.position, next_waypoint_position) < 35.0f)
			{
				nextWaypoint = nextWaypoint.next;
				shipFacade.ThrottleSet(75.0f);

				if (nextWaypoint != null)
				{
					next_waypoint_position = nextWaypoint.GetPos();
				}
				else
				{
					//speed=0;
					shipFacade.ThrottleZero();
					shipFacade.Coast(true);
				}
			}

			if (nextWaypoint != null)
			{
				Vector3 targetDir = next_waypoint_position - tr.position;
				float step = (40) * Time.deltaTime;
				Vector3 newDir = Vector3.RotateTowards(tr.forward, targetDir, step, 0.0F);
				Debug.DrawRay(tr.position, newDir, Color.red);
				tr.rotation = Quaternion.LookRotation(newDir, nextWaypoint.GetUp());
			}
		}
	}





}
