﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/**
 * Allows a turret to take damage
 * 
 * needs to be on the collider
 * */
public class TurretDamage : MonoBehaviour, IDamageable {

	[Tooltip("Hitpoints")]
	public float hp = 25;

	[Tooltip("to observe only")]
	public bool destroyed = false;


	[Tooltip("fire effect for when defeated")]
	public ParticleSystem fire;

	public Turret turret;

	public TurretTargeting turretTargeting;


	/**
	 * Call this when shooting and hitting the ship
	 * */
	public void ApplyDamage(float damage)
	{
		if( !destroyed)
		{

			HpModify(-damage);

			if (hp <= 0)
			{
				hp = 0;
				Defeat();
				destroyed = true;
			}
		}
		
	}
	

	/* 
	 * Add/Subtract hp after it has gotten through the shield, also tracks frequency of being hit
	 * */
	public void HpModify(float add)
	{
		hp += add;
	}


	public void Defeat()
	{
		turret.SetDisarmed();
		turretTargeting.SetDisarmed();
		NotifyGeneric();

		if (fire)
		{
			var em = fire.emission;
			em.enabled = true;
		}
	}

	public void ApplyDamage(int damage)
	{
		ApplyDamage(damage);
	}

	public int GetTeam()
	{
		return 0;
	}



	public void NotifyGeneric()
	{
		GameObject x = GameObject.Find("Campeign");

		string str = transform.name + " destroyed";
		print(str);

		if (x != null)
		{
			
			x.BroadcastMessage("GameEvent", str, SendMessageOptions.DontRequireReceiver);

			
		}
	}
}
