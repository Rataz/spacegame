﻿using UnityEngine;
using System.Collections;

public class ShipShieldAITest1 : MonoBehaviour , IDamageable  {
	
	public SpaceAItest1 ship;
	
	public bool front = true;


	public void ApplyDamage( float damage ){

		if( front ){
			ship.ApplyDamageFront( damage );
		}
		else {
			ship.ApplyDamageRear( damage );
		}

		ship.ApplyDamage( damage );
	}

	//compatability with int
	public void ApplyDamage( int damage ){
		float bob;
		bob = damage;
		ApplyDamage( bob );
	}


	public int GetTeam( ){
		return 0;
	}
	
}
