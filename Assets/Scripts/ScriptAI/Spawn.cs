﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


/*
 * 
 * Creates a single squadron of ships
 * 
 * 
 * Make sure the scene has a 
 * -Squadron roster for the same team as this script
 * -GlobalSceneInfo
 * 
 * 
 * */
public class Spawn : MonoBehaviour {



	[Tooltip("Ship types that can be spawned (for now just put in 1)")]
	public List<SmallShipFacade> shipOptions;

	[Tooltip("Radius of a sphere that ships will appear in, within which they are positioned randomly")]
	public float spawnSphereRange = 5; //range of spawning

	[Tooltip("number of ships in a squadron")]
	public int initialCount = 3;

	[Tooltip("team the squadron is on")]
	public int team = 1;

	[Tooltip("colour of ship trails")]
	public Color trailColor;

	[Tooltip("A gameobject with the squadron script on it, (dont put any ships in the prefab, this script does that)")]
	public Squadron squadronPrefab;

	[Tooltip("Create an inital batch immidiately?")]
	public bool spawnAtStart = true;


	private int totalSpawnedCount = 0;
	private SquadronRoster squadronsRoster;
	private GlobalSceneInfo sceneinfo;



	void Awake()
	{
		sceneinfo = GameObject.FindObjectOfType<GlobalSceneInfo>();
		squadronsRoster = sceneinfo.GetSquadronRosterOfTeam(team);
	}


	public void Start(){		

		if(spawnAtStart)
			SpawnSquadron();
	}





	public void SpawnSquadron() {
		//Create a new empty squadron
		Squadron sq = Instantiate(squadronPrefab, Vector3.zero, Quaternion.identity) as Squadron;
		
		//add ships to the squadron
		for (int i = 0; i < initialCount; i++)
		{
			SmallShipFacade ship = SpawnShip(team, shipOptions[0]);

			//tell ship which squadron
			ship.SetSquadron(sq);

			//add ship to squadron
			sq.AddShip(ship);
		}

		//add the squadron to the list of squadrons
		squadronsRoster.AddSquadron(sq);
	}



	public SmallShipFacade SpawnShip( int team , SmallShipFacade ship ){
		
		Vector3 spot = Random.insideUnitSphere * spawnSphereRange + transform.position;
		SmallShipFacade spawnedship	= Instantiate (ship, spot, transform.rotation) as SmallShipFacade;
		

		spawnedship.SetTeam( team );
		spawnedship.gameObject.GetComponent<Renderer>().material.SetColor( "_EmissionColor", trailColor);

		ChangeName( spawnedship.transform , team.ToString() );

		totalSpawnedCount++;
		return spawnedship;
	}

	public void ChangeName( Transform obj , string teamname ){
		string name = obj.name;
		name += " team" + teamname;
		name += " no." + totalSpawnedCount;
		obj.name = name;
	}

}
