﻿using UnityEngine;
using System.Collections;

public class SmallShipClusterElement  {

	public SmallShipFacade ship;

	private ShipCluster clust;

	public bool hasCluster = false;

	public SmallShipClusterElement( SmallShipFacade shipp ){
		ship = shipp;
	}

	public void SetClust(ShipCluster clust) {
		this.clust = clust;
		//clust.AddElement(this);
		hasCluster = true;
	}

	public ShipCluster GetCluster() {
		return clust;
	}

	public Vector3 GetPosition() {
		return ship.GetPosition();
	}

}
