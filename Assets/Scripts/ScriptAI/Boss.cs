﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Gives objectives to squadrons based on targets asking for defense
 *
 * Make sure the scene has a
 * 
 * -GlobalSceneInfo
 * -ShipRosterTeam for each team
 * -SquadronRoster
 * 
 * */
public class Boss : MonoBehaviour {

	//auto assigned------
	private GlobalSceneInfo sceneinfo;	
	private SquadronRoster mysquadrons;
	//--------------

//private ShipRosterTeam myships;



	public int team = 1;

	public List<AttackObjective> incompletes = new  List<AttackObjective>();


	void Start () {
		sceneinfo = GameObject.FindObjectOfType<GlobalSceneInfo> ();
		
		mysquadrons = sceneinfo.GetSquadronRosterOfTeam ( team );

//myships = sceneinfo.GetRosterOfTeam ( team );

		StartCoroutine ("ObjectiveRefresh");
	}

	public IEnumerator ObjectiveRefresh(){
		yield return new WaitForSeconds (1);
		AssignObjectives ();
		ObjectiveRefresh ();
	}

	/*
	 * Other objects call this when they are attacked
	 * */
	public void IAmBeingAttacked( Transform targetTransform , Transform attacker , int priority ){
		//create an objective to defend the thing
		AttackObjective obj = new AttackObjective ( "xxx" , attacker );

		bool assigned = false;

		//find a squadron to take on the objective
		foreach (Squadron x in mysquadrons.squadrons) {
			if (x.attackobjective == null ) {

				x.AssignObjective (obj);
				assigned = true;
				//found, search no further
				break;
			} else {
				if ( !x.attackobjective.IsPending ()) {
					x.AssignObjective (obj);
					assigned = true;
					//found, search no further
					break;
				}
			}
		}

		if ( !assigned ) {
			//could not send help, store for later
			incompletes.Add( obj );
		}
	}

	public void ObjectiveComplete(AttackObjective objet){

		RemoveStaleObjectives ();

		if (incompletes.Count > 0) {
			AttackObjective obj = incompletes [0];
			bool assigned = false;

			//find a squadron to take on the objective
			foreach (Squadron x in mysquadrons.squadrons) {
				if (x.attackobjective == null ) {

					x.AssignObjective (obj);
					assigned = true;
					//found, search no further
					break;
				} else {
					if ( !x.attackobjective.IsPending ()) {
						x.AssignObjective (obj);
						assigned = true;
						//found, search no further
						break;
					}
				}
			}

			if ( assigned ) {
				incompletes.Remove( obj );
			}
		}
	}

	/*
	 * Give pending objectives to free squadrons
	 * 
	 * */
	public void AssignObjectives(){

		RemoveStaleObjectives ();

		if (incompletes.Count > 0) {
			AttackObjective obj = incompletes [0];
			bool assigned = false;

			//find a squadron to take on the objective
			foreach (Squadron x in mysquadrons.squadrons) {
				if (x.attackobjective == null ) {

					x.AssignObjective (obj);
					assigned = true;
					//found, search no further
					break;
				} else {
					if ( !x.attackobjective.IsPending ()) {
						x.AssignObjective (obj);
						assigned = true;
						//found, search no further
						break;
					}
				}
			}

			if ( assigned ) {
				incompletes.Remove( obj );
			}
		}
	}

	//remove objectives that have been completed already from 'incomplete' list
	public void RemoveStaleObjectives(){
		List<AttackObjective> toRemove = new List<AttackObjective> ();
		foreach (AttackObjective x in incompletes) {
			if (!x.IsPending ())
				toRemove.Add (x);
		}
		foreach (AttackObjective x in toRemove) {
			incompletes.Remove (x);
		}
	}

	public void AssignObjectiveToSquadron(){
	
	}

}