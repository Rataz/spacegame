﻿using UnityEngine;
using System.Collections;

public class SquadronMarker : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine( "Stop");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public IEnumerator Stop(){
		yield return new WaitForSeconds( 0.5f );
		Destroy( gameObject );
	}

}
