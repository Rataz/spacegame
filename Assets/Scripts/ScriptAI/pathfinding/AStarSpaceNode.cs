﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AStarSpaceNode : MonoBehaviour {

	public List<AStarSpaceNode> connections;

	public float g = 0;
	public float h = 0;
	public AStarSpaceNode parent;

	void OnDrawGizmosSelected()
	{
		/*
		foreach(AStarSpaceNode x in connections) { 
			Gizmos.color = Color.blue;
			Gizmos.DrawLine(transform.position, x.transform.position);
		}
		*/


		//Gizmos.color = Color.red;
		//Gizmos.DrawLine(transform.position, parent.transform.position);
	}

	private void Start()
	{
		if (connections.Count == 0) 
			FindConnections();
	}

	public float getF() {
		return g + h;
	}

	public void FindConnections() {
		AStarSpaceNode[] points = FindObjectsOfType<AStarSpaceNode>();

		foreach (AStarSpaceNode pt in points)
		{
			if( pt != this)
			{
				if (Vector3.Distance(pt.transform.position, transform.position) < 100)
				{
					if (!Physics.Linecast(pt.transform.position, transform.position))
					{
						connections.Add(pt);
					}
				}
			}
			
		}
	}


	public void ClearParent()
	{
		parent = null;
	}


	public Vector3 GetPosition()
	{
		return transform.position;
	}

	
}
