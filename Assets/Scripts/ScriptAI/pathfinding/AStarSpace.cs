﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/* 
 * An experiment
 * 
 * Put this script on a gameobject, put node scripts on immidiate child objects
 * 
 * */
public class AStarSpace : MonoBehaviour {

	public List<AStarSpaceNode> points = new List<AStarSpaceNode>();

	public List<Vector3> path = new List<Vector3>();

	public AStarSpaceNode start;
	public AStarSpaceNode end;

	public Transform x1;
	public Transform x2;

	void Start () {

		foreach (Transform child in transform)
		{
			AStarSpaceNode x = child.GetComponent<AStarSpaceNode>();
			x.h = Vector3.Distance(end.transform.position, child.position);
			points.Add( x );
		}
		
	}
	
	
	void Update () {
		if (Input.GetKey(KeyCode.A))
		{
			start = GetClosestNode(x1.position);
			end = GetClosestNode(x2.position);
			pathfind(start, end);

			GetPath(x1.position, x2.position);
			//pathfind( GetClosestNode(x1.position) , GetClosestNode(x2.position) ) ;
		}
		
	}


	public AStarSpaceNode GetClosestNode(Vector3 pos)
	{
		//assuming there is at least 1 node

		float d = Mathf.Infinity;
		AStarSpaceNode node = points[0];

		foreach(AStarSpaceNode x in points)
		{
			float dist = Vector3.Distance(pos, x.GetPosition());
			if(dist < d)
			{
				d = dist;
				node = x;
			}
		}
		return node;
	}


	void OnDrawGizmosSelected()
	{
		/*
		if( start == end) {
			Gizmos.color = Color.red;
			Gizmos.DrawLine(transform.position, end.transform.position);
		}
		*/
	}


	/*   A* implementation
	 * */
	public void pathfind(AStarSpaceNode start , AStarSpaceNode end ) {

		ClearParents();

		//f(n) total estimated cost of path through node n
		//f = g+h
		//g(n)  cost so far to reach node n
		//h(n) estimated cost from n to goal. This is the heuristic part of the cost function, so it is like a guess

		List<AStarSpaceNode> open = new List<AStarSpaceNode>();
		List<AStarSpaceNode> closed = new List<AStarSpaceNode>();
		open.Add(start);
		AStarSpaceNode node;
		

		while (open.Count > 0)
		{
			node = open[0];

			//find lowest f score in open list
			float lowf = Mathf.Infinity;
			foreach (AStarSpaceNode xx in open) {
				float f = xx.getF();
				if (f < lowf ) {
					lowf = f;
					node = xx;
				}
			}

			open.Remove(node);
			closed.Add(node);

			if (node == end)
			{
				break;
			}
			else
			{
				closed.Add(node);
				foreach (AStarSpaceNode neighbor in node.connections)
				{					
					if (closed.Contains(neighbor))
					{
						continue;
					}

					float tempCost = node.g + Vector3.Distance(node.transform.position, neighbor.transform.position);

					if (!open.Contains(neighbor) || tempCost < neighbor.g) {
						neighbor.g = tempCost;
						neighbor.h = Vector3.Distance(neighbor.transform.position, end.transform.position);
						neighbor.parent = node;

						if (!open.Contains(neighbor))
							open.Add(neighbor);
					}
				}
			}
		}

		//drawPath();
	}


	public List<Vector3> GetPath( Vector3 me , Vector3 destination)
	{
		AStarSpaceNode point1 = GetClosestNode(me);
		AStarSpaceNode point2 = GetClosestNode(destination);

		pathfind(point2, point1);
		//pathfind(AStarSpaceNode start, AStarSpaceNode end);

		path.Clear();
		
		AStarSpaceNode curr = point1;

		path.Add(curr.GetPosition());

		while (curr.parent != null)
		{
			path.Add( curr.parent.GetPosition() );
			curr = curr.parent;
		}

		
		if(path.Count > 1)
		{
			for (int i = 0; i < path.Count -1; i++)
			{
				Vector3 x = path[i];
				Vector3 y = path[i + 1];
				Debug.DrawLine(x, y, Color.red);
			}
		}
		else
		{
			print("count<1");
		}

		return path;
	}

	public void drawPath() {
		AStarSpaceNode curr = end;
		
		while (curr != start) {
			Debug.DrawLine(curr.transform.position, curr.parent.transform.position,Color.red);
			curr = curr.parent;
		}
	}

	public void ClearParents()
	{
		foreach (AStarSpaceNode x in points)
		{
			x.ClearParent();
		}
	}



}
