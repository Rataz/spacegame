﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


/*
 * Handles targetting for a turret
 * 
 * Aims at the centroid of clusters of enemies
 * 
 * For now will only target individuals not clusters until we can separate
 * clusters of allies from clusters of enemies.
 * See ClusterTargeter
 * 
 * */
public class TurretTargeting : MonoBehaviour {

	[Tooltip("Turret to be controlled")]
	public Turret turret;
	
	[Tooltip("Max range at which things will be targeted")]
	public float maxTargettingRange = 200;

	//Center position of gun to measure angles from
	[Tooltip("Center/default position of gun to measure gun traverse angles off of.  " +
		"Should not be a child object of the rotating turret")]
	public Transform gunCenterPosition;

	[Space(20)]
	//public bool limitedTraverse;
	public float maxTrav = 30;
	//public float minTrav = -30;
	public float maxElev = 60;
	public float minElev = -10;

	[Tooltip("Close enough angle, will fire when angle to target is less than this")]
	public float fireAngle = 40;

	public ExplosiveBulletGun gun;

	[Tooltip("Speed of bullets")]
	public float projectileSpeed = 300;

	public float rangeOffset = -4;

	[Tooltip("Which team we are on")]
	public int team=1;


	private List<GameObject> boids;//flock members

	private ShipRosterTeam shiproster;



	public bool drawgizmos = false;

	//should this script fo anything, (ie not disabled or destroyed)
	private bool armed = true;
	

	void Update () {

		if (armed)
		{
			boids = new List<GameObject>();

			UpdateTargets();

			FindTargetFire();

		}

		//Disabled until allied and enemy clusters are separated
		//GetTarAndRangeFire();
	}


	/*
	 * Find range to closest cluster of ships and aim at it and fire
	 * 
	 * Works but fires on allies also
	 * */
	private void GetTarAndRangeFire() {
		ClusterTargeter clusterTargetter = FindObjectOfType<ClusterTargeter>();
		List<ShipCluster> clusters = clusterTargetter.GetClusters();

		float closest = maxTargettingRange;
		Vector3 bestAim = Vector3.zero;
		float shootrange = 999;
		bool foundTarget = false;

		foreach (ShipCluster cluster in clusters)
		{
			if (cluster.inUse)
			{
				Vector3 pos = cluster.GetCentroid();
				Vector3 velo = cluster.GetVelocity();
				
				Vector3 aim = TargettingUtils.CalculateLead(transform.position,pos, velo, projectileSpeed);
				float dist = Vector3.Distance(transform.position, aim);
				
				//in range?
				if (dist < maxTargettingRange)
				{					
					foundTarget = true;
					//is it a new closest?

					//have freedom to hit?
					if (CheckCanTraverse(aim))
					{
						if (dist < closest)
						{
							closest = dist;
							shootrange = dist;
							bestAim = aim;
						}
					}
				}
			}
		}
		
		if (foundTarget)
		{
			Fire(shootrange + rangeOffset);
			turret.SetTarget(bestAim);
		}		
	}
	
	/*
	 * Unused right now
	 * */
	void FindTargetFire(){

		bool foundTarget = false;
		float targetRange = maxTargettingRange;
		UpdateTargets();
		Vector3 aim = Vector3.zero;

		foreach ( GameObject x in boids )
		{
			if( x != null ){
				Transform targetTransform = x.transform;
								
				aim = TargettingUtils.CalculateLead(transform.position, targetTransform.position, targetTransform.root.GetComponent<Rigidbody>().velocity, projectileSpeed);

				float dist = Vector3.Distance( aim , transform.position );
				//in range?
				if( dist < targetRange ){
					//have freedom to hit?
					if( CheckCanTraverse( aim )){
						targetRange = dist;
						foundTarget = true;
					}
				}
			}
		}


		if (foundTarget)
		{
			Fire(targetRange + rangeOffset);
			turret.SetTarget(aim);
		}


	}


	//update who is in this flock, in case some destroyed
	public void UpdateTargets()
	{
		ShipRosterTeam[] t = GameObject.FindObjectsOfType<ShipRosterTeam>();
		
		for (int i = 0; i < t.Length; i++)
		{
			if (t[i].team != team)
			{
				foreach (SmallShipFacade s in t[i].ships)
				{
					boids.Add(s.gameObject);
				}
			}
		}
	}

	/*
	//Does gun have enough freedom of movement to hit target position?
	*/
	private bool CheckCanTraverse( Vector3 targetPosition ){

		//default assume cant hit
		bool ret = false;

		if( gunCenterPosition )
		{	
			//position relative to self
			Vector3 relpos =gunCenterPosition.InverseTransformPoint( targetPosition );

			//elevation to reach pos
			float azimuth = Mathf.Atan( relpos.y/relpos.z ); 

			//elevation is reachable
			if( azimuth < maxElev && azimuth > minElev )
			{
				if( Vector3.Angle(gunCenterPosition.forward, targetPosition-transform.position) <maxTrav)
					ret = true;
			}
		}


		return ret;
	}

	private void Fire(float range)
	{		
		if (gun)
			gun.Fire(range);
	}
	
	/* For other scripts to change what team we are on */
	public void SetTeam(int newTeam)
	{
		team = newTeam;
	}



	public void SetDisarmed()
	{
		armed = false;
	}

	public void Arm()
	{
		armed = true;
	}


	void OnDrawGizmosSelected()
	{
		if( drawgizmos)
		{
			Gizmos.color = Color.red;
			Vector3 direction = gunCenterPosition.TransformDirection(Vector3.forward) * maxTargettingRange;
			Gizmos.DrawRay(gunCenterPosition.position, direction);
		}
		
	}

}
