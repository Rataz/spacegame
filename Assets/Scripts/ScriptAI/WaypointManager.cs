﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WaypointManager : MonoBehaviour {

	public Waypoint first;

	public List<Waypoint> points = new List<Waypoint>();

	public List<Waypoint> GetWaypoints(){
		return points;
	}



}
