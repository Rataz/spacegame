﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(ShipUtils))]
[RequireComponent(typeof(SmallShipFacade))]



public class SpaceFighterAI : MonoBehaviour {

	//turned on controlling the ship or not, not the same as dead/alive
	public bool activated = true;
	
	private SmallShipFacade shipFacade;	
	private ShipUtils shipUtils;
	private Transform tr;
	private Vector3 acceleration;

	public float fleeRange = 70;
	public float attackRange = 150;

	public float fireAngle = 45;
	public float attackFireRange = 150;

	public float missleMaxRange = 650;
	public float missleMinRange = 200;
	public float missleAttackAngle = 4;

	public Transform flocktarget;
	public Transform target;
	public Transform targetTagObject;

	[Tooltip("primary weapon")]
	public ShipWeapon gun;

	[Tooltip("secondary wepon")]
	public MissleLauncher missleLauncher;

	[Tooltip("for ai to lead target")]
	public float projectileSpeed = 20;//for leading target, should be found automatically in future

	private string oldtag;
	public string teamTag = "EnemyTeam2";
	public string teamTagEnemy = "EnemyTeam1";
		
	

	//range to attack same target
	public float targetAssistDist = 100;

	//squadron this ship is in
	[Tooltip("to observe only")]
	private Squadron squadron;


	public float turnradius = 200;


	private int mode = 0;
	private long frame = 0;

	public float approachRange = 100;


	void Awake() {
		tr = transform;

		shipFacade = gameObject.GetComponent<SmallShipFacade> ();
		shipUtils = gameObject.GetComponent<ShipUtils>();
	}

	void Start()
	{
		if (targetTagObject) {
			oldtag = targetTagObject.tag; //setup tag for radar;
		}
	}


	void Update()
	{
		if (activated)
		{
			if (!shipFacade.GetDestroyedStatus())
			{
				//only do AI if alive

				if (frame == 0)
				{
					mode = DecideMode();
				}
				else if (frame > 10)
				{
					frame = 0;
				}
				else
				{
					frame++;
				}

				DoMode(mode);
			}
		}
	}


	//get the target of closest squadron member
	public Transform GetClosestAlliesTarget( )
	{	
		List<SmallShipFacade> ships = squadron.ships;
		float mindist = targetAssistDist;

		Transform targ = null;

		foreach (SmallShipFacade x in ships) {
			float toAlly = Vector3.Distance (  x.GetPosition (), tr.position );
			if ( toAlly < mindist ) {
				
				Transform tar = x.GetTarget ();
				if (tar) {
					targ = tar;
				}
			}
		}
		return targ;
	}

	

	//attacking behavior
	public void Attack()
	{
		Vector3 lead = shipFacade.CalculateLead(target,projectileSpeed) - tr.position;	
		Vector3 tarpos = target.position;

		//target is close
		float dist = Vector3.Distance(tarpos, tr.position);

		if (dist < approachRange)
		{
			float proportion = (dist - 7) / approachRange;
			shipFacade.ThrottleSet(proportion * 100);
		}
		else {
			shipFacade.ThrottleSet( 100);
		}
		
		if ( dist < attackFireRange)
		{
			//speeda = hardTurnSpeed; //slow down
			if (Vector3.Angle(tr.forward, lead) < fireAngle)
			{
				if(gun)
				{
					gun.Fire();
				}
			}
		}

		if ( dist < missleMaxRange &&  dist > missleMinRange )
		{
			if (Vector3.Angle(tr.forward, target.position) <  missleAttackAngle )
			{
				if (missleLauncher)
				{
					missleLauncher.Fire (target);
				}
			}
		} 

		//show who targetting, red tick beside self, line connected to target
		//Debug.DrawLine(tr.position, lead, Color.red);
		Debug.DrawLine(tr.position, tarpos, Color.magenta);
		shipUtils.BanknYank(target , projectileSpeed , turnradius);
	}





	//which behavior to use
	private int DecideMode()
	{

		GlobalSceneInfo x = GameObject.FindObjectOfType<GlobalSceneInfo> ();
		ShipRosterTeam enemyRoster; //= x.GetRosterOfTeam( 

		int myteam = shipFacade.team;

		if (myteam == 1)
		{
			enemyRoster = x.GetRosterOfTeam ( 2 );
		}
		else
		{
			enemyRoster = x.GetRosterOfTeam ( 1 );
		}

		int mode = 0;

		if (squadron)
		{

			if ( squadron.IsObjectivePending() )
			{
				//there is an objective that must be completed

				//set target to objective target and attack it
				target = squadron.GetObjective ().target;
				mode = 0;			
			}
			else {
				//there isnt an objective to complete
				if (Vector3.Distance (tr.position, squadron.GetCentroid ()) > 400.0f)
				{
					//regroup
					mode  = 1;
				}
				else
				{
					ChooseTarget ();

					if (target != null) {
						//attack
						mode = 0;
					} else {
						int numships = squadron.NumberOfSurvivingMembers ();

						if (enemyRoster.ShipCount () > 0)
						{
							if (numships > 1)
							{
								//regroup
								mode = 1;
							}
							else
							{
								//hangar
								mode = 3;
							}
						}
						else
						{
							mode = 3;
						}
					}
				}
			}
		} 
		else 
		{
			//not in a squadron

			ChooseTarget ();

			if (target != null)
			{
				//attack
				mode = 0;
			}
			else
			{			
				//go to world center
				mode =3;
			}
		}

		return mode;
	}

	private void DoMode( int mode )
	{	
		if (mode == 0) {
			Attack();
		} else if (mode == 1) {
			Regroup ();
		} else if (mode == 2) {
			shipUtils.BanknYankTo(Vector3.zero);
		} else if (mode == 3) {
			shipUtils.ReturnToHangar ();
		} else if (mode == 4) {
		
		} else if (mode == 5) {
		
		}
	}

	private void Regroup()
	{
		shipUtils.BanknYankTo (squadron.GetCentroid ());
	}


	//best target chosen based on closest within 45 deg angle
	//return true if a target found, false if no targets
	//set target
	private bool ChooseTarget() {

		bool ret = false;

		GameObject[] set = GameObject.FindGameObjectsWithTag (teamTagEnemy);

		if (set.Length > 0)
		{

			float mag = 9999f;
			Transform tar;

			foreach (GameObject g in set)
			{

				//distance
				if (Vector3.Magnitude (tr.position - g.transform.position) < mag) {

					//in view angle
					if (shipFacade.CheckFov (g.transform.position, 45, tr.position)) {
						mag = Vector3.Magnitude (tr.position - g.transform.position);
						tar = g.transform;
						target = tar;
					}
				}
			}
			ret = true;
		}
			
		return ret;
	}
	

	public void SetSquadron(Squadron squad)
	{
		squadron = squad;
	}

	public Squadron GetSquadron()
	{
		return squadron;
	}

	/*
	 * //Update tag for others scripts to find
	 * 
	 * Needed for setting team at spawn time and for death
	*/
	public void ChangeTag(string newtag)
	{
		if (!shipFacade.GetDestroyedStatus())
			targetTagObject.tag = newtag;
	}

	public void SetTeamTag(string newtag)
	{
		tr.tag = newtag;
	}

	public void RevertTag()
	{
		if (!shipFacade.GetDestroyedStatus())
			targetTagObject.tag = oldtag;
	}

	public void ChangeTeam(string teamTag, string enemyTag)
	{
		SetTeamTag(teamTag);
		teamTagEnemy = enemyTag;
	}


	public void TargetThis(Transform tar)
	{
		target = tar;
	}



}









