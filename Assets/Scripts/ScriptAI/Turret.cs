﻿using UnityEngine;
using System.Collections;


/*
 * Rotates a turret gameobject
 * 
 * this script goes on the turret itself
 * 
 * Other scripts must tell this one what Vector to rotate towards
 * 
 * Does not handle firing
 * 
 * 
 * */
public class Turret : MonoBehaviour {

	private Vector3 targetPosition;

	[Tooltip("Barrel of the gun")]
	public Transform barrel;

	[Tooltip("what the turret object is attached to, ex main hull of a ship")]
	public Transform ship;

	[Tooltip("How quickly turret can spin")]
	public float turretSpeed = 30;
		
	[Space(10)]
	[Header("Freedom of movement settings")]
	[Tooltip("Does the turret have traversal limits?")]
	public bool limitTraverse = false;
	public float turretMin = -180;
	public float turretMax = +180;	

	public float barrelMin = 0;
	public float barrelMax = 70;

	private float turretCurrent = 0;
	private float barrelCurrent = 0;

	//should this script fo anything, (ie not disabled or destroyed)
	private bool armed = true;


	void Update () {		
		RotateTurret( targetPosition );
		RotateBarrel( targetPosition );
	}
	
	public void SetTarget( Vector3 tar ){
		targetPosition = tar;
	}
	

	private void RotateTurret( Vector3 targetPosition){


		if( targetPosition !=Vector3.zero && armed  ){
			Vector3 relativepos = transform.InverseTransformPoint( targetPosition );

			if( relativepos.x > 0.1 ){
				if( limitTraverse )
				{
					if( turretCurrent<turretMax ){
						transform.Rotate( 0, turretSpeed*Time.deltaTime,0 );
						turretCurrent += turretSpeed * Time.deltaTime;
					}
				}
				else{
					transform.Rotate( 0, turretSpeed*Time.deltaTime,0 );
				}

			}
			else if( relativepos.x < -0.1) {
				if( limitTraverse )
				{
					if( turretCurrent>turretMin ){
						transform.Rotate( 0, -turretSpeed*Time.deltaTime,0 );
						turretCurrent -= turretSpeed * Time.deltaTime;
					}
				}
				else{
					transform.Rotate( 0, -turretSpeed*Time.deltaTime,0 );
				}
			}
		}
	}


	private void RotateBarrel(Vector3 targetPosition)
	{
		if( targetPosition != Vector3.zero && armed){
			Vector3 relativepos = barrel.InverseTransformPoint( targetPosition );

			if( relativepos.y > 0.1 ){
				if( barrelCurrent < barrelMax )
				{
					barrelCurrent +=turretSpeed*Time.deltaTime;
					barrel.Rotate( -turretSpeed*Time.deltaTime,0,0 );
				}
			}
			else if( relativepos.y < -0.1 ) {
				
				if( barrelCurrent > barrelMin )
				{
					barrelCurrent -= turretSpeed*Time.deltaTime;
					barrel.Rotate( turretSpeed*Time.deltaTime,0,0 );
				}

			}
		}
	}


	public void SetDisarmed()
	{
		armed = false;
	}

	public void Arm()
	{
		armed = false;
	}



	//WIP
	//Almost works, eliminates twitch on long barrels
	//barrel and turret not always in sync
	void rotShip1(){
		Vector3 relativepos = ship.InverseTransformPoint( transform.position );
		float turretY = relativepos.y;
		Vector3 pos = targetPosition;
		Vector3 lpos = ship.InverseTransformPoint( pos );
		Vector3 lpos2 = transform.InverseTransformPoint( pos );
		lpos2.x=0;
		lpos.y = turretY;

		//transform.LookAt( ship.TransformPoint(lpos) , ship.up );
		//barrel.LookAt( transform.TransformPoint(lpos2) , ship.up );

		float step = 30 * Time.deltaTime;
		transform.rotation = Quaternion.RotateTowards( transform.rotation, Quaternion.LookRotation( ship.TransformPoint(lpos) , ship.up), step);
		barrel.rotation = Quaternion.RotateTowards( barrel.rotation, Quaternion.LookRotation( transform.TransformPoint(lpos2) , ship.up), step);
		//barrel.Rotate(  Quaternion.LookRotation( ship.TransformPoint(lpos) , ship.up).eulerAngles );
	}


}
