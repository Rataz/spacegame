using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Rigidbody))]

/**
 * For reference only
 * 
 * */

public class SpaceAItest1 : MonoBehaviour, IDamageable
{

	public Transform target;
	public Transform flocktarget;

	public float speed = 5.0f;
	public float turnRate = 3.0f;

	public float hardTurnSpeed = 5.0f;
	public float hardTurnRate = 3.0f;

	public Transform targetTagObject;
	private string oldtag;

	public float hp = 100.0f;
	public float shield_pow = 100;

	private float shld_f_pow;
	private float shld_b_pow;

	public bool destroyed = false;

	private GameObject[] boids;//flock members
	private GameObject[] boidsSameTarget; //flock members with the same target

	private Transform tr;
	private Rigidbody rb;
	private Vector3 acceleration;
	private Vector3 velo;
	private bool damageable = true;

	private float dist; //distance to target

	//collision avoidance, outside colliders
	public Transform castpointT;
	public Transform castpointB;
	public Transform castpointL;
	public Transform castpointR;
	public Transform castpointCen;

	public ShipWeapon gun;

//	private readonly VectorPid angularVelocityController = new VectorPid(33.7766f, 0, 0.2553191f);
//	private readonly VectorPid headingController = new VectorPid(9.244681f, 0, 0.06382979f);

	public List<GameObject> visibleBoids;

	private int mode = 0;

	public bool isleader = false;//leader of flock?

	public string teamTag = "EnemyTeam2";
	public string teamTagEnemy = "EnemyTeam1";

	private bool targetAvailable = false;

	public float projectileSpeed = 20;//for leading target

	Quaternion desiredRotation = Quaternion.identity;


	public float maxVelocityChange = 5.0f;



	void Awake() {
		tr = transform;
		rb = gameObject.GetComponent<Rigidbody>();
	}


	void Start() {

		//init shields
		shld_f_pow = shield_pow / 2;
		shld_b_pow = shield_pow / 2;

		if (targetTagObject)
			oldtag = targetTagObject.tag; //setup tag for radar;

		rb = gameObject.GetComponent<Rigidbody>();

		UpdateBoids(); //find flock members
		ChooseTarget();
		StartCoroutine(DecideMode());

		nexthit = Time.time;
		evadeTimer = Time.time;
	}

	//update who is in this flock, in case some destroyed
	public void UpdateBoids(){
		boids = GameObject.FindGameObjectsWithTag(teamTag);
	}


	void Update() {

		if (!destroyed)    { //only do AI if alive
			/*
			if (Time.time > nexthit)     { //reset hit counter
				hitsSince = 0;
			}
			else if (hitsSince > 3)      {//evade
				Attack();
			}*/

			if (isleader)       { //do not follow anyone else
				Attack(); //attack movement
			}
			else         {
				if (mode == 0) {
					Flock(); //flock movement
				}
				else if (mode == 1)  {
					Attack(); //attack movement
				}
				else if (mode == 2)  { //Flee
					Flee();
				}
				else if (mode == 3)   {//evade
					Attack();
				}
			}
		}

	}

	public float fleeRange = 70;
	public float attackRange = 150;


	//which behavior to use
	private IEnumerator DecideMode() {

		bool haveTarget = ChooseTarget();// update target

		dist = Vector3.Distance(tr.position, target.position);

//		Vector3 targetDir = target.position - tr.position;

		if ( dist < fleeRange )  {
			mode = 2; //Flee
			yield return new WaitForSeconds(2);//cooldown till change mode
		}
		else if (haveTarget && dist < attackRange)  {// 
			mode = 1;  //Attack
			yield return new WaitForSeconds(3);
		}
		else   {
			UpdateBoids();
			mode = 0; //Flock
			yield return new WaitForSeconds(2);
		}

		StartCoroutine(DecideMode());

	}


	//return true if a target found, false if no targets
	private bool ChooseTarget() {
		bool ret = false;

		GameObject[] set;
		set = GameObject.FindGameObjectsWithTag(teamTagEnemy);

		if (set.Length > 0)
		{
			float mag = 9999f;
			Transform tar;

			foreach (GameObject g in set)     {
				if (Vector3.Magnitude(tr.position - g.transform.position) < mag)      {
					mag = Vector3.Magnitude(tr.position - g.transform.position);
					tar = g.transform;
					target = tar;

				}
			}
			ret = true;
		}

		targetAvailable = ret;
		return ret;

	}

	private void Turnfight() {
		desiredRotation = Quaternion.Slerp(tr.rotation, Quaternion.LookRotation(acceleration), Time.deltaTime * 0.3f);
		tr.rotation = Quaternion.RotateTowards(tr.rotation, desiredRotation, turnRate * Time.deltaTime);
	}


	//move according to flocking
	private void Flock()
	{

		Vector3 r1 = Vector3.zero;

		if (targetAvailable)
		{
			r1 = Rule_1();
		}

		Vector3 r2 = Rule_2();
		Vector3 r3 = Rule_3();

		acceleration = r1 + r2 + r3;
		velo += 2 * acceleration * Time.deltaTime;

		//cap top speed
		if (velo.magnitude > speed)
			velo = velo.normalized * speed;

		//rb.AddForce( Vector3.forward*force );
		//rb.velocity = velo;
		//rb.velocity = (target.position-tr.position).normalized * speed;

		//desiredRotation = Quaternion.LookRotation(  target.position-tr.position );
		desiredRotation = Quaternion.Slerp(tr.rotation, Quaternion.LookRotation(acceleration), Time.deltaTime * 0.4f);
		tr.rotation = Quaternion.RotateTowards(tr.rotation, desiredRotation, turnRate * Time.deltaTime);
		//rb.velocity = tr.forward * speed ; //original , works

		// Apply force to reach target velocity--------
		Vector3 moveDir = tr.forward * speed;
		Vector3 velocity = rb.velocity;
		Vector3 velocityChange = (moveDir - velocity);
		velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
		velocityChange.y = Mathf.Clamp(velocityChange.y, -maxVelocityChange, maxVelocityChange);
		velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
		rb.AddForce(velocityChange, ForceMode.VelocityChange);
		//-------------------

	}

	GameObject[] enemies;
	public void CheckEnemiesFov() {
		bool ret = false;

		enemies = GameObject.FindGameObjectsWithTag(teamTagEnemy);

		if (enemies.Length > 0)
		{
			float mag = 9999f;
			Transform tar;

			foreach (GameObject g in enemies)
			{

				Vector3 enemypos = g.transform.position;
//				Vector3 trpos = tr.position;

				if (CheckFov(enemypos, 60))
				{
					if (Vector3.Magnitude(tr.position - g.transform.position) < mag)
					{
						mag = Vector3.Magnitude(tr.position - g.transform.position);
						tar = g.transform;
						target = tar;
					}
				}
			}
			ret = true;
		}

		targetAvailable = ret;
		//		return ret;
	}


	//check if position pos is visible within 'angle' off of center
	public bool CheckFov(Vector3 pos, float angle)
	{
		float ang = Vector3.Angle(pos - tr.position, tr.forward);
		bool ret = false;
		if (ang < angle)
		{
			ret = true;
		}
		return ret;
	}

	public float rot = 60;

	//turn like an airplane
	private void BanknYank(){
		Vector3 tar = transform.InverseTransformPoint( CalculateLead(target) );

		if(tar.x>2){
			transform.Rotate( 0, 0, -rot* Time.deltaTime  ) ;
		}
		else if( tar.x<-2 ){
			transform.Rotate( 0 , 0 , rot* Time.deltaTime );
		}
		if(tar.x>0.1){
			transform.Rotate(0, 10* Time.deltaTime, 0  ) ;
		}
		else if( tar.x<-0.1){
			transform.Rotate( 0 , -10* Time.deltaTime , 0 );
		}		
		else if(tar.y>0.1){
			transform.Rotate( -rot* Time.deltaTime , 0 , 0 );
		}
		else if( tar.y<-0.1){
			transform.Rotate( rot* Time.deltaTime , 0 , 0 );
		}


	}

	private float targetRange = 100f;
	public void SetTargetRange( float range ){
		targetRange = range;
	}

	public float GetTargetLeadRange(){
		return targetRange;
	}

	public float fireAngle = 45;
	public float attackFireRange = 150;


	//attacking behavior
	public void Attack() {

		Vector3 lead = CalculateLead(target) - tr.position;
		velo = lead + Rule_2();

		Vector3 tarpos = target.position;
//		Vector3 diff = tarpos - tr.position;
//		float turn = turnRate;
		float speeda = speed;

		//target is close
		SetTargetRange( Vector3.Distance(tarpos, tr.position)) ;

		if ( targetRange < attackFireRange) {
			speeda = hardTurnSpeed; //slow down
			if (Vector3.Angle(tr.forward, lead) < fireAngle)  {//fire if angle is good
				if (gun)  {
					gun.Fire();
					//print ("fire" + gameObject.name );
				}
			}
			else { //turn harder if not
//				turn = hardTurnRate;
			}
		}
		
				
		BanknYank();

		//desiredRotation = Quaternion.LookRotation(velo);
		//tr.rotation = Quaternion.RotateTowards(tr.rotation, desiredRotation, turn * Time.deltaTime);
		//rb.velocity = target.gameObject.GetComponent<Rigidbody>().velocity.magnitude * tr.forward;


		//rb.velocity = tr.forward * speeda ; //original, works

		// Apply force to reach target velocity--------
		Vector3 moveDir = tr.forward * speeda;
		Vector3 velocity = rb.velocity;
		Vector3 velocityChange = (moveDir - velocity);
		velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
		velocityChange.y = Mathf.Clamp(velocityChange.y, -maxVelocityChange, maxVelocityChange);
		velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
		rb.AddForce(velocityChange, ForceMode.VelocityChange);
		//-------------------

		//show who targetting, red tick beside self, line connected to target
		Debug.DrawLine(tr.position, target.position, Color.red);
		Vector3 bug = new Vector3(0, 0, 10);
		Debug.DrawRay(tr.position + desiredRotation * bug, desiredRotation * Vector3.right * 5, Color.red);

	}

	float evadeTimer = 0;
	private Vector3 evadeVector; //save evade vector, avoid twitching
	public float evadeSpeed = 45;

	private void Evade() {//for being chased

		print("evading");

		if (Time.time > evadeTimer)
		{ // choose new evade vector if have been on this one too long
			evadeTimer = Time.time + 1.0f;
			//print ("evade");
			Vector3 dodge = Random.insideUnitSphere * 50;
			evadeVector = Rule_2() + AvoidBoids() + dodge;
		}

		velo = evadeVector;
		desiredRotation = Quaternion.Slerp(tr.rotation, Quaternion.LookRotation(velo), Time.deltaTime * 0.1f);
		tr.rotation = Quaternion.RotateTowards(tr.rotation, desiredRotation, hardTurnRate * Time.deltaTime * .1f);

		//rb.velocity =   tr.forward * evadeSpeed; //original, works

		// Apply force to reach target velocity--------
		Vector3 moveDir = tr.forward * evadeSpeed;
		Vector3 velocity = rb.velocity;
		Vector3 velocityChange = (moveDir - velocity);
		velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
		velocityChange.y = Mathf.Clamp(velocityChange.y, -maxVelocityChange, maxVelocityChange);
		velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
		rb.AddForce(velocityChange, ForceMode.VelocityChange);
		//-------------------

		Debug.DrawRay(tr.position, evadeVector, Color.white);
	}


	public void Flee()
	{
		velo = Rule_2() + Rule_3() + tr.position - target.position;

		desiredRotation = Quaternion.Slerp(tr.rotation, Quaternion.LookRotation(velo), Time.deltaTime * 0.5f);
		tr.rotation = Quaternion.RotateTowards(tr.rotation, desiredRotation, turnRate * Time.deltaTime);
		rb.velocity = tr.forward * speed;
	}



	//find position to go towards to hit target
	private Vector3 CalculateLead(Transform tar)
	{

		Rigidbody target;
		target = tar.root.GetComponent<Rigidbody>();

		Vector3 soln = tar.position;

		if (target)
		{

			Vector3 V = target.velocity;
			Vector3 D = tar.position - tr.position;
			float A = V.sqrMagnitude - projectileSpeed * projectileSpeed;
			float B = 2 * Vector3.Dot(D, V);
			float C = Vector3.SqrMagnitude(D);

			if (A >= 0)
			{
				//Debug.LogError ("No solution exists");
			}
			else
			{
				float rt = Mathf.Sqrt(B * B - 4 * A * C);
				float dt1 = (-B + rt) / (2 * A);
				float dt2 = (-B - rt) / (2 * A);
				float dt = (dt1 < 0 ? dt2 : dt1);
				soln = (tar.position + V * dt);
			}
		}

		return soln;
	}



	public float rule1DistanceMag = 3.0f;
	public float rule1Multiplier = 20.0f;

	Vector3 Rule_1()
	{ //repels as reaching target , attracts when far away

		if (target)
		{
			Vector3 distance = target.position - tr.position;

			if (distance.magnitude < rule1DistanceMag)
			{
				Debug.DrawLine(tr.position, target.position, Color.red);
				return distance.normalized * -rule1Multiplier;
			}
			else
			{
				Debug.DrawLine(tr.position, target.position, Color.blue);
				return distance.normalized * rule1Multiplier;
			}
		}
		else
		{
			return Vector3.zero;
		}

	}


	public float fwdCheckLenth = 10.0f;
	public float Rule2Multip = 2.0f;

	//raycast avoidance
	//break away upward when obsticle in front
	Vector3 Rule_2()
	{ //check in front,  yellow true , magenta false

		Vector3 ret = Vector3.zero;

		//make sure not hitting own shield

		if (Physics.Raycast(castpointT.position, tr.forward, fwdCheckLenth))
		{//hit a collider
			Debug.DrawRay(castpointT.position, tr.forward * fwdCheckLenth, Color.yellow); //hit
			ret = tr.up * -Rule2Multip;
		}
		else if (Physics.Raycast(castpointB.position, tr.forward, fwdCheckLenth))
		{
			Debug.DrawRay(castpointB.position, tr.forward * fwdCheckLenth, Color.yellow);
			ret = tr.up * Rule2Multip;
		}
		else if (Physics.Raycast(castpointL.position, tr.forward, fwdCheckLenth))
		{
			Debug.DrawRay(castpointL.position, tr.forward * fwdCheckLenth, Color.yellow);
			ret = tr.right * Rule2Multip;
		}
		else if (Physics.Raycast(castpointR.position, tr.forward, fwdCheckLenth))
		{
			Debug.DrawRay(castpointR.position, tr.forward * fwdCheckLenth, Color.yellow);
			ret = tr.right * -Rule2Multip;
		}
		else if (Physics.Raycast(castpointCen.position, tr.forward, fwdCheckLenth))
		{
			Debug.DrawRay(castpointCen.position, tr.forward * fwdCheckLenth, Color.yellow);
			ret = tr.up * Rule2Multip;
		}
		else
		{
			Debug.DrawRay(castpointL.position, tr.forward * fwdCheckLenth, Color.magenta);//no hit
			Debug.DrawRay(castpointR.position, tr.forward * fwdCheckLenth, Color.magenta);
			Debug.DrawRay(castpointB.position, tr.forward * fwdCheckLenth, Color.magenta);
			Debug.DrawRay(castpointT.position, tr.forward * fwdCheckLenth, Color.magenta);
			Debug.DrawRay(castpointCen.position, tr.forward * fwdCheckLenth, Color.magenta);
		}

		return ret;
	}


	public float Rule3Mag = 3.5f;//how forcefully they are repelled from each other when too close
	public float Rule3Bound = 1.0f;//range to stay away from each other, ~30 to make a difference

	Vector3 Rule_3()	{
		UpdateBoids();
		Vector3 c = Vector3.zero;

		foreach (GameObject g in boids)	{

			Vector3 pos = g.transform.position;

			if (pos != tr.position)
			{

				Vector3 diff = pos - tr.position;

				if ((diff).magnitude < Rule3Bound)
				{ // Boids try to keep a small distance away from other boids).  
					c -= diff.normalized * Rule3Mag;
				}
				else
				{
					c += diff.normalized * Rule3Mag; //Boids try to keep with other boids).  
				}
			}

			//Debug.DrawLine( tr.position , pos , Color.green );
		}
		//Debug.DrawRay( tr.position , c , Color.green ) ;

		return c * Rule3Mag;
	}

	//for fleeing
	Vector3 AvoidBoids()	{
		UpdateBoids();
		Vector3 c = Vector3.zero;
		foreach (GameObject g in boids)
		{

			Vector3 pos = g.transform.position;

			if (pos != tr.position)
			{

				Vector3 diff = pos - tr.position;

				if (diff.magnitude < Rule3Bound)
				{ // Boids try to keep a small distance away from other boids).  
					c -= (pos - tr.position);
				}
			}
		}
		return c * Rule3Mag;
	}


	Vector3 FindFlockAveragePos(){
		Vector3 avg = Vector3.zero;

		foreach (GameObject g in boids)
		{
			avg += g.transform.position;
		}

		return avg;
	}




	private int hitsSince = 0;
	private float nexthit;


	// add/decr hp, also tracks frequency of being hit
	public void HpModify(float add)	{
		hp += add;

		if (Time.time < nexthit)
		{
			hitsSince++;
		}
		else
		{
			hitsSince = 0;
		}

		nexthit = Time.time + 2;

	}


	public void ApplyDamage(float damage)	{
		float remain = shield_pow - damage;
		shield_pow -= damage;

		if (shield_pow < 0)
		{
			shield_pow = 0;
		}

		if (remain < 0)
		{
			HpModify(remain);
		}

		if (hp < 0)
		{
			Defeat();
		}
	}


	public void ApplyDamage(int damage)
	{ //int compatable
		float bob = damage;
		ApplyDamage(bob); //call float version
	}

	public void ApplyDamageFront(float damage)
	{

		if (damageable)
		{
			float remain = shld_f_pow - damage;
			shld_f_pow -= damage;

			if (shld_f_pow < 0)
			{
				shld_f_pow = 0;
			}
			if (remain < 0)
			{
				HpModify(remain);
			}

			if (hp < 0)
			{
				Defeat();
			}
		}
	}

	public void ApplyDamageRear(float damage)
	{

		if (damageable)
		{
			float remain = shld_b_pow - damage;
			shld_b_pow -= damage;

			if (shld_b_pow < 0)
			{
				shld_b_pow = 0;
			}
			if (remain < 0)
			{
				HpModify(remain);
			}

			if (hp < 0)
			{
				Defeat();
			}
		}
	}


	public ParticleSystem fire;

	public void Defeat()
	{

		if (damageable)
		{
			ChangeTag("Untagged");
			damageable = false;
			destroyed = true;

			rb.drag = 0.01f;
			rb.angularDrag = 0.01f;

			tr.tag = "Untagged";

			if (fire){
				var em = fire.emission;
				em.enabled = true;
			}
				
//				fire.enableEmission = true;

			//add random torque to ship
			rb.AddTorque(Random.Range(-2200, 2200), Random.Range(-2200, 2200), Random.Range(-1200, 2200), ForceMode.Impulse);


			//notify ships roster
			ShipsList lis = GameObject.FindObjectOfType<ShipsList>();

			if (lis != null)
			{
				//lis.RefreshShips();
				if (teamTag == "EnemyTeam1")
				{
					lis.ShipDestroyed(1, 1);
				}
				else if (teamTag == "EnemyTeam2")
				{
					lis.ShipDestroyed(2, 2);
				}
			}

			StartCoroutine(WaitToDestroy());
		}


	}

	public IEnumerator WaitToDestroy()
	{
		yield return new WaitForSeconds(3.0f);
		Detonate();
	}


	public int GetTeam()
	{
		return 0;
	}

	//Update tag for others scripts to find
	public void ChangeTag(string newtag)
	{
		if (!destroyed)
			targetTagObject.tag = newtag;
	}

	public void SetTeamTag(string newtag)
	{

		tr.tag = newtag;
	}

	public void RevertTag()
	{
		if (!destroyed)
			targetTagObject.tag = oldtag;
	}



	public float explosionRadius = 5.0f;
	public float explosionDamage = 100.0f;
	public GameObject explosionObj;


	public void Detonate()
	{

		print( transform.name + " destroyed" );
		var explosionPosition = tr.position;

		Collider[] colliders = Physics.OverlapSphere(explosionPosition, explosionRadius);


		foreach (Collider hit in colliders)
		{

			var closestPoint = hit.ClosestPointOnBounds(explosionPosition);  // Calculate distance from the explosion position to the closest point on the collider
			var distance = Vector3.Distance(closestPoint, explosionPosition);
			var hitPoints = 1.0 - Mathf.Clamp01(distance / explosionRadius);   // The hit points we apply fall decrease with distance from the explosion point
			hitPoints *= explosionDamage;

			hit.BroadcastMessage("ApplyDamage", hitPoints, SendMessageOptions.DontRequireReceiver);

		}

		Instantiate(explosionObj, explosionPosition, tr.rotation);
		Destroy(gameObject);
	}


	public Color colour = Color.magenta;
	void OnDrawGizmosSelected()
	{
		Gizmos.color = colour;
		Gizmos.DrawWireSphere(transform.position, attackRange);


	}
}











/*
	public float correctionMult = 100;
	public void FixedUpdate (){

		var angularVelocityError = rb.angularVelocity * -1;
		//Debug.DrawRay( tr.position, rb.angularVelocity * 10, Color.grey );
		
		var angularVelocityCorrection = angularVelocityController.Update( angularVelocityError , Time.deltaTime );
		//Debug.DrawRay( tr.position, angularVelocityCorrection , Color.green );
		
		rb.AddTorque(angularVelocityCorrection);
		
		//var desiredHeading = target.position - transform.position;
			
		var headingError = Vector3.Cross( tr.forward , velo );
		var headingCorrection = headingController.Update( headingError , Time.deltaTime );
		
		rb.AddTorque( headingCorrection * correctionMult );
		//rb.AddRelativeForce( Vector3.forward * speed );
	}
	*/



