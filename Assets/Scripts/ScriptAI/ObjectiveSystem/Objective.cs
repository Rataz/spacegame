﻿using UnityEngine;
using System.Collections;


/*
 * 
 * */
public class Objective : MonoBehaviour {

	public bool complete;
	public bool visible;

	public virtual bool IsVisible(){
		return false;
	}

	public virtual void Activate(){

	}

	public virtual bool CheckComplete(){
		return false;
	}



}
