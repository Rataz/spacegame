﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * Sets a transform as a target for attack
 * 
 * */
public class AttackObjective  {

	public string name = "";

	public Transform target;

	public AttackObjective( string nam , Transform dtarget ){
		name = nam;
		target = dtarget;
	}

	public AttackObjective( string nam  ){
		name = nam;
	}

	/*
	 * Used by objective system to tell if the objective has been completed or not
	 * //return true if target has been destroyed (made null)
	 * */	
	public bool IsPending(){

		bool ret = false;

		if (target == null) {
			ret = false;
		} else {
			ret = true;
		}

		return ret;
	}





}
