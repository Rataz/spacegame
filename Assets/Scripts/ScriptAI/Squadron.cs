﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Ship coordination and squadron marking

/*
 * Attach this to an empty and create a prefab for spawnpoints to use
 * 
 * */
public class Squadron : MonoBehaviour {

	private Vector3 centroidTotal = Vector3.zero;
	private Vector3 centroid = Vector3.zero; 
	private int numberOfElements = 0;



	public List<SmallShipFacade> ships = new List<SmallShipFacade>();
	
	public string squadronName = "unknown squadron";
	
	public string objectivename="none";

	public AttackObjective attackobjective;

	


	void Awake(){
		//prevent squadron being counted destroyed if not created dynamically
		numberOfElements = ships.Count;
	}

	public void Update(){
		transform.position = GetCentroid();
	}




	//give this squadron an objective object to follow
	public void AssignObjective(AttackObjective obj)
	{
		attackobjective = obj;
		objectivename = obj.name;
	}

	public AttackObjective GetObjective()
	{
		return attackobjective;
	}


	/*
	 * Use to ask this script if the objective is complete or not
	 * */
	public bool IsObjectivePending()
	{
		bool ret = false;

		if (attackobjective != null)
		{
			ret = attackobjective.IsPending();
		}

		return ret;
	}
	

	public void AddShip( SmallShipFacade member ){
		ships.Add( member );
		numberOfElements++;
	}

	//return how many members of squadron are alive, some list elements may be null
	public int NumberOfSurvivingMembers(){
		int negativeElements =0;
		for( int i=0; i<ships.Count; i++){
			if (ships [i] != null) {
			}
			else{
				negativeElements++;
			}
		}
		int totalElements = numberOfElements-negativeElements;
		return totalElements;
	}

	//return the average position of all squadron members
	public Vector3 GetCentroid(){
		centroidTotal = Vector3.zero;
		Vector3 ret =  Vector3.zero;
		int negativeElements =0;
		for( int i=0; i<ships.Count; i++){
			if( ships[i] != null )
				centroidTotal+= ships[i].GetPosition();
			else{
				negativeElements++;
			}
		}
		int totalElements = numberOfElements-negativeElements;
		if( totalElements == 0){
			print(squadronName + " destroyed");
			Del();
		}
		else{
			float x = centroidTotal.x/totalElements;
			float y = centroidTotal.y/totalElements;
			float z = centroidTotal.z/totalElements;
			centroid = new Vector3( x , y , z );
			ret = centroid;
		}
		return ret; 
	}

	public void Del(){
		Destroy( gameObject );
	}

	//get average velocity of whole squadron
	public Vector3 GetVelocity(){
		Vector3 velo = Vector3.zero;
		int items = 0;
		foreach( SmallShipFacade ship in ships ){
			if( ship != null ){
				Rigidbody r ;
				r = ship.GetRigidbody();
				if( r!=null ){
					velo+=r.velocity;
					items++;
				}
			}
		}
		if( items>0 ){
			velo.x = velo.x/items;
			velo.y = velo.y/items;
			velo.z = velo.z/items;
		}
		return velo;
	}

	public void SetMarkerColour( Color markerColor){
		//_Color
		//_EmissionColor
		//_SpecColor
		gameObject.GetComponent<Renderer>().material.SetColor( "_Color", markerColor );
		gameObject.GetComponent<Renderer>().material.SetColor( "_EmissionColor", markerColor );
	}

}
