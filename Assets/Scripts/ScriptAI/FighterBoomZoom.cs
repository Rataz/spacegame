﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(ShipUtils))]
[RequireComponent(typeof(SmallShipFacade))]




public class FighterBoomZoom : MonoBehaviour {


	private SmallShipFacade shipFacade;
	private ShipUtils shipUtils;
	private Transform tr;
	
	public float fleeRange = 70;

	public float attackRangeMax = 150;
	public float attackRangeMin = 60;

	public float fireAngle = 45;
	public float attackFireRange = 150;

	public Transform target;
	public ShipWeapon gun;
	public float projectileSpeed = 20;//for leading target
	
	public string teamTag = "EnemyTeam2";
	public string teamTagEnemy = "EnemyTeam1";
	
		
	void Awake() {
		tr = transform;
		shipFacade = gameObject.GetComponent<SmallShipFacade>();
		shipUtils = gameObject.GetComponent<ShipUtils>();
	}


	void Start() {
		shipFacade.ThrottleSet(100);
	}

	void Update() {
		target = shipUtils.InRangeAndFov(GameObject.FindGameObjectsWithTag(teamTagEnemy), attackRangeMin, attackRangeMax, 45, tr.position);

		if( target != null){
			shipUtils.SimpleGunAttack(target , projectileSpeed , attackFireRange, fireAngle , gun);
		}
		else{
			//Return to the middle of the action
			if( Vector3.Distance(transform.position, Vector3.zero ) > 500 ){
				shipUtils.BanknYankTo(Vector3.zero);
				target = null;
			}
		}
		
	}

//	private bool isfinished = false;
	public IEnumerator countdown(){
		yield return new WaitForSeconds(1);
	}
	
	/*
	//which behavior to use
	private IEnumerator DecideMode() {

		//bool haveTarget = ChooseTarget();// update target

		if( target ){
			
			dist = Vector3.Distance( tr.position, target.position);

			if ( dist < fleeRange ){
				mode = 2; //Flee
				yield return new WaitForSeconds(1);//cooldown till change mode
			}
			else if ( dist < attackRange ){// 
				mode = 1;  //Attack
				yield return new WaitForSeconds(2);
			}
			else{
				mode = 0; //Flock
				yield return new WaitForSeconds(1);
			}
		}
		else{
			mode = 4;
			yield return new WaitForSeconds(4);
			mode = 3;
		}


		yield return new WaitForSeconds(1);
		StartCoroutine(DecideMode());
	}*/
	
}
