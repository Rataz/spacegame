﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(ShipUtils))]
[RequireComponent(typeof(SmallShipFacade))]

/**
 * An AI that orientates to the up vector of a marker, and releases a bomb when raycast sees a target
 * 
 * */
public class BomberAI : MonoBehaviour {

	private SmallShipFacade shipFacade;
	private ShipUtils shipUtils;
	private Transform tr;
	private Rigidbody rb;
	private Transform target;	
	private Squadron squadron;//squadron this ship is in


	//public float speed = 5.0f;
	public float attackRangeMaximum = 150;
	public float attackRangeMinimum = 60;	
	public float fireAngle = 45;
	
		
	public ShipWeapon gun;
	public MissleLauncher misslelauncher;
	
	public float projectileSpeed = 20;//for leading target

	public string teamTagEnemy = "EnemyTeam1";

	public Transform[] collisionDetectRaycastPoints;
	
	public float missleAttackRange = 300;
	

	//for ywing like craft
	public bool levelBomber = false;
	public float bombheight = 50;
	
	//downward launch speed of bomb
	public float bomblaunchspeed = 30;
	
	//select layers to check collision against
	public LayerMask myLayerMask;


	void Awake() {
		tr = transform;
		rb = gameObject.GetComponent<Rigidbody>();

		shipFacade = gameObject.GetComponent<SmallShipFacade>();
		shipUtils = gameObject.GetComponent<ShipUtils>();
	}

	void Update() {
		ChooseTarget();
		Vector3 obstacleAvoidDirection = shipUtils.CheckAhead(collisionDetectRaycastPoints , myLayerMask);

		if ( obstacleAvoidDirection != Vector3.zero ){
			shipUtils.TurnToDirection(obstacleAvoidDirection); 
		}
		else {
			if (target != null)
			{
				if (levelBomber) {
					AttackLevel();
				}
				else
					shipUtils.AttackMissle(target , misslelauncher , fireAngle , missleAttackRange, projectileSpeed);
			}
			else
			{
				if (Vector3.Distance(tr.position, Vector3.zero) > 500)
				{
					shipUtils.BanknYankTo(Vector3.zero);
					target = null;
				}				
			}


			shipFacade.ThrottleSet(100);
		}
	}

	//	private bool isfinished = false;
	public IEnumerator countdown(){
		//	isfinished = false;
		yield return new WaitForSeconds(1);
		//	isfinished = true;
	}
	
	

	/** 
	 * level bombing 
	 * use a raycast to predict if will hit
	 */
	public void AttackLevel()
	{
		Vector3 tarpos = target.position + new Vector3(0, bombheight, 0);

		GroundTarget groundTarget = target.gameObject.GetComponent<GroundTarget>();
		if (groundTarget != null) {
			tarpos = target.position + new Vector3(0, groundTarget.attackHeight , 0);
		}
		
		//target is close
	//	float targetRange = (Vector3.Distance(tarpos, tr.position));
		Vector3 launchVector = transform.TransformDirection( new Vector3(0, -bomblaunchspeed, rb.velocity.magnitude) );

		RaycastHit hit;

		if (Physics.Raycast(transform.position, launchVector, out hit, 500)) {
			if (hit.collider.transform.tag.Equals("ground_target")){
				gun.transform.rotation = Quaternion.LookRotation(launchVector, Vector3.up);
				gun.Fire();
			}
		}

		if (Physics.Linecast(transform.position, target.position, out hit))
		{
			//cant see target

		}
		else
		{
			//can see target
			shipUtils.BanknYank2(tr.InverseTransformPoint(tarpos) , Vector3.up );
		}
	}
			

	//return true if a target found, false if no targets
	private bool ChooseTarget() {

		bool ret = false;
		
		//set previous target
		Transform prevtar = null;

		if( target !=null)	prevtar = target;
		

		GameObject[] enemies = GameObject.FindGameObjectsWithTag(teamTagEnemy);

		if (enemies.Length > 0)
		{
			Transform target = shipUtils.InRangeAndFov(enemies, attackRangeMinimum, attackRangeMaximum,45,tr.position);			
			shipUtils.TargetInform(prevtar, target);
			ret = true;
		}
		
		return ret;
	}

	


	public Squadron GetSquadron()
	{
		return squadron;
	}

	public void SetSquadron(Squadron squad)
	{
		squadron = squad;
	}
	

	


}
