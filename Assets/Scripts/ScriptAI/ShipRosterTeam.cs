﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/**
Put one of this script on an empty gameobject (one in the scene for each team)


	WIP

Maintains list of small craft on a single team

	Can assign an existing ship to a player

*/

public class ShipRosterTeam : MonoBehaviour {

	[Tooltip("Team cooresponding to this roster")]
	public int team = 0;

	[Tooltip("Auto assigned, for viewing only")]
	public List<SmallShipFacade> ships;

	[Tooltip("Assign this so player can take over an AI ship")]
	public PlayerSceneInfo playerSceneInfo;

	//current ship in list selected by player
	private int selected = 0;

	void Start () {
		GetTeamShips(  );
		StartCoroutine (TeamUpdate());
	}
	
	public int ShipCount(){		
		return ships.Count;
	}

	//regular refresh team members
	IEnumerator TeamUpdate(){
		
		yield return new WaitForSeconds (1);
		GetTeamShips ();
		TeamUpdate ();
	}

	public List<Transform> GetShipsTransforms()
	{
		SmallShipFacade[] tmp = GameObject.FindObjectsOfType<SmallShipFacade>();
		List<Transform> ships = new List<Transform>();
		foreach( SmallShipFacade x in tmp)
		{
			ships.Add(x.transform);
		}
		return ships;
	}

	public void GetAllShips(  ){
		SmallShipFacade[] tmp = GameObject.FindObjectsOfType<SmallShipFacade>();
		ships.Clear();

		ships.AddRange( tmp );
	}

	public void GetTeamShips(  ){
		
		SmallShipFacade[] tmp = GameObject.FindObjectsOfType<SmallShipFacade>();
		ships.Clear();

		foreach( SmallShipFacade ship in tmp ){
			if( ship.GetTeam() == team ){
				ships.Add( ship );
			}
		}
	}


	public void RemoveShip( SmallShipFacade ship ){
		ships.Remove( ship );
	}

	//add the ship if it is on this team
	public void AddShipTeamLimited( SmallShipFacade ship ){
		if( ship.GetTeam() == team ){
			AddShip( ship );
		}
	}


	public void AddShip(  SmallShipFacade ship ){
		ships.Add( ship );
	}

	public void NotifyDead( int shipteam ){
		if( shipteam == team ){
			
		}
	}



	/**
	 * Puts one of the ships on this team under player control
	 * */
	public void GivePlayerAShip(){

		//check if index is too much
		if( selected > ships.Count-1 ){
			selected = 0;
		}

		//1 = ai
		//2 = player

		//set current ship to ai
		ships[selected].SetAIorPlayer( 1 );

		//increment index
		selected++;
		if( selected > ships.Count-1 ){
			selected = 0;
		}

		//set to player
		ships[selected].SetAIorPlayer( 2 );

		Camera.main.transform.parent = ships[selected].cameraMount;
		Camera.main.transform.localPosition = Vector3.zero;
		Camera.main.transform.rotation = ships[selected].cameraMount.rotation;

		playerSceneInfo = GameObject.FindObjectOfType<PlayerSceneInfo>();
		playerSceneInfo.player = ships[selected].transform;

	}


}
