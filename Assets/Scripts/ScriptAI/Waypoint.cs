﻿using UnityEngine;
using System.Collections;

public class Waypoint : MonoBehaviour {

	public Waypoint next;

	public Waypoint first;

	//how far to one side position can be
	public float randomVariation = 5.0f;


	//auto assigned
	public Vector3 pos;

	void Awake () {
		pos =  transform.position;
	}

	public Vector3 GetUp(){
		return transform.up;
	}

	public Vector3 GetPos(){
		Random.InitState(System.DateTime.Now.Millisecond);
		Vector3 offset1 = new Vector3( Random.Range( 0, randomVariation) ,0,0);
		Vector3 p1 = transform.position + transform.rotation*offset1;

		return p1;
	}

	void OnDrawGizmos(){

		Gizmos.color = Color.black;

		if( next != null )
			Gizmos.DrawLine( transform.position , next.transform.position);

		Vector3 offset1 = new Vector3(randomVariation,0,0);
		Vector3 p1 = transform.position + transform.rotation*offset1;

		Gizmos.DrawLine( transform.position , p1);

		Gizmos.color = Color.red;
		Gizmos.DrawSphere(  p1 , 1 );

	}

}
