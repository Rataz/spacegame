﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


/*
 * 
 *  Put this script on an empty in the scene
 * 
 * 
 * Creates clusters for flak turrets to shoot at
 * 
 * */
public class ClusterTargeter : MonoBehaviour {
	
	public float margin = 300;


	//public Transform markerPrefab;
	[Tooltip("Marker object for squadron centers (not used right now)")]
	public Transform prefab;

	//Create list of clusters	
	private List<ShipCluster> clusters = new List<ShipCluster>();


	void Start () {
	}


	void Update () {
		Cluster();

		/*
		if ( Input.GetKeyUp( KeyCode.L )){
			//Debug.Log("asdasd");
			Cluster();
		}	*/
	}
	
	public List<ShipCluster> GetClusters() {
		return clusters;
	}

	public void Cluster(){
		SmallShipFacade[] ships = FindObjectsOfType<SmallShipFacade>();

		//find centroid of each cluster
		foreach (ShipCluster cluster in clusters)
		{
			cluster.Clear();
			cluster.inUse = false;
		}

		//reset all the ships
		for (int i = 0; i < ships.Length; i++)
		{
			if( ships[i] !=null)
				ships[i].hasCluster = false;
		}


		//iterate over all cluster elements (ships)
		for (int i = 0; i < ships.Length; i++)
		{

			ShipCluster currentCluster;
			SmallShipFacade ship = ships[i];

			//if not currently in a cluster, start a new cluster for the ship
			if (!ships[i].hasCluster)
			{
				bool found = false;
				foreach (ShipCluster cluster in clusters)
				{
					if (!cluster.inUse)
					{
						currentCluster = cluster;
						cluster.AddElement(ship);
						ship.SetClust(currentCluster);
						found = true;
						break;
					}
				}

				if (!found)
				{
					currentCluster = new ShipCluster();
					currentCluster.AddElement(ship);

					//draw markers for debug (nxt 3 lines)
					//Transform marker = Instantiate(prefab, Vector3.zero, Quaternion.identity) as Transform;
					//marker.gameObject.GetComponent<Renderer>().material.color = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));
					//currentCluster.marker = marker;
					
					clusters.Add(currentCluster);
					ship.SetClust(currentCluster);

					//Debug.Log("asd");
				}
			}

			ShipCluster cl = ship.GetCluster();

			//iterate over all remaining elements after the current one and assign clusters
			for (int j = i + 1; j < ships.Length; j++)
			{
				if (!ships[j].hasCluster)
				{
					float dist = Vector3.Distance(ships[i].GetPosition(), ships[j].GetPosition());
					//add to the current cluster if close enough
					if (dist < margin)
					{
						ships[j].SetClust(cl);
						cl.AddElement(ships[j]);
					}
				}
			}

			foreach (ShipCluster cluster in clusters)
			{
				cluster.CalcCentroid();
			}
		}	
		
		
	}


}
