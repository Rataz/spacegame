﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * WIP
 * 
 * Attackers should tell the gameobject that they are attacking it
 * usually bombers
 * 
 * See BomberAI.cs choosetarget
 * 
 * Defenders can get all attackers of the target they should defend
 * usually fighters
 * 
 * */

public class GroundTarget : MonoBehaviour {

	[Tooltip("View only, ships attacking this gameobject")]
	public List<GameObject> attackers = new List<GameObject> ();

	private List<AttackObjective> objectives = new List<AttackObjective> ();

	[Tooltip("1 or 2")]
	public int team = 0 ;


	public int priority = 0;

	//if being attacked, tell boss about it, boss may send help if priority is high enough
	public bool pushToBoss = true;

	//items to 
	public List<AttackObjective> defenseObjectives = new List<AttackObjective>();


	[Tooltip("to attack this target how high should bomber be?")]
	public float attackHeight = 10;



	public GameObject GetClosestAttacker (){
		return this.gameObject;
	}

	public void SortAttackers (){
	
	}

	public void AddAttackTicket( AttackObjective att ){
		objectives.Add (att);
	}

	public List<GameObject> GetAllAttackerList(){
		return attackers;
	}

	//tell this object it is being attacked
	public void AttackingYou( GameObject attacker ){
		if (attackers.Contains (attacker)) {
			print ("duplicate");
		} else {
			attackers.Add (attacker);
		}
		
		if (pushToBoss) {
			Boss myboss = GameObject.FindObjectOfType<GlobalSceneInfo> ().GetBossOfTeam ( team );
			myboss.IAmBeingAttacked (transform, attacker.transform, 1);
		}
	}

	//for attackers to say they have broken off
	public void NotAttackingYouAnymore( Transform attacker){
		attackers.Remove (attacker.gameObject);


		AttackObjective objective=null;

		//set objective complete
		foreach (AttackObjective x in objectives) {
			if (x.target = attacker) {
				x.target = null;
				objective = x;
			}
		}

		//tell boss objective is complete
		Boss myboss = GameObject.FindObjectOfType<GlobalSceneInfo> ().GetBossOfTeam ( team );
		myboss.ObjectiveComplete (objective);

	}

	public bool IsAttackerStillAttacking( Transform attacker ){
		bool ret=false;
		if (attackers.Contains (attacker.gameObject)) {
			ret = true;
		}
		return ret;
	}

}
