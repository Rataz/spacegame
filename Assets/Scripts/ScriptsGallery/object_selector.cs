﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class object_selector : MonoBehaviour {

	private int last_value;
	public Transform object_displayed;
	private int sub_cat;
	public Dropdown my_dropdown;
	public select_persist persistance;
	public Text display_text;
	public bool control_selector;
//	private bool Ist_time = false;

	public void Start()
	{
		change_team(0); //ensures everything is up to date on scene start, removes need to pre set the dropdown possibilities
	}

	public void change_team(int team) // change the sub-category we look in
	{
		sub_cat = team;
		change_object(last_value);
		update_text(sub_cat);
		//print(team); 
	}


	public void change_object(int value){

		last_value = value;
		//print("change");
		Transform new_object ;
		/*
		if (Ist_time == false)
		{
			Destroy(transform.GetChild(sub_cat).GetChild(0).gameObject);
			Ist_time = true;
			new_object = transform.GetChild(sub_cat).GetChild(value-1);
		}
		*/
		 if(value < transform.GetChild(sub_cat).childCount) // in-case of one team being larger than the other, it defaults to avoid an error.
		{
				new_object = transform.GetChild(sub_cat).GetChild(value); //set the value
		}
		else{
			new_object = transform.GetChild(sub_cat).GetChild(transform.GetChild(sub_cat).childCount-1);
			update_text(sub_cat); // updates the text
		}

		object_displayed.gameObject.SetActive(false); //display the new object
		new_object.gameObject.SetActive(true);
		object_displayed = new_object;
		if(display_text)
		{
		display_text.text = new_object.GetChild(0).GetComponent<Text>().text; //this changes the description
		}
		if(control_selector == false)
		{
		persistance.int_select(value); //tell the persistance the new values
		persistance.ship_select(object_displayed.name);
		persistance.team_select(sub_cat);
		}
		else
		{
			persistance.control_select(value+1);
		}
	}

	public void update_text(int holder) // child index of the holder (sub-category)
	{
		my_dropdown.ClearOptions();
		foreach(Transform selectable in transform.GetChild(holder)){
			my_dropdown.options.Add(new Dropdown.OptionData(selectable.name)); //add the new options
		}
		my_dropdown.RefreshShownValue();
	}



}
