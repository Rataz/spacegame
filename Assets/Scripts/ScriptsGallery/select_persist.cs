﻿using UnityEngine;
using System.Collections;

public class select_persist : MonoBehaviour {

	public Transform thing;
	public int number;
	public string ship; //things we want to store
	public int team;

	public int controlScheme = 1;

	//functions so that a ui button can change the value
	public void team_select (int team_t)
	{
		team = team_t;
	}

	public void thing_select (Transform thing_t)
	{
		thing = thing_t;
	}

	public void int_select (int int_t)
	{
		number = int_t;
	}

	public void ship_select (string ship_t)
	{
		ship = ship_t;
	}

	public void control_select( int control )
	{
		controlScheme = control;
	}

	void Awake() {
		// Do not destroy this game object:
		DontDestroyOnLoad(this);
	}


}
