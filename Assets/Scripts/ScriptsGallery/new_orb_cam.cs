﻿using UnityEngine;
using System.Collections;

public class new_orb_cam : MonoBehaviour
{

	//The target of the camera. The camera will always point to this object.
	public Transform _target;

	private bool over_ui ;

	//The default distance of the camera from the target.
	public float _distance = 20.0f;

	//Control the speed of zooming and dezooming.
	public float _zoomStep = 1.0f;

	//The speed of the camera. Control how fast the camera will rotate.
	public float _xSpeed = 1f;
	public float _ySpeed = 1f;

	//The position of the cursor on the screen. Used to rotate the camera.
	private float _x = 0.0f;
	private float _y = 0.0f;

	//clamp limits
	public float yMinLimit = 0f;
	public float yMaxLimit = 85f;

	//zoom limits
	public float zoom_min = 0f;
	public float zoom_max = 85f;

	//Distance vector. 
	private Vector3 _distanceVector;

	/**
  * Move the camera to its initial position.
  */
	void Start ()
	{
		_distanceVector = new Vector3(0.0f,0.0f,-_distance);

		Vector2 angles = this.transform.localEulerAngles;
		_x = angles.x;
		_y = angles.y;

	}

	/**
  * Rotate the camera or zoom depending on the input of the player.
  */
	void Update()
	{
		if ( _target )
		{
			this.RotateControls();
			this.Zoom();
		}

		this.Rotate(_x,_y);
	}


  //Rotate the camera when the first button of the mouse is pressed.
  
	void RotateControls()
	{

		if ( Input.GetMouseButton(2) && over_ui == false)
		{
			_x += Input.GetAxis("Mouse X") * _xSpeed;
			_y += -Input.GetAxis("Mouse Y")* _ySpeed;
			_y = ClampAngle(_y,yMinLimit,yMaxLimit); //limit the angle


		}

	}

	/**
  * Transform the cursor mouvement in rotation and in a new position
  * for the camera.
  */
	void Rotate( float x, float y )
	{
		//Transform angle in degree in quaternion form used by Unity for rotation.
		Quaternion rotation = Quaternion.Euler(y,x,0.0f);

		//The new position is the target position + the distance vector of the camera
		//rotated at the specified angle.
		Vector3 position = rotation * _distanceVector + _target.position;

		//Update the rotation and position of the camera.
		transform.rotation = rotation;
		transform.position = position;
	}

	/**
  * Zoom or dezoom depending on the input of the mouse wheel.
  */
	void Zoom()
	{
		if ( Input.GetAxis("Mouse ScrollWheel") < 0.0f  && over_ui == false && _distance < zoom_max)
		{
			this.ZoomOut();
		}
		else if ( Input.GetAxis("Mouse ScrollWheel") > 0.0f  && over_ui == false && _distance > zoom_min)
		{
			this.ZoomIn();
		}

	}

	/**
  * Reduce the distance from the camera to the target and
  * update the position of the camera (with the Rotate function).
  */
	void ZoomIn()
	{
		_distance -= _zoomStep;
		_distanceVector = new Vector3(0.0f,0.0f,-_distance);
		this.Rotate(_x,_y);
	}

	/**
  * Increase the distance from the camera to the target and
  * update the position of the camera (with the Rotate function).
  */
	void ZoomOut()
	{
		_distance += _zoomStep;
		_distanceVector = new Vector3(0.0f,0.0f,-_distance);
		this.Rotate(_x,_y);
	}
	// clamp function
	public static float ClampAngle(float angle, float min, float max) //this allows us to limit the y directions
	{
		if (angle < -360F)
			angle += 360F;
		if (angle > 360F)
			angle -= 360F;
		return Mathf.Clamp(angle, min, max);
	}


	public void in_ui (bool over)
	{
		over_ui = over; //the canvas tells the script of the mouse through this function, we will disable interaction if so
	}

} 