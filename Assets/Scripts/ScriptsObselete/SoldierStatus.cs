
using UnityEngine;
using System.Collections;

//Impact death, Hp, objective, etc
public class SoldierStatus : MonoBehaviour {
	
	public int team;

	public GameObject objective;

	public int mode;
	public GameObject closestCp;
	public bool onDefense=false;
	public bool cpTopPriority;
	
	public float deathImpactMagnitude = 10;


	public Transform target;
	
	public bool setObjectiveNullWhenReached = true;

	private SoldierDamage hpscr;

	void Start(){

		hpscr = GetComponentInChildren< SoldierDamage >();

	}


	void Update () {
	/*
		if( setObjectiveNullWhenReached ){
			if( objective ){
				if( objective.GetComponent<CommandPost>() ){
					if( objective.GetComponent<CommandPost>().myTeam == gameObject.GetComponent<SoldierStatus>().team ){
						objective=null;
					}
				}
			}
		}
		*/
	}
	
	public void UpdateObjective(GameObject obj){
		objective=obj;
	}
	
	//Impact Damage
	void OnCollisionEnter( Collision col ){

		if( col.relativeVelocity.magnitude > deathImpactMagnitude ){

			if( hpscr )
				hpscr.ApplyDamage(100);

		}

	}
	
	public GameObject GetObjective(){
		return objective;
	}

	public Transform GetTarget(){
		return target;
	}

	public void SetTarget( Transform tar ) {
		target = tar;
	}
	
}
