﻿using UnityEngine;
using System.Collections;

public class SoldierFacade : MonoBehaviour , IDamageable {


	public SoldierStatus soldierStatus;
	//public RigidbodyController rbController;
	public SoldierDamage soldierDamage;

	public WeaponSelector weapons;

	public Animator anim;
	public BulletGun gun;
	//public AimElevationV4 aimElev;

	public Transform shootpt;

	public void SelectNextWeapon(){
		if( weapons ){
			weapons.SelectNextWeapon();
		}
	}

	public void SelectPreviousWeapon(){
		if( weapons ){
			weapons.SelectPreviousWeapon();
		}
	}

	public void GetWeaponIcon(){
		if( weapons ){

		}
	}

	public void GetWeaponAmmo(){
		if( weapons ){
			
		}
	}

	public void SelectWeapon(){
		if( weapons ){
			
		}
	}

	public WeaponSelector GetWeaponSelector(){
		return weapons;
	}

	public void SetIndependentOperation( bool on ){

	}

	public void NotifyLeaderOfDeath(){

	}

	public void SetLeader(){

	}

	public void SetAimElevation( float azimuth ){
		//if( aimElev ){

		//}
	}

	public void Fire(){
		gun.Fire();
	}

	public void SetObjective( GameObject objective ){
		if( soldierStatus ){
			soldierStatus.objective  = objective;
		}
	}

	public void SetAimTarget( Vector3 pos ){
/*		if( rbController ){
			rbController.SetFaceDir( pos );
		}

		if( aimElev ){
			aimElev.SetTarget( pos );
		}*/
	}

	public void SetAimTarget( Transform transf ){
		if( soldierStatus )
			soldierStatus.SetTarget( transf );
			/*
		if( rbController )
			rbController.SetFaceDir( transf.position );*/
	}

	public void SetTarget( Transform transf ){

		if( soldierStatus )
			soldierStatus.SetTarget( transf );

		if( shootpt ){							//should move elsewhere
			shootpt.transform.LookAt( transf );
		}
		/*
		if( aimElev ){
			aimElev.SetTarget( transf );
		}*/
	}

	public void SetMoveTarget( Vector3 pos ){
		/*if( rbController )
			rbController.SetMoveDir( pos );*/
	}

	public void SetMoveTarget( Transform transf ){
		if( soldierStatus )
			soldierStatus.SetTarget( transf );

		/*if( rbController )
			rbController.SetMoveDir( transf.position );*/
	}

	public void SetMoveDir( Vector3 dir ){
		/*rbController.SetMoveDir( dir );*/
	}

	public void SetFaceDir( Vector3 dir ){
		/*if( rbController )
			rbController.SetFaceDir( dir );*/
	}

	public void SetTeam( int team ){
		if( soldierStatus )
			soldierStatus.team = team;
	}

	public int GetTeam(){
		return soldierStatus.team;
	}

	public void SetCanMove( bool tf ){

	}

	public void ApplyDamage( float damage ){
		if( soldierDamage  ){
			soldierDamage.ApplyDamage( damage );
		}

	}

	public void ApplyDamage( int damage ){
		if( soldierDamage  ){
			soldierDamage.ApplyDamage( damage );
		}
	}

	public void SetFiring(){

	}

	public void SetMoveMode(){

	}

	public void SetGun(  ){

	}


}
