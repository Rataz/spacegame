using UnityEngine;
using System.Collections;

public class SoldierDamage : MonoBehaviour , IDamageable {
	
	private double maxHp =100.0;
	public double hitPoints = 100.0;
	public Transform deadReplacement;
	public AudioClip dieSound;
	private SoldierStatus statScr;
	private SoldierFacade sf;

	public bool player = false;

	void Start(){

		maxHp = hitPoints;

		if(statScr==null){
			statScr=transform.GetComponentInChildren<SoldierStatus>();
		}
		if(statScr==null){
			statScr=transform.root.GetComponentInChildren<SoldierStatus>();
		}
	}

	public void ApplyDamage( int damage ){
		ApplyDamage ( (float)damage );
	}
	
	public void ApplyDamage ( float damage ) {
		// We already have less than 0 hitpoints, maybe we got killed already?
		if (hitPoints <= 0.0)
			return;
	
		hitPoints -= damage;
		if (hitPoints <= 0.0){
			//Detonate();
//			GameObject[] boss = GameObject.FindGameObjectsWithTag("Boss");
			
//			foreach(GameObject x in boss){
				//if(x.GetComponent<SoldierCommand>()){
//					if( x.GetComponent<SoldierCommand>().myTeam == transform.root.gameObject.GetComponent<SoldierStatus>().team ){
//						x.GetComponent<SoldierCommand>().manDown( transform.root.gameObject );
						//x.GetComponent<SoldierCommand>().manDown(GameObject);
//					}
				//x.GetComponent<SoldierCommand>().manDown(transform.root.gameObject);
				//x.GetComponent<SoldierCommand>().manDown(transform.gameObject);

				//}
//			}

	//		if( player ){
//				ProtoSpawn[] spawns = GameObject.FindObjectsOfType<ProtoSpawn>();
//				foreach( ProtoSpawn x in spawns ){
//					if( x.team == transform.root.gameObject.GetComponent<SoldierStatus>().team  )
//						x.PlayerDown();//differece from soldierdamage
//				}
		//	}

			Detonate();
			
		}
	}


	private void Detonate () {

		Destroy( gameObject );

		if( transform.parent )
			Destroy( transform.parent.gameObject );

		if (dieSound)// Play a dying audio clip
			AudioSource.PlayClipAtPoint( dieSound , transform.position);
	
		if (deadReplacement) {// Replace ourselves with the dead body
			Transform dead;
				dead = Instantiate( deadReplacement , transform.position , transform.rotation) as Transform;
				
			// Copy position & rotation from the old hierarchy into the dead replacement
			CopyTransformsRecurse(transform, dead);
		}
		
		//Destroy(transform.parent.gameObject);
	}
	
	private void CopyTransformsRecurse ( Transform src , Transform dst ) {

		dst.position = src.position;
		dst.rotation = src.rotation;

		if(dst.GetComponent<Rigidbody>())
			dst.GetComponent<Rigidbody>().velocity = transform.root.GetComponent<Rigidbody>().velocity;

		foreach ( Transform child in dst ) {
			// Match the transform with the same name
			var curSrc = src.Find(child.name);

			if (curSrc)
				CopyTransformsRecurse(curSrc, child);
		}
	}

	public int GetTeam( ){
		if( statScr )
			return statScr.team;
		else
			return 0;
	}

	public double GetHp(){
		return hitPoints;
	}

	public double GetmaxHp(){
		return maxHp;
	}

	public void SetHp(double Hp){
		hitPoints=Hp;
	}
	
	
}
