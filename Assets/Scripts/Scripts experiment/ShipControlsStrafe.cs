﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipControlsStrafe :  ShipControlsPID
{

	public float MainThrust;
	public float MainMaxV;

	//private MotionLineManager mLmanager;

	public ShipControlsStrafe()
	{
		MainThrust = 20;
		MainMaxV = 100;
		maxV = new Vector3(100, 100, 300);
		thrust = new Vector3(50000, 50000, 50000);
		Kp = new Vector3(1100, 1100, 1100);
		Ki = new Vector3(120, 120, 120);
	}

	void Start()
	{
		Initialize();
	}

	protected override void Initialize()
	{
		base.Initialize();
		//var mmLroot = GameObject.Find("MotionLineManager");
		//mLmanager = mmLroot.GetComponent<MotionLineManager>();
		//mLmanager.UpdateVelocity(rigidbody.velocity);
	}

	protected override void SetForces()
	{
		maxForces = thrust;
	}

	protected override void ApplyValues()
	{
		base.ApplyValues();
		//pControl.outputMax.z = MainThrust;
		//pControl.SetBounds();
	}

	protected override void SetVelocities()
	{
		// collect inputs
		var inputs = new Vector3(getI("Horizontal"), getI("Vertical"), getI("Power"));
		targetVelocity = Vector3.Scale(inputs, maxV);

		//take the rigidbody.angularVelocity and convert it to local space; we need this for comparison to target rotation velocities
		curVelocity = transform.InverseTransformDirection(rb.velocity);
		//mLmanager.UpdateVelocity(rigidbody.velocity);
	}

	protected override void SetOutputs()
	{
		rb.AddRelativeForce(pControl.output * Time.fixedDeltaTime, ForceMode.Impulse);
	}

	void FixedUpdate()
	{
		RCS();
	}
}