﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * Experiment with flocks using astar to navigate around obstacles
 * 
 * */
public class FlockExp1 : MonoBehaviour {


	public bool isLeader = false;

	public float weightCohesion = 1.0f;
	public float weightSeparation = 1.0f;
	public float weightSeek = 1.0f;
	public float weightAlign = 1.0f;
	public float weightSeekAim = 1.0f;
	public float weightCollisionRay = 1.0f;
	public float weightAstar = 1.0f;

	public float distanceCohesion = 10.0f;
	public float distanceSeparate = 5.0f;

	public string teamTag = "EnemyTeam2";
	public string teamTagEnemy = "EnemyTeam1";

	public float speed = 5.0f;


	public List<FlockExp1> boids;//flock members
	private GameObject[] boidsSameTarget; //flock members with the same target

	public Transform seekTarget;


	private Transform tr;
	private Rigidbody rb;

	public float turnRate = 3.0f;


	public float maxVelocityChange = 5.0f;


	public int team = 1;

	void Start () {
		tr = transform;
		rb = gameObject.GetComponent<Rigidbody>();
	}


	void Update()
	{
		if (rb.velocity.magnitude>0.25)
			tr.LookAt(tr.position + rb.velocity);	
	}


	public void UpdateBoids()
	{
		boids.Clear();
		FlockExp1[] tmp = GameObject.FindObjectsOfType<FlockExp1>() ;

		foreach(FlockExp1 boid in tmp)
		{
			if( boid.team == 1)
			{
				float angle = Vector3.Angle(tr.forward, boid.GetPosition() - tr.position);
				float dist = Vector3.Distance(tr.position, boid.GetPosition());

				if (angle < 90 || dist < 10)
				{
					boids.Add(boid);
				}
			}
		}

	}


	void FixedUpdate () {
		UpdateBoids();
		Flock();
	}


	//move according to flocking
	private void Flock()
	{

		Vector3 totalForce =
			Seek() * weightSeek
			 + SeekAim() * weightSeekAim
			 +	Cohesion() * weightCohesion
			 + Separation() * weightSeparation
			 + Align() * weightAlign
			 + AvoidCollision()  * weightCollisionRay ;
		
		Vector3 finalvector = Arrive( totalForce.normalized  )  ;
		rb.AddForce(finalvector);		
		Debug.DrawRay(tr.position, finalvector);
	}


	public Vector3 Astar()
	{
		if( seekTarget != null)
		{
			if( Physics.Linecast( tr.position , seekTarget.position ) )
			{
				AStarSpace astar = GameObject.FindObjectOfType<AStarSpace>();
				List<Vector3> path = astar.GetPath(tr.position, seekTarget.position);

				if( path.Count > 1)
				{
					float distWapt1 = Vector3.Distance(tr.position, path[0]);

					if (distWapt1 < 5.0f)
					{
						return path[1] - tr.position;
					}
					else
						return path[1] - tr.position;
				}

			}
		}
		
		return Vector3.zero;
	}


	public Vector3 AvoidCollision()
	{
		Vector3 ret = Vector3.zero;
		RaycastHit hit;

		if (Physics.SphereCast(tr.position, 2 , tr.forward, out hit, 15.0f))
		{
			//hit a collider
			ret = hit.normal;
		}

		return ret;
	}


	public float arriveRadius = 5.0f;

	public Vector3 Arrive( Vector3 steerVector )
	{
		if (seekTarget != null)
		{
			float dist = Vector3.Distance(tr.position, seekTarget.position);

			if( dist< arriveRadius)
			{
				float proportion = (dist-1) / arriveRadius;
				steerVector *= proportion;
			}

		}
		return steerVector;
	}


	public float distanceSeekAim = 10.0f;
	public float angleSeekAim = 5.0f;

	Vector3 SeekAim()
	{
		Vector3 result = Vector3.zero;		
		if (seekTarget != null)
		{
			float dist = Vector3.Distance(tr.position, seekTarget.position);
			float angle = Vector3.Angle(tr.forward, seekTarget.position - tr.position);
			
			if( dist < distanceSeekAim  && dist > 5)
			{
				if( angle < angleSeekAim)
				{
					result = seekTarget.position - tr.position;
					result = result.normalized;
					Debug.DrawRay(tr.position, rb.velocity, Color.black);
				}				
			}
		}
		return result;
	}


	Vector3 Seek()
	{
		Vector3 result = Vector3.zero;

		if (Physics.Linecast(tr.position, seekTarget.position))
		{
			if(seekTarget != null)
			{
				return Astar();
			}
			else
			{
				return Vector3.zero;
			}
			
		}
		else { 
			if (seekTarget != null)
			{
				float dist = Vector3.Distance(tr.position, seekTarget.position);

				result = seekTarget.position - tr.position;
				result = result.normalized;

				if (dist < 7)
				{
					result = Vector3.zero;
				}
			}

			return result;
		}
	}


	Vector3 Cohesion()
	{
		Vector3 steer = Vector3.zero;
		Vector3 summ = Vector3.zero;
		int count = 0;

		foreach (FlockExp1 g in boids)
		{
			Vector3 pos = g.GetPosition();

			if (pos != tr.position)
			{
				float dist = Vector3.Distance(tr.position, g.GetPosition());

				if (dist < distanceCohesion)
				{
					summ += pos;
					count++;
				}
				
			}
		}		

		if( count>0)
		{
			summ /= count;
			steer = summ - tr.position;
			steer = steer.normalized;
		}

		return steer;
	}

	
	Vector3 Separation()
	{
		
		Vector3 steer = Vector3.zero;
		int count = 0;

		foreach (FlockExp1 g in boids)
		{
			Vector3 pos = g.GetPosition();

			if (pos != tr.position)
			{
				float dist = Vector3.Distance(tr.position, g.GetPosition());

				if(dist < distanceSeparate)
				{
					Vector3 diff = tr.position - g.GetPosition();
					diff = diff.normalized;
					diff = diff/dist;
					steer += diff;
					count++;
				}
			}
		}

		if(count > 0)
		{
			steer /= count;
		}

		//return Vector3.zero;
		return steer.normalized;
	}
	

	Vector3 Align()
	{
		Vector3 summ = Vector3.zero;
		int count = 0;

		foreach (FlockExp1 g in boids)
		{
			Vector3 velo = g.GetVelocity();

			if (velo != tr.position)
			{
				//not counting self
				summ += velo;
				count++;
			}
		}
		summ = summ / count;
		summ = summ.normalized * speed;

		Vector3 steer = summ - rb.velocity;
		return steer.normalized;
	}


	/** chase enemies */
	Vector3 ChaseClosest()
	{
		GameObject[] enemies = GameObject.FindGameObjectsWithTag(teamTagEnemy);

		Vector3 tar = Vector3.zero;
		float closest = Mathf.Infinity;

		foreach( GameObject x in enemies)
		{
			float dist = Vector3.Distance(tr.position, x.transform.position);

			if (dist < closest)
			{
				tar = x.transform.position;
				closest = dist;
			}			
		}
		return  tar;

	}




	public Vector3 GetVelocity()
	{
		return rb.velocity;
	}

	public Vector3 GetPosition()
	{
		return tr.position;
	}



}
