﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]

/*
 * Experimental script to create a radar from a particle system
 * 
 * Position a particle system just in front of the radar screen
 * 
 * todo, add an object to 'measure' from, more flexibility, more efficient about params passed
 * 
 * */
public class ParticlePositioner : MonoBehaviour {
		
	ParticleSystem ourParticleSystem;
	ParticleSystem.Particle[] particles;

	[Space(20)]


	//Scale of the radar compared to the real world
	private float mapScale = 0.3f;
	
	[Tooltip("Physical Radius of the radar screen")]
	public float physicalSize = 0.01f;

	[Tooltip("What distance is represented by the edge of the radar " +
		"beyond this point blips will cling to the edge of the radar")]
	public float maxDist = 10;


	public enum targetfilter : int { targetsInFront , targetsbehind, all};
	
	[Tooltip("Use to filter out some targets")]
	public targetfilter displayOnly = targetfilter.targetsInFront;

	private int maxparticles = 0;

	[Tooltip("Optional.  Set a different object to measure from, " +
		"ex if the radar screen is tilted compared to the ship body")]
	public Transform radarForward;






	void Start(){
		mapScale = physicalSize / maxDist;
		ourParticleSystem = GetComponent<ParticleSystem>();
		maxparticles = ourParticleSystem.main.maxParticles;
		ourParticleSystem.Play();

		if( radarForward == null)
		{
			radarForward = transform;
		}
	}


	void LateUpdate()
	{		
		GameObject[] targets = GameObject.FindGameObjectsWithTag("EnemyTeam1");

		//particles = new ParticleSystem.Particle[ourParticleSystem.particleCount];
		particles = new ParticleSystem.Particle[maxparticles];
		ourParticleSystem.GetParticles(particles); //without this no particles will appear


		List<GameObject> filteredTargets = new List<GameObject>();
		int totalTargets = 0;


		for (int i = 0; i < targets.Length; i++)
		{

			Vector3 tarpos = radarForward.InverseTransformPoint(targets[i].transform.position);
						
			if( displayOnly == targetfilter.targetsInFront)
			{
				if (tarpos.z > 0)
				{
					totalTargets++;
					particles[i].position = GetPosition(targets[i].transform.position, radarForward);
				}
			}
			else if(displayOnly == targetfilter.targetsbehind)
			{
				if (tarpos.z < 0)
				{
					totalTargets++;
					particles[i].position = GetPosition(targets[i].transform.position, radarForward);
				}
			}
			else
			{
				totalTargets++;
				particles[i].position = GetPosition(targets[i].transform.position, radarForward);
			}
		}



		// Apply the particle changes to the particle system
		ourParticleSystem.SetParticles(particles, totalTargets);
	}
	


	/*
	 * Find where the icon should be positioned on the radar screen
	 * 
	 * Finds the right position and then flattens and scales it
	 * 
	 * Good for a round screen
	 * */
	private Vector3 GetPosition(Vector3 targetPosition , Transform myTransform)
	{
		Vector3 result = targetPosition - myTransform.position;
		float dist = Vector3.Distance(myTransform.position, targetPosition);

		if (dist > maxDist)
			result = result.normalized * maxDist;

		result = myTransform.InverseTransformVector(result);
		result.z = 0;
		result = mapScale * result;
		return result;
	}


	/**
	 * Basically maps the horizontal and vertical angles to a square grid with 
	 * the azimuth on y axis and degrees left or right on x axis
	 * */
	private Vector3 GetPositionAngleMode(GameObject go)
	{
		Vector3 extPos = transform.InverseTransformPoint(go.transform.position);
		Vector3 rot = Vector3.zero;
		float dist = Vector3.Distance(transform.position, go.transform.position);

		if (extPos != Vector3.zero)
		{
			rot = Quaternion.LookRotation(extPos).eulerAngles;
		}

		float bY = rot.x;
		float bX = rot.y;
		float bZ = extPos.z;


		if (bY > 90)
		{
			if (bY > 270)
			{//front half
				bY = -1 * (360 - bY);
			}
			else
			{//rear half
				if (bY > 180)
				{
					bY = (180 - bY);
				}
			}
		}
		bY *= -1;


		if (bX > 90)
		{
			if (bX > 270)
			{//front half
				bX = -1 * (360 - bX);
			}
			else
			{//rear half
				if (bX > 180)
				{
					bX = -1 * (180 - bX);
				}
				else
				{
					bX = -1 * (180 - bX);
				}
			}
		}

		if (bZ > 0 )
		{
			return new Vector3(mapScale*bX, mapScale*bY, 0);
		}
		else
		{
			return Vector3.zero;
		}
			
	}

		

	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.yellow;
		//Gizmos.DrawWireSphere(transform.position, maxDist);
		Gizmos.DrawWireSphere(transform.position, physicalSize);
	}
	


}
