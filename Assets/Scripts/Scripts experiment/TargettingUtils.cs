﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * Generic class to have in the project to help with common targetting tasks
 * 
 * 
 * 
 * */
public static class TargettingUtils  {



	public static bool CheckRange(Vector3 myposition, Vector3 tarPos, float range)
	{
		bool ret = false;
		float dist = Vector3.Distance(tarPos, myposition);
		if (dist < range)
		{
			ret = true;
		}
		return ret;
	}


	public static Transform ClosestInFov( List<Transform> set , Transform myTransform)
	{
		Transform target = null;
		float mag = 9999f;
		Transform tar = null;

		foreach (Transform g in set)
		{
			Vector3 enemypos = g.transform.position;

			if (CheckFov(enemypos, 60, myTransform.position, myTransform.forward))
			{
				if (Vector3.Magnitude(myTransform.position - g.transform.position) < mag)
				{
					mag = Vector3.Magnitude(myTransform.position - g.transform.position);
					tar = g.transform;
					target = tar;
				}
			}
		}

		return target;
	}

	/*
	//check if a position 'pos' is visible within 'angle' off of dead ahead
	*/
	public static bool CheckFov(Vector3 targetPosition, float angle, Vector3 myPosition , Vector3 myforward)
	{
		float ang = Vector3.Angle(myforward, targetPosition - myPosition);
		bool ret = false;//return value

		if (ang < angle)
		{
			ret = true;
		}

		return ret;
	}

	/**
	 * Find which transform is closest to dead ahead of the 'player' transform
	 * */
	public static Transform ClosestToForwards( List<Transform> candidates , Transform player)
	{
		float closest = Mathf.Infinity;
		Transform newTar = null;

		for (int i = 0; i < candidates.Count; i++)
		{
			//if( Vector3.Angle( playership.forward, 
			if (candidates[i] != null)
			{
				Vector3 pos = candidates[i].transform.position;
				float ang = Vector3.Angle(player.forward, pos - player.position);

				if (ang < closest)
				{
					newTar = candidates[i].transform;
					closest = ang;
				}
			}
		}
		return newTar;
	}

	/*
	//find position to go towards to hit target
	//tar = target object with rigidbody
	//tr = transform of starting point gun that is shooting, or your starfighter
	//projectileSpeed = bullet velocity
	*/
	public static Vector3 CalculateLead(Transform tar, Transform tr, float projectileSpeed)
	{
		Rigidbody target;
		target = tar.root.GetComponent<Rigidbody>();

		Vector3 soln = tar.position;

		if (target)
		{
			Vector3 V = target.velocity;
			Vector3 D = tar.position - tr.position;
			float A = V.sqrMagnitude - projectileSpeed * projectileSpeed;
			float B = 2 * Vector3.Dot(D, V);
			float C = Vector3.SqrMagnitude(D);

			if (A >= 0)
			{
				//Debug.LogError ("No solution exists");
			}
			else
			{
				float rt = Mathf.Sqrt(B * B - 4 * A * C);
				float dt1 = (-B + rt) / (2 * A);
				float dt2 = (-B - rt) / (2 * A);
				float dt = (dt1 < 0 ? dt2 : dt1);
				soln = (tar.position + V * dt);
			}
		}
		return soln;
	}



	//find position to go towards to hit target
	public static Vector3 CalculateLead(Vector3 myposition , Vector3 targetPosition, Vector3 targetVelocity, float projectileSpeed)
	{
		Vector3 soln = targetPosition;
		Vector3 V = targetVelocity;
		Vector3 D = targetPosition - myposition;
		float A = V.sqrMagnitude - projectileSpeed * projectileSpeed;
		float B = 2 * Vector3.Dot(D, V);
		float C = Vector3.SqrMagnitude(D);

		if (A >= 0)
		{
			//Debug.LogError ("No solution exists");
		}
		else
		{
			float rt = Mathf.Sqrt(B * B - 4 * A * C);
			float dt1 = (-B + rt) / (2 * A);
			float dt2 = (-B - rt) / (2 * A);
			float dt = (dt1 < 0 ? dt2 : dt1);
			soln = (targetPosition + V * dt);
		}

		return soln;
	}



}
