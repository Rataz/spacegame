﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * Should go on a gameobject called Campeign or be a child of one
 * 
 * */
public class CampeignDestroyXThings : MonoBehaviour {

	[Tooltip("How many to destroy")]
	public int count = 1;

	[Tooltip("Actual string to search for gameobject string")]
	public string thingName = "FighterTest(Clone)";

	[Tooltip("If this objective is currently active")]
	public bool armed = true;

	[Tooltip("Objective to activate after this one if any")]
	public GameObject next;



	[Tooltip("Text name of thing to destroy for the ui")]
	public string thingNameForString = "Fighters";

	[Tooltip("UI text object that will be updated")]
	public GameObject textObject;


	private void Start()
	{
		if (armed)
		{
			UpdateText(MakeText());
		}
	}

	public void GameEvent(string str)
	{

		if( armed)
		{
			if (str.Contains("destroyed"))
			{
				if (str.Contains(thingName))
					count--;

				UpdateText(MakeText());
			}

			if (count < 1)
			{
				Completed();
			}
		}
		
	}

	public void Completed()
	{
		Disarm();

		print(transform.name);

		if (next)
			next.SendMessage("Arm", SendMessageOptions.DontRequireReceiver);
	}

	public void Arm()
	{
		UpdateText(MakeText());
		armed = true;
	}

	private string MakeText()
	{
		return "Destroy " + count + " " + thingNameForString;
	}

	public void UpdateText(string text)
	{
		if (textObject)
		{
			textObject.SendMessage("SetText", text, SendMessageOptions.DontRequireReceiver);
		}
	}


	public void Disarm()
	{
		armed = false;
	}

	public void ResetToStart()
	{
		
	}



}
