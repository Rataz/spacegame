﻿using System.Collections;
using UnityEngine;
[RequireComponent(typeof(ParticleSystem))]

public class ParticlePosition : MonoBehaviour {

	ParticleSystem ourParticleSystem;
	ParticleSystem.Particle[] particles = null;

	void Start () {
		ourParticleSystem = GetComponent<ParticleSystem>();
	}

	void LateUpdate () {

		particles = new ParticleSystem.Particle[ourParticleSystem.particleCount];
		ourParticleSystem.GetParticles(particles); //without this no particles will appear

		for (int i = 0; i < particles.Length; i++)
		{
			/*
			ParticleSystem.Particle p = particles[i];
			p.position = new Vector3(0, 0, i);
			particles[i] = p;
			*/

			particles[i].position = new Vector3(0, 0, i);
		}

		ourParticleSystem.SetParticles(particles, particles.Length);
	}
}
