﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class parabola : MonoBehaviour {


	public LineRenderer line;

	public int steps=50;
	public float start =0;
	public float stepsize = 0.5f;
	public float divisor = 20f;
	public float height = 20f;

	// Use this for initialization
	void Start () {

		

	}
	
	// Update is called once per frame
	void Update () {
		List<Vector3> positions = new List<Vector3>();

		float x = start;

		for (int i = 0; i < steps; i++) {

			float y = (x * x) / divisor + height;
			x += stepsize;

			positions.Add(new Vector3(x, y, 0));
		}
		line.positionCount = 200;
		line.SetPositions(positions.ToArray() );
	}



}
