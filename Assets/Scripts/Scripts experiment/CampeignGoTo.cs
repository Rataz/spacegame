﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * Incomplete 
 * 
 * Player must go into collider to complete
 * 
 * */
public class CampeignGoTo : MonoBehaviour {



	[Tooltip("If this objective is currently active")]
	public bool armed = true;

	[Tooltip("Objective to activate after this one if any")]
	public GameObject next;


	[Tooltip("UI text object that will be updated")]
	public GameObject textObject;

	[Tooltip("What to write in the interface?")]
	public string textToDisplay = "";

	private void Start()
	{
		if (armed)
		{
			UpdateText("GOTO");
		}
	}


	//TODO MODIFY THIS 
	void OnTriggerEnter(Collider other)
	{
		if (other.transform.name.Contains("PLAYER"))
		{
			Completed();
		}
	}

	public void Completed()
	{
		Disarm();

		print(transform.name);

		if (next)
			next.SendMessage("Arm", SendMessageOptions.DontRequireReceiver);
	}



	public void Disarm()
	{
		armed = false;
	}


	public void Arm()
	{
		armed = true;
		UpdateText("GOTO");
	}


	public void GameEvent(string str)
	{

	}


	public void UpdateText(string text)
	{
		if (textObject)
		{
			textObject.SendMessage("SetText", textToDisplay, SendMessageOptions.DontRequireReceiver);
		}
	}



}
