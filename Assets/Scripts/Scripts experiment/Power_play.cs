﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/**
 * One of these for each component that consumes (or distrubutes/routes) power
 * 
 * Make a tree of these
 * 1 root
 * children
 * children of children
 * 
 * receivers are children of this script
 * 
 * 
 * size of each list must be the same
 * 
 * order of items in the lists must be the same
 * 
 * */
public class Power_play : MonoBehaviour {

	[Tooltip(" child nodes")]
	public List<Power_play> receivers = new List<Power_play>();

	[Tooltip("calculated percent of total power each child gets (view only, dont try to change in inspector(unless usepts disabled))")]
	public List<float> pow_percent_out = new List<float>();

	[Tooltip("number of points assigned to each child node by player")]
	public List<int> pts_assigned = new List<int>();

	[Tooltip(" unassigned pts")]
	public float pts_left; //points to assign the power

	[Tooltip(" total points in the system")]
	public float pts_total; //the total of the points in the system

	[Tooltip("do we use the point system?")]
	public bool use_pts; //do we use the point system?

	[Tooltip("current power you have, added to by incoming power, make nonzero on root node")]
	public float power; //this is the received power from any upper power source, (it also represents our current power)

	[Tooltip("maximum power this node can store and distrubute")]
	public float max_pow; //maximum power

	[Tooltip("Size of one unit of power added to current power each frame")]
	public float power_increment; 

 


	void Update () {
        
        if (power > max_pow)
            power = max_pow; //cap the power at the max power
        
        if (receivers.Count !=0 ) //check we have things to send power to (this script also should serve as the end receiver of any power, which will be accessed by the powered accessories scripts)
        {
            for (int i = 0; i < receivers.Count; i++)  //go through all the receivers of our power and give them our extra power
            {
				receivers[i].power += power * pow_percent_out[i] * power_increment * Time.deltaTime;

                if(use_pts)
                {
                     pow_percent_out[i] = pts_assigned[i] / pts_total;
                }


            }
        }
    }

	/* Use to view current power level * */
	public float GetCurrentPower()
	{
		return power;
	}

	/** Call this to use power
	 * if you have enough power, subtracts used power and returns true
	 * otherwise returns false and no power deducted * */
	public bool ConsumePower(float pow)
	{
		bool ret = false;

		if( power >= pow)
		{
			power -= pow;
			ret = true;
		}

		return ret;
	}
	
	/** Use to distribute power	 * */
    public void Add_pt(int index)
    {
        if (pts_left > 0)
        {
            pts_assigned[index] += 1;
            pts_left -= 1;
        }
    }

	/** Use to distribute power	 * */
	public void Remove_pt(int index)
    {
        if (pts_assigned[index] > 0)
        {
            pts_assigned[index] -= 1;
            pts_left += 1;
        }
    }



}
