﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class UiUtils {

	public static Rect GetRectangleOverWorldPosition(Camera cam , Vector3 worldposition , float scale , Texture tex )
	{
		Vector3 po = worldposition;

		float x = cam.WorldToScreenPoint(po).x - tex.width / 2 * scale;
		float y = Screen.height - (cam.WorldToScreenPoint(po).y) - tex.height / 2 * scale;
		float width = tex.width * scale;
		float height = tex.height * scale;
		Rect rectangle = new Rect(x, y, width, height);

		return rectangle;
	}

	public static bool IsInFront( Transform player , Transform target)
	{
		Transform tar = target;
		Vector3 fwd = player.forward;
		Vector3 playerPosition = player.position;

		float dot = Vector3.Dot(tar.position - playerPosition, fwd);

		return dot > 0;
	}


}
