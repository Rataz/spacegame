﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTargetOverlay : MonoBehaviour {


	public Transform player;
	public float scale = 1.0f;

	public bool drawAllMarkers = true;

	public Texture crosshairTex;
	public Texture crosshairTex2;

	public float alpha = 0.75f;
	private Color guiColor;

	public ShipRosterTeam roster;

	public Camera gunnerCam;
	public Camera mainCam;


	//unused-------
	//for in future preventing player icons from going to the wrong area of the divided mp screen
	public float mainCamX = 0.5f;
	public float guncamX = 0.5f;

	public float mainCamH = 0.5f;
	public float mainCamW = 0.5f;

	public float guncamH = 0.5f;
	public float guncamW = 0.5f;
	//unused----------

	void Start () {
		
	}
	
	
	void OnGUI()
	{
		List<Transform> ships = roster.GetShipsTransforms();
		OnGUIx( ships, player, null);

	}

	void OnGUIx(List<Transform> targets , Transform playership, Transform playerTarget)
	{
		if(playership != null)
		{
			DrawTargetLead(playership, playerTarget ,1);
			DrawAllBoxes(targets, playership);
		}
		
		if(gunnerCam != null)
		{
			DrawGunnerBoxes(targets, gunnerCam.transform);
		}		
	}



	private void DrawTargetLead(Transform playership, Transform playerTarget , int screensector)
	{
		if (crosshairTex && (playerTarget != null))
		{
						
			guiColor = Color.white;
			guiColor.a = 1;

			if (UiUtils.IsInFront(playership, playerTarget))
			{ //target is in front

				Vector3 leadVector = TargettingUtils.CalculateLead(playerTarget, playership, 400);
				Rect rectangle = UiUtils.GetRectangleOverWorldPosition(mainCam, leadVector, scale, crosshairTex2);
				
				bool xrange = false;

				if (screensector == 1)
				{
					//pilot >
					xrange = rectangle.x > Screen.width * 0.5;
				}
				else if ( screensector == 2)
				{
					//gunner <
					xrange = rectangle.x < Screen.width * 0.5;
				}
				
				if (xrange)
					GUI.DrawTexture(rectangle, crosshairTex2);

			}

		}
	}

	private void DrawGunnerBoxes(List<Transform> targets, Transform gunner)
	{
		if (drawAllMarkers)
		{
			guiColor = Color.green;
			guiColor.a = alpha;
			for (int i = 0; i < targets.Count; i++)
			{
				Transform currentObject = targets[i];
				if (currentObject != null)
				{
					Vector3 pos = currentObject.position;
					float dot2 = Vector3.Dot(pos - gunner.position, gunner.forward);

					if (dot2 > 0)
					{		
						//position is in front
												
						Rect rectangle = UiUtils.GetRectangleOverWorldPosition(gunnerCam, pos, scale, crosshairTex);
						
						bool xrange = rectangle.x < Screen.width *0.5;
						//bool yrange = y > Screen.height - (Screen.height * guncamH);

						if (  xrange  )
							GUI.DrawTexture(rectangle, crosshairTex);
					}

				}
			}
		}
	}

	private void DrawAllBoxes(List<Transform> targets, Transform playership)
	{
		if (drawAllMarkers)
		{
			guiColor = Color.green;
			guiColor.a = alpha;
			for (int i = 0; i < targets.Count; i++)
			{
				Transform currentTransform = targets[i];
				if (currentTransform != null)
				{
					Vector3 pos = currentTransform.position;
					float dot2 = Vector3.Dot(pos - playership.position, playership.forward);

					if (dot2 > 0)
					{
						//position is in front

						Rect rectangle = UiUtils.GetRectangleOverWorldPosition(gunnerCam, pos, scale, crosshairTex);
						
						bool xrange = (rectangle.x > Screen.width * 0.5);

						if( xrange )
							GUI.DrawTexture(rectangle, crosshairTex);
					}

				}
			}
		}
	}




	public void SetPlayer()
	{

	}


	

}
