﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;


/*
 * Just for displaying power lvls
 * 
 * */
public class pow_distribution : MonoBehaviour {

    public List<Scrollbar> levels  = new List<Scrollbar>(); // the different scroll bars we are interacting with
    public Power_play power; // the power manager the levels will be controlling
	

    void Update()
    {
		for (int i = 0; i < levels.Count; i++)  //set all the correct power levels in the pwoer_play script based on the visual settings
        {
			levels[i].value = power.pow_percent_out[i];
        }
    }

}
