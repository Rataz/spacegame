﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * Place this on an empty gameobject
 * 
 * provides AI an easy way so get some info about the scene
 * */
public class GlobalSceneInfo : MonoBehaviour {

	//place team rosters in here in order of team number, team1 goes in element 0, t2 in element 1
	[Tooltip("team1 roster object goes in element 0, t2 in element 1")]
	public ShipRosterTeam[] rosters;

	//list of squadron lists
	[Tooltip("team1 roster object goes in element 0, t2 in element 1")]
	public SquadronRoster[] squadronRosters;

	[Tooltip("team1 boss object goes in element 0, t2 in element 1")]
	public Boss[] bosses;

	//get roster by team number
	public ShipRosterTeam GetRosterOfTeam( int team ){
		return rosters [team - 1];
	}

	//get squadron roster by team number
	public SquadronRoster GetSquadronRosterOfTeam( int team ){
		return squadronRosters [team - 1];
	}

	//get boss by team number
	public Boss GetBossOfTeam( int team ){
		return bosses [team - 1];
	}

	public void ListShipsNotOnTeam( int team ){
		/*
		List<GameObject> targets;
		foreach (ShipRosterTeam x in rosters) {
			if (x.team != team) {
				//targets.
			}
		}
		*/

	}


}
