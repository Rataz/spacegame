﻿using UnityEngine;
using System.Collections;

/*
 * Just stores some variables for player object
 * 
 * put this script on an empty in the scene
 * */
public class PlayerSceneInfo : MonoBehaviour {

	public Transform player;

	public Transform ShipCamera;

	public PlayerShipGuiController playerShipGUI;

	public int team = 2;

}
