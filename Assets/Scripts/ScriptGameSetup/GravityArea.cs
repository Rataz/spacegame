﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/**
 * Artificial gravity within trogger collider
 * 
 * Mothership hangar should have kinematic rigidbody to prevent being pushed by objects on it
 * 
 * */
public class GravityArea : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay(Collider other) {
		if (other.attachedRigidbody) {
			Vector3 direction = transform.TransformDirection ( Vector3.up * -9.81f );
			other.attachedRigidbody.AddForce( direction ,ForceMode.Acceleration);
		}
	

	}
}
