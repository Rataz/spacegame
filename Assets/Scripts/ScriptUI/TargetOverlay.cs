﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

//
//overlays target icons on screen
//not same as radar,
//HUD
public class TargetOverlay : MonoBehaviour {

	public Texture crosshairTex;
	public Texture crosshairTex2;

	public float scale = 1.0f;

	public Transform playership;

//	int team =1;

	public List<SmallShipFacade> targets;

	private GlobalSceneInfo globalinfo;

	public ShipRosterTeam roster;

	public PlayerSceneInfo playerSceneInfo;

	void Start () {
		playerSceneInfo = GameObject.FindObjectOfType<PlayerSceneInfo>();
		globalinfo =  GameObject.FindObjectOfType<GlobalSceneInfo>();
	}


	void Update () {

		//find roster of player team enemies
		int team = playerSceneInfo.team;

		if (team == 1) {
			roster = globalinfo.GetRosterOfTeam (2);
		}

		if (team == 2) {
			roster = globalinfo.GetRosterOfTeam (1);
		}
		targets = roster.ships;


		//
		playership = playerSceneInfo.player;

		if( Input.GetKeyDown( KeyCode.O )){
			TargetInReticule(  );
		}
	}


	public Transform playerTarget;

	//target ship that is closest to fwd
	//fwd of playership
	public void TargetInReticule(  ){

		float closest = Mathf.Infinity;

		Transform newTar;

		for( int i=0; i< targets.Count; i++ ){
			//if( Vector3.Angle( playership.forward, 
			if(  targets[i] != null ){
				Vector3 pos =  targets[i].transform.position;
				float ang = Vector3.Angle( playership.forward,  pos - playership.position  );

				if( ang<closest ){
					newTar =  targets[i].transform;
					closest = ang;
					playerTarget = newTar;
				}
			}
		}

		/*ic bool CheckFov( Vector3 pos , float angle ){
		float ang = Vector3.Angle(   pos - tr.position , tr.forward );
		bool ret = false;
		if( ang < angle ){
			ret = true;
		}*/
	}



	void OnGUI(){

		Transform tar = playerTarget;

		if( playership != null ){



			Vector3 fwd =  playership.forward;
			Vector3 ppos = playership.position;

			if( tar ){
				float dot = Vector3.Dot( tar.position - ppos , fwd );


				//Draw targetted object markers for lead and position
				if( dot>0 ){ //target is in front
					GUI.DrawTexture(new Rect(Camera.main.WorldToScreenPoint( tar.position ).x- crosshairTex.width/2 * scale,
						Screen.height-(Camera.main.WorldToScreenPoint( tar.position ).y)- crosshairTex.height/2 * scale,
						crosshairTex.width*scale,crosshairTex.height*scale),crosshairTex);

					Vector3 bob = CalculateLead( tar , playership , 300 );

					GUI.DrawTexture(new Rect(Camera.main.WorldToScreenPoint( bob ).x- crosshairTex.width/2 * scale,
						Screen.height-(Camera.main.WorldToScreenPoint( bob ).y)- crosshairTex.height/2 * scale,
						crosshairTex.width*scale,crosshairTex.height*scale),crosshairTex2);
				}
			}

			//markers ships
			for( int i=0; i<targets.Count; i++){

				SmallShipFacade h = targets[i];

				if( h!=null ){
					Transform othership = h.transform;
					Vector3 po = othership.position;
					float dot2 = Vector3.Dot( po - ppos , fwd );

					if( dot2>0 ){
						GUI.DrawTexture(new Rect(Camera.main.WorldToScreenPoint( po ).x- crosshairTex.width/2 * scale,
							Screen.height-(Camera.main.WorldToScreenPoint( po ).y)- crosshairTex.height/2 * scale,
							crosshairTex.width*scale,crosshairTex.height*scale),crosshairTex);
					}

				}

			}


		}



	}


	//find position to go towards to hit target
	//tar = target object with rigidbody
	//tr = transform of starting point gun that is shooting, or your starfighter
	//projectileSpeed = bullet velocity
	private Vector3 CalculateLead ( Transform tar, Transform tr , float projectileSpeed ) {

		Rigidbody target;
		target = tar.root.GetComponent<Rigidbody>() ;

		Vector3 soln = tar.position;

		if( target ){

			Vector3 V = target.velocity;
			Vector3 D = tar.position - tr.position;
			float A = V.sqrMagnitude - projectileSpeed * projectileSpeed;
			float B = 2 * Vector3.Dot (D, V);
			float C = Vector3.SqrMagnitude( D );

			if ( A >= 0 ) {
				//Debug.LogError ("No solution exists");
			}
			else {
				float rt = Mathf.Sqrt (B*B - 4*A*C);
				float dt1 = (-B + rt) / (2 * A);
				float dt2 = (-B - rt) / (2 * A);
				float dt = (dt1 < 0 ? dt2 : dt1);
				soln = ( tar.position + V * dt ) ;
			}
		}

		return soln;
	}




}
