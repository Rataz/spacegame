﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiTextChanger  : MonoBehaviour{
	
	private Text tex;
	
	void Awake()
	{
		tex = gameObject.GetComponent<Text>();	
	}

	public void SetText(string str)
	{
		tex.text = str;
	}


}
