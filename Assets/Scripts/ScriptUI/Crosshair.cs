using UnityEngine;
using System.Collections;

/*
Attach to player, makes croshair on the screen
*/
public class Crosshair : MonoBehaviour {
	//[Header("Hi there!")]

	[Tooltip("Player Crosshair texture")]
	public Texture crosshairTex;


	[Space(10)]
	[Tooltip("A transform will be used to position crosshair, can be a child of the player")]
	public bool positionOnTarget=false;
	public Transform target;

	[Space(10)]
	[Tooltip("Vector will be used to set crosshair position")]
	public bool useVector=false;
	public Vector3 positionVector;
	[Space(10)]

	[Tooltip("Scale of the texture")]
	public float scale=1;

	[Space(10)]
	[Tooltip("If true, use a horizontal splitscreen preset")]
	public bool splitscreen=false;

	[Tooltip("For splitscreen preset")]
	public int playerNo=1;
	[Space(10)]

	//[Header("Hi there!")]
	public float alpha=0.75f;
	private Color guiColor;

	[Tooltip("Camera that should have crosshair, main camera if null")]
	public Camera cam;

	[Tooltip("Use a custom fraction of the screen")]
	public bool useCustomPosition = false;
	public Vector2 customPosition = Vector2.zero;

	void Start(){
		guiColor = Color.white;
		guiColor.a = alpha;

		if (cam == null)
			cam = Camera.main;
	}

	void OnGUI(){
		GUI.color = guiColor;

		float h = crosshairTex.width * scale;
		float w = crosshairTex.height * scale;
		float x = 0;
		float y = 0;


		if(useCustomPosition)
		{
			x = Screen.width * customPosition.x - crosshairTex.width / 2 * scale;
			y = Screen.height * customPosition.y - crosshairTex.height / 2 * scale;
		}
		else if ( positionOnTarget == true )
		{
			if(useVector==true){
				x = cam.WorldToScreenPoint(positionVector).x - crosshairTex.width / 2 * scale;
				y = Screen.height - (cam.WorldToScreenPoint(positionVector).y) - crosshairTex.height / 2 * scale;
			}
			else{
				x = cam.WorldToScreenPoint(target.position).x - crosshairTex.width / 2 * scale;
				y = Screen.height - (cam.WorldToScreenPoint(target.position).y) - crosshairTex.height / 2 * scale;
			}
		}
		else
		{
			if( splitscreen ){
				if( playerNo==1 ){
					x = Screen.width / 2 - crosshairTex.width / 2 * scale;
					y = Screen.height / 4 - crosshairTex.height / 2 * scale;
				}
				else{
					x = Screen.width / 2 - crosshairTex.width / 2 * scale;
					y = Screen.height * 3 / 4 - crosshairTex.height / 2 * scale;
				}
			}
			else{
				x = Screen.width / 2 - crosshairTex.width / 2 * scale;
				y = Screen.height / 2 - crosshairTex.height / 2 * scale;				
			}
		}

		GUI.DrawTexture(new Rect(x, y, w, h), crosshairTex);
		
	}




}
