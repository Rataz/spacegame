﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class TextChanger : MonoBehaviour {

	public Text target;

	public int indx = 0;

	public List < TextHolder > texts;



	public void NextPos(){
		
		indx++;
		
		if( indx > texts.Count-1 )
			indx = 0;
		
		target.text = texts[indx].GetText();

	}
	
	public void PrevPos(){
		
		indx--;
		
		if( indx < 0 )
			indx = texts.Count-1;


		target.text = texts[indx].GetText();

	}

}
