﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//Brings up pause menu on escape key press
//pause panel is to be the GUI box and buttons object.  Should be disabled on start up

//Also locks cursor on scene load if needed
public class PauseMenu : MonoBehaviour {

	public bool paused  = false;

	public GameObject pausePanel;

	public bool lockMouse = false;

	void Start(){
		paused  = false;
		if( lockMouse ){
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = true;
		}

	}

	void Update () {

		if( Input.GetKeyUp( KeyCode.Escape ) ){

			if( paused ){
				//Unpause things.....

				//Lock mouse
				if( lockMouse ){
					Cursor.lockState = CursorLockMode.Locked;
					Cursor.visible=true;
				}

				if( pausePanel ){
					pausePanel.SetActive( false );
				}

				Time.timeScale = 1.0f;

				paused = false;
			}//if currently UNpaused do these
			else{
				//Pause things.........

				if( lockMouse ){
					Cursor.lockState = CursorLockMode.None;
					Cursor.visible = true;
				}

				//show panel
				if( pausePanel ){
					pausePanel.SetActive( true );
				}

				Time.timeScale = 0.0000001f;
				paused = true;
			}

		}

	}


	/*
	public IEnumerator Pause(){
		Time.timeScale = 0.0000001f;
		yield return new  WaitForSeconds( pauseDelay * Time.timeScale );
		Time.timeScale = 1.0f;		
		Application.LoadLevel( Application.loadedLevel );
	}
*/

}
