﻿using UnityEngine;
using System.Collections;

public class AnimatorUISetter : MonoBehaviour {

	public Animator anim;

	public string boolname;

	public float coolDownTime = .5f;

	public bool tog;
	private bool canclick = true;


	public void SetBoolTrue(){
		anim.SetBool( boolname , true );
	}

	public void SetBoolFalse(){
		anim.SetBool( boolname , false );
	}

	public void TogBool(){

		if( canclick ){

			StartCoroutine ( "CoolDown" );

			if( tog == true ){

				tog = false;
				anim.SetBool( boolname , tog );

			}
			else{

				tog = true;
				anim.SetBool( boolname , tog );

			}


		}

	}


	public IEnumerator CoolDown(){
		canclick = false;
		yield return new WaitForSeconds ( coolDownTime );
		canclick = true;
	}

}
