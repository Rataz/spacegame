﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/*
Changes a sprite and text object to create a slideshow of dialog from a character
*/

public class DialogScroller : MonoBehaviour {

	public List<AvatarDialog> dialogs;

	public Image img;

	public Image background;

	public Text dialogText;


	// Use this for initialization
	void Start () {
		StartCoroutine("scroll");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public IEnumerator scroll(){
		foreach( AvatarDialog d in dialogs ){
			img.sprite = d.sprite;
			dialogText.text = d.words;
			yield return new WaitForSeconds( d.time );
		}
		dialogText.text = "";
		img.enabled=false;
		background.enabled=false;
		yield return 0;
	}
}
