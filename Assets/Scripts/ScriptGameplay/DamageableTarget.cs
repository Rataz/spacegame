﻿using UnityEngine;
using System.Collections;

public class DamageableTarget : MonoBehaviour , IDamageable {


	float hp=10;
	private bool damageable = true;


	public ParticleSystem fire;

	public void ApplyDamage(float damage)	{
		hp-=damage;
		if (hp < 0)
		{
			Defeat();
		}
	}


	public void ApplyDamage(int damage)
	{ //int compatable
		float bob = damage;
		ApplyDamage(bob); //call float version
	}


	public int GetTeam()
	{
		return 0;
	}

	public void Defeat(){
		if (damageable)
		{
			damageable = false;


			if (fire){
				var em = fire.emission;
				em.enabled = true;
			}


		}
	}

}
