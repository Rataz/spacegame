﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class VictoryDetector : MonoBehaviour {

	public int playerTeam = 1;
	public Text texVictory;
	public Text textDefeat;
	public bool loss = false;
	public float pauseDelay = 2;
	public int totalTeams = 2;
	public List <int> teamsForDefeating;
	public int teamsToVictory = 1;



	public void Defeated( int team ){

		if( team == playerTeam){
			PlayerLoose();
		}
		else{
			for( int i = 0; i< teamsForDefeating.Count ; i++ ){
				if( teamsForDefeating[i] == team ){
					teamsToVictory --;
				}
			}
		}

		if( teamsToVictory <= 0 ){
			PlayerWin();
		}
	}

	public void PlayerLoose(){
		loss = true;
		if( texVictory ){
			textDefeat.enabled = true;
		}
		StartCoroutine("Pause");
	}

	public void PlayerWin(){
		loss = true;
		if( texVictory ){
			texVictory.enabled = true;
		}
		StartCoroutine("Pause");
	}

	//should be renamed
	//pause the scene and reload it
	public IEnumerator Pause(){

		//free the mouse in case it was frozen
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible=true;

		//Pause
		Time.timeScale = 0.0000001f;
		yield return new  WaitForSeconds( pauseDelay * Time.timeScale );
		Time.timeScale = 1.0f;		
		//Application.LoadLevel( Application.loadedLevel );
		SceneManager.LoadScene( "gallery" );
	}


}
