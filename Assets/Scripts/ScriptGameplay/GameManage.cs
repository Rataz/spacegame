﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


/**
Keeps player score etc UI up to date
*/

public class GameManage : MonoBehaviour {

	public Transform player;


	public Text menTeamPlayer;
	public Text menTeamNPC;
	
	public Text xptext;
	public Text creditstext;
	public Text scoretext;
	
	public int reinforcePlayer = 1;
	public int reinforceNPC = 10;
	
	public int score = 0;
	public int xp = 0;
	public int credits = 0;

	public Text teamLeftLabel;
	public Text teamRightLabel;
	
	public VictoryDetector victoryd; //script to pause game and restart


	int playerteam = 1;


	//Update the team labels at the top
	public void UpdateTeamLabels(){

		select_persist persistent = GameObject.FindObjectOfType<select_persist>();

		if( persistent ){
			playerteam = persistent.team;

			if( playerteam == 0 ){

				if( teamLeftLabel ){
					teamLeftLabel.text = "Republic";
				}

				if( teamRightLabel ){
					teamRightLabel.text = "CIS";
				}
			}

			if(playerteam == 1 ){
				if( teamLeftLabel ){
					teamLeftLabel.text = "CIS";
				}

				if( teamRightLabel ){
					teamRightLabel.text = "Republic";
				}
			}

		}

	}

	public void Start(){
		RefreshGUI();
		UpdateTeamLabels();
	}

	public void AddXP( int m ){
		xp += m;
		RefreshGUI();
	}

	public void AddCredits( int m ){
		credits += m;
		RefreshGUI();
	}

	public void AddScore( int m ){
		score += m;
		RefreshGUI();
	}

	public void AddReinforcePlayer(  int m ){
		reinforcePlayer +=m;
		RefreshGUI();
		CheckLoss();
	}

	public void AddReinforceNPC(  int m ){
		reinforceNPC +=m;
		RefreshGUI();
		CheckLoss();
	}

	public void CheckLoss(){
		//detect win or loss
		if( reinforceNPC < 0 )
			return;
		
		if( reinforcePlayer < 0 )
			return;
		
		if( reinforceNPC == 0 ){
			if( victoryd ){
				victoryd.PlayerWin();
			}
		}
		
		if( reinforcePlayer == 0 ){
			if( victoryd ){
				victoryd.PlayerLoose();
			}
		}
	}
	
	public void RefreshGUI(){//Update interface
		
		if( menTeamPlayer )
			menTeamPlayer.text = reinforcePlayer.ToString();
		
		if( menTeamNPC )
			menTeamNPC.text = reinforceNPC.ToString();
		
		if( creditstext )
			creditstext.text = credits.ToString();
		
		if( xptext )
			xptext.text = xp.ToString();
		
		if( scoretext )
			scoretext.text = score.ToString();
		
	}


}
