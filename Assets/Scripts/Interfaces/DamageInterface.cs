﻿using UnityEngine;
using System.Collections;

public interface IDamageable {


	void ApplyDamage( int damage );
	void ApplyDamage( float damage );
	int GetTeam( );

}
