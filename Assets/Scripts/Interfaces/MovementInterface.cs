﻿using UnityEngine;
using System.Collections;

public interface IMovement {

	void SetMoveDir( Vector3 dir );
	void SetFaceDir( Vector3 dir );
	void SimpleMove( Vector3 direction );
	void SetCanMove( bool tf );
	void jump() ;


	void SetStrafeOn( bool lookInDifferentDirectionThanPath );
	void SetPathingTarget( Vector3 tar );
	void SetPathingOn( bool tf );


}
