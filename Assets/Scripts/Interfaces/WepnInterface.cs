﻿using UnityEngine;
using System.Collections;

public interface IWeapon{
	
	void Fire();
	void GetAmmo();
	void Arm();
	void Disarm();

}

public interface IAimer{

	void SetAzimuth( float az );
	void SetOn( bool onOff );
	void SetTarget( Vector3 target );
	void SetTarget( Transform target );

}
