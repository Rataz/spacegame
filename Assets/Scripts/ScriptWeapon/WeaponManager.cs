﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponManager : MonoBehaviour {

	public Transform lefthand ;
	public Transform righthand ;

	public bool UseWeapons = false;
	public List<Weapon> weapons;
	public int selectedweapon =1;
	public int playerNo =1;
	
	public string p1Fire1 = "Fire1";
	
	public string selectback = "selectback";
	public string selectfor = "selectfor";
	public string selectbackp2 = "selectbackp2";
	public string selectforp2 = "selectforp2";

	
	void Start () {
		
		if(UseWeapons){
			Check();
			Select();
		}
		
		if(weapons.Count>0){
			for (var i = 0; i < weapons.Count ; i++) {
				//weapons[i].SetActive(false);
				weapons[i].BroadcastMessage("DisArm", SendMessageOptions.DontRequireReceiver);
			}
		}
		
	}
	
	void Update (){
		
		if( UseWeapons == true ){
			
			if ( Input.GetButton (p1Fire1) ){
				BroadcastMessage("Fire", SendMessageOptions.DontRequireReceiver);
			}
			
			if ( Input.GetButtonUp (p1Fire1 )){
				
			}
			
			if (Input.GetButtonDown(selectback)) {
				selectedweapon-=1;
				Check();
				Select();
			}	
			else if (Input.GetButtonDown(selectfor)) {
				selectedweapon+=1;
				Check();
				Select();
			}
			
		}	
	}	
	
	void Check(){//so not over or under the number of weapons
		
		if(selectedweapon > weapons.Count - 1){
			selectedweapon = weapons.Count - 1;	
		}	
		if(selectedweapon < 0){
			selectedweapon=0;
		}
		
	}
	
	void Select(){
		
		if(weapons.Count>0){
			for (var i = 0; i < weapons.Count ; i++) {
				weapons[i].BroadcastMessage("DisArm", SendMessageOptions.DontRequireReceiver);
			}
			weapons[selectedweapon].BroadcastMessage("Arm", SendMessageOptions.DontRequireReceiver);

		}
		else{

		}
	}
	
	public void AddWeapon(Weapon x){
		
		weapons.Add ( x );
		x.DisArm();
		
	}

	public void DropAllWeapons(){
		
		foreach(Weapon w in weapons){
			w.Drop();
		}
		
		weapons.Clear();
	}
}
