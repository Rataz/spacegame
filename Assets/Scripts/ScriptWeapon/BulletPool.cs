﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BulletPool : MonoBehaviour {


	private List<Bullet> bullets;

	public Bullet bulletPrefab;

	// Use this for initialization

	void Start () {
		bullets = new List<Bullet>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public bool isStackEmpty(){

		bool ret = false;

		if( bullets.Count <= 10 ){
			ret = true;
		}

		return ret;
	}

	public void Push( Bullet bull ){
		bullets.Add( bull );
	}

	public void SpawnBullet( Vector3 position , Quaternion rotation , int team ){

		Bullet bull;

		if( isStackEmpty() ){
			bull = Instantiate( bulletPrefab , position , rotation ) as Bullet;
		}
		else{
			bull = Pop ();
			bull.Begin(  position ,  rotation  );
		}

		bull.SetTeam( team );
	}

	public Bullet GetBullet( int team ){

		Bullet bull;
		if( isStackEmpty() ){
			bull = Instantiate( bulletPrefab , transform.position , transform.rotation ) as Bullet;
		}
		else{
			bull = Pop();
		}
		bull.SetTeam( team );
		return bull;
	}

	public Bullet Pop(){
		Bullet bull = bullets[0];
		bullets.Remove( bull );
		return bull;
	}

}
