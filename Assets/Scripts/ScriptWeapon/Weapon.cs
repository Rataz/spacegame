﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Weapon : MonoBehaviour {

	public int weaponNo = 0;

	public int farRating=10;
	public int medRating=20;
	public int meleRating=5;

	public int ammo=999;
	public float range=10;
	public bool infinteAmmo=false;
	public float reloadTime=1;
	public bool canFire = false;
	public int shotsPerReload=6;
	protected bool loaded=true;
	protected int shotCount=0;
	public float firingRate = .5f;

	public int damage=10;
	public Animator anim;
	public Sprite weaponIcon;

	public Renderer rend;



	public virtual void Fire(){
	}

	protected bool loading=false;
	public virtual IEnumerator WaitTillReloaded(){

		if(!loading){
	
			loading=true;
			loaded=false;
			yield return new WaitForSeconds(reloadTime);
			loaded=true;
			loading=false;
			shotCount=0;

		}
	}

	protected bool betweenShots=false;
	public virtual IEnumerator WaitTillNextShot(){
		if(!betweenShots){
			betweenShots=true;
			yield return new WaitForSeconds(firingRate);
			betweenShots=false;
		}
	}

	public virtual void Arm(){
		canFire=true;

		if(GetComponent<Rigidbody>()){
			//rigidbody.freezeRotation=true;
			//rigidbody
			//rigidbody.detectCollisions=false;
			//rigidbody.Sleep();
			//rigidbody.useGravity=false;
		}
	}

	public virtual void DisArm(){
		canFire=false;

		if(GetComponent<Rigidbody>()){
			//rigidbody.detectCollisions=false;
			//rigidbody.Sleep();
			//rigidbody.useGravity=false;
		}
	}

	public virtual void Drop(){

		if(GetComponent<Rigidbody>()){
			GetComponent<Rigidbody>().detectCollisions=true;
			GetComponent<Rigidbody>().isKinematic=false;
		}

		if(rend){
			rend.enabled=true;
		}

		canFire=true;
		anim=null;
		transform.parent=null;
	}

	public virtual int GetAmmo(){
		return ammo;
	}

	public virtual int GetShotsPerReload(){
		return shotsPerReload;
	}

	public void SetWielderAnimator(Animator wielder){
		anim = wielder;
	}

	public Sprite GetIcon(){
		return weaponIcon;
	}


	/*
	public Transform GetHandle(){
		return handle;
	}
*/
}
