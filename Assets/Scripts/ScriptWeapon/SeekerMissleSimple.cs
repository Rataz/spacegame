﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Simple persuit, no lead or lag
 * */
public class SeekerMissleSimple : Missle {

	public bool fullTracking = false;

	//degrees to either side of center to track target
	public float coneAngle=10;

	void Awake(){
		tr = transform;
		rb = transform.GetComponent<Rigidbody> ();
	}

	void FixedUpdate(){
		float forwardVelocity = Vector3.Dot ( rb.velocity, tr.forward);
		if (forwardVelocity < projectileSpeed) {
			rb.AddRelativeForce (0, 0, thrust);
		}
	}

	void Update () {

		if (target != null) {

			if (fullTracking) {
				Track ();
			} else {
				if (CheckFov (target.position,  coneAngle)) {
					Track ();
				} else {
					target = null;
				}

			}

		}

	}

	//point towards target
	public void Track(){
		Vector3 targetDir = target.position - tr.position;
		float step = rotSpeed * Time.deltaTime;
		Vector3 newDir = Vector3.RotateTowards (tr.forward, targetDir, step, 0.0F);

		Debug.DrawLine (tr.position, target.position, Color.white);

		tr.rotation = Quaternion.LookRotation (newDir);
	}


	//check if position pos is visible within 'angle' off of center
	public bool CheckFov(Vector3 pos, float angle)
	{
		float ang = Vector3.Angle(pos - tr.position, tr.forward);
		bool ret = false;

		if (ang < angle)
		{
			ret = true;
		}

		return ret;
	}



}
