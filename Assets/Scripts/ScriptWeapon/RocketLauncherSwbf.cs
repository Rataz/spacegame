using UnityEngine;
using System.Collections;

public class RocketLauncherSwbf : Weapon {

//	public Renderer minemesh;
//	public Animator anim;
	//public int weaponNo = 3;
	public int primaryWepnNo = 2;
//	private bool canFire=false;
	public int team=0;
	
	public Rigidbody projectile;
	private Rigidbody par;
	public float initialSpeed = 20.0F;
	//public float reloadTime = 0.5F;
	public int ammoCount = 20;
	private float lastShot = -10.0F;
	
//	private int stateid;
	
	void Start () {
//		minemesh.enabled = false;
//		stateid=Animator.StringToHash("Gun.rocketLa");
		par=transform.root.GetComponent<Rigidbody>();
	}
	
	void Update(){
				/*
			if(anim.GetCurrentAnimatorStateInfo(1).nameHash == stateid)
			{
				
				if(anim.GetCurrentAnimatorStateInfo(1).normalizedTime>.9999){
					launch();
					DisArm();
				}else if(anim.GetCurrentAnimatorStateInfo(1).normalizedTime>.1){
					minemesh.enabled = true;
				}
				
			}
			*/
	}
	
	public override void Arm(){
		canFire=true;
	//	minemesh.enabled = true;
	//	anim.SetInteger("WeaponNo",weaponNo);
	}
	
	public override void DisArm(){
		canFire=false;
	//	minemesh.enabled = false;
	//	anim.SetBool("Fire",false);
		//anim.SetInteger("WeaponNo",primaryWepnNo);
	}
	
	public void launch(){
		
		if ( Time.time > reloadTime + lastShot && ammoCount > 0 ) {
				// Instantiate the projectile at the position and rotation of this transform
	            Rigidbody clone;
	            clone = Instantiate(projectile, transform.position, transform.rotation) as Rigidbody;
	            clone.velocity = transform.TransformDirection(Vector3.forward * initialSpeed) + par.velocity;
				
				clone.BroadcastMessage("setTeam",team, SendMessageOptions.DontRequireReceiver);
				
				// Ignore collisions between the missile and the character controller
				//if(transform.root.collider)
				//Physics.IgnoreCollision(clone.collider, transform.root.collider);
				
				lastShot = Time.time;
				ammoCount--;
		}
	}
	
	public override void Fire(){
		
		if( canFire == true ){
				//StartCoroutine(PlayOneShot());		
			//	anim.SetInteger("WeaponNo",weaponNo);
		//		anim.SetBool("Fire",true);
				launch();
			
			/*
			// Did the time exceed the reload time?
			if (Time.time > reloadTime + lastShot && ammoCount > 0) {
				// Instantiate the projectile at the position and rotation of this transform
	            Rigidbody clone;
	            clone = Instantiate(projectile, transform.position, transform.rotation) as Rigidbody;
	            clone.velocity = transform.TransformDirection(Vector3.forward * 10);
		
				// Ignore collisions between the missile and the character controller
				if(transform.root.collider)
				Physics.IgnoreCollision(clone.collider, transform.root.collider);
				
				lastShot = Time.time;
				ammoCount--;
				
			}
			*/
			
			
			//anim.SetInteger("WeaponNo",primaryWepnNo);
			//anim.SetBool("Fire",false);
		//anim.SetInteger("WeaponNo",primaryWepnNo);}
		}
	}
	
	public IEnumerator PlayOneShot ()
    {
		//anim.SetBool("Fire",true);
		//anim.SetInteger("WeaponNo",weaponNo);
        yield return null;
        //anim.SetInteger("WeaponNo",primaryWepnNo);
	//	anim.SetBool("Fire",false);
		//anim.i
    }
}
