﻿using UnityEngine;
using System.Collections;

public class ShipBulletGunWrap : ShipWeapon {

	public BulletGun gun;
	public override void Fire () {
		gun.Arm();
		gun.Fire();
	}
}
