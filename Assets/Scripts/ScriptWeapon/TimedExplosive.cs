﻿using UnityEngine;
using System.Collections;

public class TimedExplosive : MonoBehaviour {

	public float reloadTime = 3.0f;

	public Transform explosionPrefab;
	
	public float explosionRadius = 5.0f;
	public float explosionPower = 10.0f;
	public float explosionDamage = 100.0f;
	public float explosionTimeout = 2.0f;
	public bool smart;
	public int team=0;
	public float time=1;
	public float splodeTime=1.0f;
	public GameObject explosionObj;


	// Use this for initialization
	void Start () {
		StartCoroutine( Reload() );
	}


	IEnumerator Reload () {
		
		// Wait for reload time first - then add more bullets!
		yield return new WaitForSeconds( reloadTime );
		Detonate();
		// We have a clip left reload
		//if (clips > 0) {
		//	clips--;

		//}
	}

	public void Detonate(){
		var explosionPosition = transform.position;
		Collider[] colliders= Physics.OverlapSphere (explosionPosition, explosionRadius);
				
		
		foreach(Collider hit in colliders) {
			
			var closestPoint = hit.ClosestPointOnBounds(explosionPosition);  // Calculate distance from the explosion position to the closest point on the collider
			var distance = Vector3.Distance(closestPoint, explosionPosition);
			var hitPoints = 1.0 - Mathf.Clamp01(distance / explosionRadius);   // The hit points we apply fall decrease with distance from the explosion point
			hitPoints *= explosionDamage;
			
			hit.BroadcastMessage("ApplyDamage", hitPoints, SendMessageOptions.DontRequireReceiver);
			
		}
		
		Instantiate( explosionObj,  explosionPosition, transform.rotation);
		Destroy( gameObject );
	}

	public Color colour = Color.magenta;
	void OnDrawGizmosSelected() {
		Gizmos.color = colour;
		Gizmos.DrawWireSphere(transform.position, explosionRadius );
	}
}
