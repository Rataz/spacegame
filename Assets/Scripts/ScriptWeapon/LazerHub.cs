﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic ;

//Allows control of more than 1 lazer from 1 script

public class LazerHub : MonoBehaviour , IWeapon {

	public List<Weapon> weapons;
	public string fire = "fire1";
	public bool autoGetControls ;
	private bool armed = true;

	public float shotDrain = 4.0f;

	public ShipBase ship ;

	public bool aiControlled = false;

	void Start () {

		foreach(  Weapon w in weapons ){
			w.Arm();
		}

		if(autoGetControls){
			PlayerControls con = transform.root.GetComponent<PlayerControls>();

			if( con )
				fire = con.GetFire1();
			//selectfor = con.GetSelectFwd();
			//selectback = con.GetSelectBack();
		}

	}
	
	void Update () {

		if( armed && !aiControlled ){
			if ( Input.GetButton ( fire ) ){
				Fire();
			}
		}

	}

	public void Fire(){
		foreach(  Weapon w in weapons ){

			if( ship ){
				if( ship.lazer_pow > shotDrain ){
					w.Fire();
					ship.lazer_pow -= shotDrain;
				}
			}
			else
				w.Fire();

		}
	}

	public void GetAmmo(){

	}

	public void Arm(){

	}

	public void Disarm(){

	}
}
