using UnityEngine;
using System.Collections;


/*
 * A non-physics based projectile
 * 
 * */
public class Bullet : MonoBehaviour {

	[Tooltip("travel speed , m/s")]
	public float speed = 10; //travel speed , m/s

	[Tooltip("hp to take away")]
	public float damage = 1; //hp to take away

	[Tooltip("true = allow teamkill")]
	public bool friendlyFire = false; //true = allow teamkill

	[Tooltip("time till dissapear and destory/recycle")]
	public float aliveTime = 5f; //time till dissapear and destory/recycle

	[Tooltip("exposion prefab")]
	public GameObject blasterExplosion; //exposion object

	//deselect layers that should not trigger collision
	public LayerMask selectAllButThisLayer;

	[Tooltip("impact force")]
	public float force = 100; //impact force

	public int team=0; //team owning this bullet


	public BulletPool bulletPool; //cache to reuse bullets
	public Renderer rend; //renderer of object

	private bool stopped = false;
	private Vector3 previousPosition; //for collision detection
	private Transform tr; //cache own transform optimization

	// Use this for initialization
	void Start () {

		//bulletPool = GameObject.FindObjectOfType<BulletPool>();
		tr = transform;
		previousPosition = tr.position;

		StartCoroutine( "waitToDissapear" );
		//Physics.IgnoreCollision(instantiatedProjectile.collider, transform.root.collider);
	}



	void Update(){
		if( !stopped ){
			tr.Translate( Vector3.forward * Time.deltaTime * speed ); //movement
		}
	}

	void FixedUpdate () {
		if( !stopped ){
			CheckCollisions(); 
		}
	}


	public void Stop(){

		if( rend )
			rend.enabled = false;

		stopped = true;

		if( bulletPool )
			bulletPool.Push ( this );

	}

	//for recycling
	public void Go( Vector3 pos , Quaternion rot ){
		Begin( pos , rot );
	}

	public void Begin( Vector3 pos , Quaternion rot ){

		tr.position = pos;
		tr.rotation = rot;
		stopped = false;

		if( rend )
			rend.enabled = true;

		StartCoroutine( "waitToDissapear" );

	}



	//destroy or recycle object after time
	public IEnumerator waitToDissapear(){

		yield return new WaitForSeconds ( aliveTime );

		if( bulletPool ){
			Stop ();
		}
		else{
			Dissapear();
		}

	}

	private void Dissapear(){
		Destroy(gameObject);
	}


	public void SetTeam(int team){
		this.team = team;
	}
	
	void CheckCollisions(){//,selectAllButThisLayer

	    RaycastHit hit;
	    if( Physics.Linecast(previousPosition, tr.position, out hit) ){//add a layermask for nxt var for what to colide with
	        // hit something, can get information back out, now can apply force, damage, particle effects etc.

			IDamageable ii;
			ii = hit.collider.gameObject.GetComponent( typeof(IDamageable) ) as IDamageable ;

			if( ii != null ){
				if( friendlyFire ){
					ii.ApplyDamage( damage );
				}
				else if( team != ii.GetTeam() ){
					ii.ApplyDamage( damage );
				}
			}
						
			if( blasterExplosion )
				Instantiate( blasterExplosion,hit.point , tr.rotation );

			if( hit.rigidbody ){
				hit.rigidbody.AddForceAtPosition( tr.forward*force , hit.point );
			}
			else if( hit.transform.root.GetComponent<Rigidbody>() ){
				hit.transform.root.GetComponent<Rigidbody>().AddForceAtPosition( tr.forward * force, hit.point);
			}

			if( bulletPool ){ //recycle in queue
				Stop();
				bulletPool.Push( this );
			}
			else{
				Dissapear();
			}
	    }

	    previousPosition = tr.position;
	}
	

}
