﻿using UnityEngine;
using System.Collections;


/*
 * Lead persuit missle
 * 
 * 
 * */
public class SeekerMissle : Missle {

	public float armTime = 1.0f;
	private float timeLeft = 20.0f;

	private bool armed = false;
	public Transform explosionPrefab;


	private float lastmag = Mathf.Infinity;

	void Awake(){
		tr = transform;
		rb = transform.GetComponent<Rigidbody> ();
	}


	void FixedUpdate(){
		//thrust
		float forwardVelocity = Vector3.Dot ( rb.velocity, tr.forward);
		if (forwardVelocity < projectileSpeed) {
			rb.AddRelativeForce (0, 0, thrust);
		}
	}


	void Update () {

		//dont arm for a second
		if (armTime > -500f) {
			armTime -= Time.deltaTime;

			if (armTime < 0) {
				armed = true;
			}
		}

		//explode if timer runs out
		timeLeft -= Time.deltaTime;
		if(timeLeft < 0)
		{
			Detonate ();
		}
		
		if (target != null) {
			//explode if getting farther away from target
			if (armed) {

				float mag = (target.position - tr.position).magnitude;
				if (mag > lastmag+1) {
					Detonate ();
				} else {
					lastmag = mag;
				}
			}
						
			Vector3 targetDir = CalculateLead ( target ) - tr.position;
			float step = rotSpeed * Time.deltaTime;
			Vector3 newDir = Vector3.RotateTowards( tr.forward , targetDir, step, 0.0F);

			//Debug.DrawLine( tr.position, CalculateLead(target), Color.yellow);

			tr.rotation = Quaternion.LookRotation(newDir);
		}

	}	

	
	//find position to go towards to hit target
	private Vector3 CalculateLead ( Transform tar ) {
		
		Rigidbody target;
		target = tar.root.GetComponent<Rigidbody>() ;
		
		Vector3 soln = tar.position;

		if( target ){
			
			Vector3 V = target.velocity;
			Vector3 D = tar.position - tr.position;
			float A = V.sqrMagnitude - rb.velocity.sqrMagnitude;
			float B = 2 * Vector3.Dot (D, V);
			float C = Vector3.SqrMagnitude( D );
			
			if ( A >= 0 ) {
				//Debug.DrawLine( tr.position, tar.position, Color.white);
				//Debug.LogError ("No solution exists");
			}
			else {
				float rt = Mathf.Sqrt (B*B - 4*A*C);
				float dt1 = (-B + rt) / (2 * A);
				float dt2 = (-B - rt) / (2 * A);
				float dt = (dt1 < 0 ? dt2 : dt1);
				soln = ( tar.position + V * dt ) ;
			}
		}
		
		return soln;
	}




	//public float explosionDetectRadius = 5.0f;

	void OnCollisionEnter(Collision collision) {

		if (armed) {
			Detonate ();
		}

		/*
		foreach (ContactPoint contact in collision.contacts) {
			Debug.DrawRay(contact.point, contact.normal, Color.white);
		}
		*/
	}

	public void Detonate(){
		Instantiate( explosionPrefab,  transform.position , transform.rotation);
		Destroy( gameObject );
	}


}
