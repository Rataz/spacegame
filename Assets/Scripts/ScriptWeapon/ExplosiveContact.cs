﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveContact : MonoBehaviour {

	public Transform explosionPrefab;

	public float explosionDetectRadius = 5.0f;

	void OnCollisionEnter(Collision collision) {
		Detonate ();
		/*
		foreach (ContactPoint contact in collision.contacts) {
			Debug.DrawRay(contact.point, contact.normal, Color.white);
		}
		*/
	}

	public void Detonate(){
		Instantiate( explosionPrefab,  transform.position , transform.rotation);
		Destroy( gameObject );
	}


}
