using UnityEngine;
using System.Collections;


//positions an object at raycast position
//Sets vector that the weapon actually aims at

public class AimingPt : MonoBehaviour {

	public Vector3 aimPoint; //where the raycast hit
	
	public Transform thing; //object to position wher the ray hit
	
	public Crosshair crosshairScr; //tell crosshair where to position itself
	//public PistolTargetting pistol; 
	public Transform spcAimFrom; //where to aim from if not this objects forward
	
	public bool aimWayFarNoFancy = false; //no raycasting option

	public bool useMousePos = false;
	
	void Start () {
		if( crosshairScr == null ){
			crosshairScr=gameObject.GetComponent<Crosshair>();
		}
	}

	public Vector3 GetAimVector(){

		if( spcAimFrom != null ){
			aimPoint = spcAimFrom.transform.position+transform.forward*50;
		}
		else{
			aimPoint = transform.position + transform.forward*50 ;
		}
		return aimPoint;

	}
	
	void Update () {
		
		if( !aimWayFarNoFancy ){//have not checked off non raycasting
		    RaycastHit hit;



			if( useMousePos ){
				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				
				if ( Physics.Raycast (ray, out hit, Mathf.Infinity)) {
					aimPoint = hit.point;
					
					if( crosshairScr ) 
						crosshairScr.positionVector = hit.point;
					
					//if(pistol)
						//pistol.aimVector = hit.point;
					
					if( thing )
						thing.position=aimPoint;
					
				}
			}
			else
	        if (Physics.Raycast(transform.position, transform.forward ,out hit)){ //hit

				aimPoint = hit.point;

				if( crosshairScr ) 
					crosshairScr.positionVector = hit.point;

				//if(pistol)
				//	pistol.aimVector = hit.point;

				if( thing )
					thing.position=aimPoint;

			}
			else{ //no hit
				aimPoint = transform.position+transform.forward*500;

				if( crosshairScr ) 
					crosshairScr.positionVector = transform.position+transform.forward*500;

				if( thing )
					thing.position = aimPoint;

				//if(pistol)
				//	pistol.aimVector = aimPoint;
			}


        }
		else{ //non raycasting
			if( spcAimFrom != null )
				aimPoint = spcAimFrom.transform.position+transform.forward*50;
			else
				aimPoint = transform.position + transform.forward*50 ;
				//pistol.aimVector = aimPoint;
		}
		
	}
}
