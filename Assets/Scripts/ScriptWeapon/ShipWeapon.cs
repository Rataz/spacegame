﻿using UnityEngine;
using System.Collections;


/**
 * Inherit from this to make more weapons for ships
 * 
 * */
public class ShipWeapon : MonoBehaviour {

	public float firingRate = .5f;
		
	public int bulletsPerClip = 40;
	public int clips = 20;
	public Renderer muzzleFlash;
	
	public int bulletsLeft = 0;
	
	protected bool canFire = false;
	public GameObject bullet;
	
	public bool AIcontrolled=true;
	protected float timeToDisableFlash=0;
	public float flashTime=0.01f;
	public bool flashOn=false;

	public float reloadTime=1;
	
	
	void Start () {				
		bulletsLeft = bulletsPerClip;
	}

	public virtual void Fire () {
	}

	public virtual void Fire ( float range ) {
	}

	public virtual void Fire ( float range , float accuracy ) {
	}

	public virtual void Arm () {
	}

	public virtual void DisArm (){
	}
	
	private void FireOneShot () {
	}
		
	IEnumerator Reload () {
		yield return new WaitForSeconds(reloadTime);// Wait for reload time first, then add more bullets!
		bulletsLeft = bulletsPerClip;
	}
}
