using UnityEngine;
using System.Collections;

public class BulletGun : Weapon {

	private BulletPool bulletPool;

	public int bulletsPerClip = 40;
	public int clips = 20;
	public Renderer muzzleFlash;

	public int bulletsLeft = 0;
	private double nextFireTime = 0.0;

	public GameObject bullet;

	public bool AIcontrolled=true;
	private float timeToDisableFlash=0;
	public float flashTime=0.01f;
	public bool flashOn=false;

	private SoldierStatus stats;

	void Start () {
		bulletPool = GameObject.FindObjectOfType<BulletPool>();
		bulletsLeft = bulletsPerClip;
		if(muzzleFlash){
			muzzleFlash.GetComponent<Renderer>().enabled = false;
		}
		stats = transform.root.GetComponent<SoldierStatus>();
	}

	//void  LateUpdate() {
	void  Update() {
		timeToDisableFlash -= Time.deltaTime;
		if (muzzleFlash){
			if(flashOn==true)
				timeToDisableFlash -= Time.deltaTime;
			
			if (timeToDisableFlash<=0) 	{
				muzzleFlash.GetComponent<Renderer>().enabled = false;
				flashOn=false;
			}	
		}
	}

	public override void Fire () {
		
		if( canFire||AIcontrolled ){
			if (bulletsLeft != 0){
				// If there is more than one bullet between the last and this frame
				// Reset the nextFireTime
				if (Time.time - firingRate > nextFireTime)
					nextFireTime = Time.time - Time.deltaTime;

				while( nextFireTime < Time.time && bulletsLeft != 0) {
					FireOneShot();
					nextFireTime += firingRate;
				}
				
				if(anim)
					anim.SetInteger("WeaponNo",weaponNo);
			}
			
			if(anim)
				anim.SetBool("Fire",false);
		}
	}

	private void FireOneShot () {
		
		if( bulletPool ){
			bulletPool.SpawnBullet( transform.position , transform.rotation , stats.team );
		}
		else if( bullet ){
			GameObject bull = Instantiate( bullet , transform.position , transform.rotation ) as GameObject;

			if( stats )
				bull.GetComponent<Bullet>().SetTeam( stats.team );
		}
		
		//bulletsLeft--;
	
		// Register that we shot this frame,
		// so that the LateUpdate function enabled the muzzleflash renderer for one frame
	//	m_LastFrameShot = Time.frameCount;

		if ( muzzleFlash ){
			muzzleFlash.GetComponent<Renderer>().enabled = true;
			//muzzleFlash.transform.localRotation = Quaternion.AngleAxis(Random.value * 360, Vector3.right);
			timeToDisableFlash=flashTime;
			flashOn=true;
		}
		
		// Reload gun in reload Time		
		if (bulletsLeft == 0)
			Reload();			
	}



	IEnumerator Reload () {
	
		// Wait for reload time first - then add more bullets!
		yield return new WaitForSeconds(reloadTime);
	
		// We have a clip left reload
		//if (clips > 0) {
		//	clips--;
			bulletsLeft = bulletsPerClip;
		//}
	}

	private float GetBulletsLeft () {
		return bulletsLeft;
	}
	
	public override void Arm(){
		canFire=true;
		if(anim){
			anim.SetInteger("WeaponNo",weaponNo);
			anim.SetBool("Fire",false);
		}
	}
	
	public override void DisArm(){
		canFire=false;
	}

	private void DisableFlash(){

	}
	
}
