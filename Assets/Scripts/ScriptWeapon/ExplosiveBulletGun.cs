﻿using UnityEngine;
using System.Collections;


/**
 * Gun which sets a fuse for bullet it fires
 * 
 * */
public class ExplosiveBulletGun : ShipWeapon {

	private double nextFireTime = 0.0;

	public BulletTimedExplosive bx;



	void Start () {
		bulletsLeft = bulletsPerClip;

		if(muzzleFlash){
			muzzleFlash.GetComponent<Renderer>().enabled = false;
		}

	}

	void  Update() {

		timeToDisableFlash -= Time.deltaTime;

		if (muzzleFlash){

			if(flashOn==true)
				timeToDisableFlash -= Time.deltaTime;

			if (timeToDisableFlash<=0) 	{
				muzzleFlash.GetComponent<Renderer>().enabled = false;
				flashOn=false;
			}	
		}
	}

	public override void Fire () {

		if( canFire||AIcontrolled ){
			if (bulletsLeft != 0){
				// If there is more than one bullet between the last and this frame
				// Reset the nextFireTime
				if (Time.time - firingRate > nextFireTime)
					nextFireTime = Time.time - Time.deltaTime;

				while( nextFireTime < Time.time && bulletsLeft != 0) {
					
					FireOneShot();

					nextFireTime += firingRate;
				}
			}
		}
	}

	public override void Fire ( float range , float accuracy ) {

		if( canFire||AIcontrolled ){
			if (bulletsLeft != 0){
				// If there is more than one bullet between the last and this frame
				// Reset the nextFireTime
				if (Time.time - firingRate > nextFireTime)
					nextFireTime = Time.time - Time.deltaTime;

				while( nextFireTime < Time.time && bulletsLeft != 0) {
					FireOneShot( range , accuracy );
					nextFireTime += firingRate;
				}
			}
		}
	}

	public override void Fire ( float range ) {

		if( canFire||AIcontrolled ){
			if (bulletsLeft != 0){
				// If there is more than one bullet between the last and this frame
				// Reset the nextFireTime
				if (Time.time - firingRate > nextFireTime)
					nextFireTime = Time.time - Time.deltaTime;

				while( nextFireTime < Time.time && bulletsLeft != 0) {
					FireOneShot( range );
					nextFireTime += firingRate;
				}
			}
		}
	}

	private void FireOneShot ( ) {

		if(bx)
		{
			Instantiate( bx , transform.position , transform.rotation ) ;
		}

		if ( muzzleFlash ){
			muzzleFlash.GetComponent<Renderer>().enabled = true;
			//muzzleFlash.transform.localRotation = Quaternion.AngleAxis(Random.value * 360, Vector3.right);
			timeToDisableFlash=flashTime;
			flashOn=true;
		}

		// Reload gun in reload Time		
		if (bulletsLeft == 0)
			Reload();			
	}

	private void FireOneShot ( float range, float accuracy ) {

		Vector3 pos = transform.position + transform.forward*range + Random.insideUnitSphere*accuracy;



		if(bx)
		{
			BulletTimedExplosive bull = Instantiate( bx , transform.position ,Quaternion.LookRotation( pos - transform.position ) ) as  BulletTimedExplosive;
			bull.SetRangeFuse( range );
		}

		if ( muzzleFlash ){
			muzzleFlash.GetComponent<Renderer>().enabled = true;
			//muzzleFlash.transform.localRotation = Quaternion.AngleAxis(Random.value * 360, Vector3.right);
			timeToDisableFlash=flashTime;
			flashOn=true;
		}

		// Reload gun in reload Time		
		if (bulletsLeft <= 0)
			Reload();

	}

	private void FireOneShot ( float range ) {
		
		if(bx)
		{
			BulletTimedExplosive bull = Instantiate( bx , transform.position , transform.rotation ) as  BulletTimedExplosive;
			bull.SetRangeFuse( range );
		}

		if ( muzzleFlash ){
			muzzleFlash.GetComponent<Renderer>().enabled = true;
			//muzzleFlash.transform.localRotation = Quaternion.AngleAxis(Random.value * 360, Vector3.right);
			timeToDisableFlash=flashTime;
			flashOn=true;
		}

		// Reload gun in reload Time		
		if (bulletsLeft <= 0)
			Reload();
		
	}



	IEnumerator Reload () {
		yield return new WaitForSeconds(reloadTime);
		bulletsLeft = bulletsPerClip;
	}

	private float GetBulletsLeft () {
		return bulletsLeft;
	}

	public override void Arm(){
		canFire=true;

	}

	public override void DisArm(){
		canFire=false;
	}

	private void DisableFlash(){

	}



}
