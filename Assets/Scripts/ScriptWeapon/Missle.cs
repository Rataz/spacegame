﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missle : MonoBehaviour {
	
	public Transform target;
	protected Transform tr;
	protected Rigidbody rb;

	public float projectileSpeed = 100f;
	public float rotSpeed = 15f;

	public float thrust=5.0f;


	public void SetFuse(){

	}

	public void SetVelocity( Vector3 velo ){
		rb.velocity = velo;
	}

	public void SetTarget(Transform tar){
		target = tar;
	}

}
