﻿using UnityEngine;
using System.Collections;

public class BulletTimedExplosive : MonoBehaviour {


	private Transform tr; //cache own transform optimization

	//travel speed , m/s
	public float speed = 400; 

	public float damage = 1; //hp to take away from hit object

	public bool friendlyFire = false; //true = allow teamkill

	public float aliveTime = 5f; //time till dissapear and destory/recycle, just deletes after this time

	//time before detonation, creates explosion
	private float explodeTime = 5.0f;

	public GameObject blasterExplosion; //exposion object

	//deselect layers that should not trigger collision
	public LayerMask selectAllButThisLayer;

	public float force = 100; //impact force

	public int team=0; //team owning this bullet


	public Renderer rend; //renderer of object

	private bool stopped = false;
	private Vector3 previousPosition; //for collision detection

	//private float rangeToExplode = 9999;



	private float timeLeft = 100.0f;


	// Use this for initialization
	void Start () {

		//bulletPool = GameObject.FindObjectOfType<BulletPool>();
		tr = transform;
		previousPosition = tr.position;

		StartCoroutine( "waitToDissapear" );
		//Physics.IgnoreCollision(instantiatedProjectile.collider, transform.root.collider);
	}

	public void SetRangeFuse( float range ){
		//rangeToExplode = range;

		range = Mathf.Abs( range );
		float time = range/speed;

		SetExplodeTimer( time );

		//print( "range: "  + range + "      time: " + time);

	}

	public void SetExplodeTimer( float time ){



		if( time <= 0){
			time =0.1f;
		}
		else{
			explodeTime = time;
		}

		timeLeft = time;
	}


	void Update(){



		if( !stopped ){

			//movement
			tr.Translate( Vector3.forward * Time.deltaTime * speed ); 


			timeLeft -= Time.deltaTime;
			if( timeLeft <0){
				Explode();
			}

		}
	}

	void FixedUpdate () {
		if( !stopped ){
			CheckCollisions(); 
		}
	}


	public void Stop(){

		if( rend )
			rend.enabled = false;

		stopped = true;
	}

	//for recycling
	public void Go( Vector3 pos , Quaternion rot ){
		Begin( pos , rot );
	}

	public void Begin( Vector3 pos , Quaternion rot ){

		tr.position = pos;
		tr.rotation = rot;
		stopped = false;

		if( rend )
			rend.enabled = true;

		StartCoroutine( "waitToDissapear" );

	}



	//destroy or recycle object after time
	public IEnumerator waitToDissapear(){
		yield return new WaitForSeconds ( aliveTime );
		Dissapear();
	}

	//destroy or recycle object after time
	public IEnumerator waitToExplode(){
		yield return new WaitForSeconds ( explodeTime );
		Explode();
	}


	private void Dissapear(){
		Destroy(gameObject);
	}

	public void Explode(){
		if( blasterExplosion )
			Instantiate( blasterExplosion, tr.position , tr.rotation );

		Dissapear();
	}


	void CheckCollisions(){//,selectAllButThisLayer

		//distangeTraveled += Vector3.Distance( previousPosition, tr.position);

		RaycastHit hit;
		if( Physics.Linecast(previousPosition, tr.position, out hit , selectAllButThisLayer) ){//add a layermask for nxt var for what to colide with
			// hit something, can get information back out, now can apply force, damage, particle effects etc.

			IDamageable ii;
			ii = hit.collider.gameObject.GetComponent( typeof(IDamageable) ) as IDamageable ;

			if( ii != null ){
				if( friendlyFire ){
					ii.ApplyDamage( damage );
				}
				else if( team != ii.GetTeam() ){
					ii.ApplyDamage( damage );
				}
			}

			if( blasterExplosion )
				Instantiate( blasterExplosion,hit.point , tr.rotation );

			if( hit.rigidbody ){
				hit.rigidbody.AddForceAtPosition( tr.forward*force , hit.point );
			}
			else if( hit.transform.root.GetComponent<Rigidbody>() ){
				hit.transform.root.GetComponent<Rigidbody>().AddForceAtPosition( tr.forward * force, hit.point);
			}

			Dissapear();

		}

		previousPosition = tr.position;
	}


	public void SetTeam(int team){
		this.team = team;
	}


}
