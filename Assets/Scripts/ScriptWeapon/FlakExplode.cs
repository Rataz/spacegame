﻿using UnityEngine;
using System.Collections;

public class FlakExplode : MonoBehaviour {
	
	public float explosionRadius = 5.0f;
	public float explosionPower = 10.0f;
	public float explosionDamage = 100.0f;
	public float explosionTimeout = 2.0f;

	public float time=1;
	public float splodeTime=1.0f;


	// Use this for initialization
	void Start () {
		Detonate();
	}

	public void Detonate(){
		var explosionPosition = transform.position;
		Collider[] colliders= Physics.OverlapSphere (explosionPosition, explosionRadius);

		foreach(Collider hit in colliders) {

			var closestPoint = hit.ClosestPointOnBounds(explosionPosition);  // Calculate distance from the explosion position to the closest point on the collider
			var distance = Vector3.Distance(closestPoint, explosionPosition);
			var hitPoints = 1.0 - Mathf.Clamp01(distance / explosionRadius);   // The hit points we apply fall decrease with distance from the explosion point
			hitPoints *= explosionDamage;

			hit.BroadcastMessage("ApplyDamage", hitPoints, SendMessageOptions.DontRequireReceiver);

		}

		StartCoroutine( "Del");
	}

	public IEnumerator Del(){
		yield return new WaitForSeconds( 3 );
		Destroy( gameObject );
	}


	public Color colour = Color.magenta;
	void OnDrawGizmosSelected() {
		Gizmos.color = colour;
		Gizmos.DrawWireSphere(transform.position, explosionRadius );
	}


}
