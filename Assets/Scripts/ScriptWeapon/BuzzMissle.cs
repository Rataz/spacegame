﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuzzMissle : MonoBehaviour {

	Transform tr;
	Rigidbody rb;
	public Transform target;

	public float coneAngle = 45;
	public float projectileSpeed = 50;
	public float rotSpeed = 30;
	public float thrust = 10;

	public int team = 1;
	public float radius = 100;
	public GlobalSceneInfo sceneinfo;

	bool engineOn = false;
	bool collided = false;

	void Awake(){
		tr = transform;
		rb = transform.GetComponent<Rigidbody> ();
		sceneinfo = GameObject.FindObjectOfType<GlobalSceneInfo> ();
	}

	void FixedUpdate(){

		if (engineOn) {
			float forwardVelocity = Vector3.Dot ( rb.velocity, tr.forward);

			if (forwardVelocity < projectileSpeed) {
				rb.AddRelativeForce (0, 0, thrust);
			}
		}

	}


	void OnCollisionEnter(Collision collision)
	{
		engineOn = false;
		transform.parent = collision.transform;
		rb.isKinematic = true;
		collided = true;
	}


	void Update () {

		if (!collided) {
			if (target != null) {

				if (CheckFov (target.position, coneAngle)) {
					engineOn = true;
					Track ();
				} else {
					target = null;
					engineOn = false;
				}
			} else {
				engineOn = false;
			}
		}


	}

	//point towards target
	public void Track(){
		Vector3 targetDir = target.position - tr.position;
		float step = rotSpeed * Time.deltaTime;
		Vector3 newDir = Vector3.RotateTowards (tr.forward, targetDir, step, 0.0F);

		Debug.DrawLine (tr.position, target.position, Color.white);

		tr.rotation = Quaternion.LookRotation (newDir);
	}

	//check if position pos is visible within 'angle' off of center
	public bool CheckFov(Vector3 pos, float angle)
	{
		float ang = Vector3.Angle(pos - tr.position, tr.forward);
		bool ret = false;

		if (ang < angle)
		{
			ret = true;
		}

		return ret;
	}

}
