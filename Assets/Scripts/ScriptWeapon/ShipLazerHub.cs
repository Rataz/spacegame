﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*
 * For ships with multiple guns that should be fired at the same time
 * */
public class ShipLazerHub : ShipWeapon {
	
	public List<Weapon> weapons;
	
	public float shotDrain = 4.0f;

	public SpaceAItest1 aiShip;

	[Tooltip("Where guns should converge to")]
	public Transform convergeTarget;
	
	void Start () {
		foreach(  Weapon w in weapons ){
			w.Arm();
		}
	}

	public override void Fire(){

		float range = 200;

		if( aiShip ){
			range = aiShip.GetTargetLeadRange();
		}

		Vector3 hitpos = transform.position + transform.forward*( range + 10 );

		foreach(  Weapon w in weapons ){
			w.transform.LookAt( hitpos );
			w.Fire();
		}

	}

	public void LateUpdate(){
		if( convergeTarget ){
			foreach(  Weapon w in weapons ){
				w.transform.LookAt( convergeTarget );
			}
		}
	}
	
	public void GetAmmo(){
		
	}

	public override void Arm(){
		
	}
	
	public void Disarm(){
		
	}

}
