﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class WeaponSelector : MonoBehaviour {

	public bool UseWeapons = false;
	public List<Weapon> weapons;
	public int selectedweapon =1;
	public int playerNo = 1;
	
	public string p1Fire1 = "Fire1";
	
	public string selectback = "selectback";
	public string selectfor = "selectfor";
	public string selectbackp2 = "selectbackp2";
	public string selectforp2 = "selectforp2";

	public Image uiWeaponImage;	
	public bool autoGetControls = true;

	void Start () {

		if( UseWeapons ){
			Check();
			Select();
		}
		
		if(weapons.Count>0){
			for (var i = 0; i < weapons.Count ; i++) {
				//weapons[i].SetActive(false);
				weapons[i].BroadcastMessage("DisArm", SendMessageOptions.DontRequireReceiver);
			}
		}

		if(autoGetControls){
			PlayerControls con = transform.root.GetComponent<PlayerControls>();
			p1Fire1 = con.GetFire1();
			selectfor = con.GetSelectFwd();
			selectback = con.GetSelectBack();
		}

	}

	void Update (){
		
		if( UseWeapons == true ){

			if ( Input.GetButton (p1Fire1) ){
				BroadcastMessage("Fire", SendMessageOptions.DontRequireReceiver);
				SetUIAmmo();
			}
			
			if ( Input.GetButtonUp (p1Fire1 )){
				
			}
			
			if ( Input.GetButtonDown(selectback )) {
				selectedweapon-=1;
				Check();
				Select();
			}	
			else if ( Input.GetButtonDown(selectfor) ) {
				selectedweapon+=1;
				Check();
				Select();
			}
			
		}	
	}

	public void SelectNextWeapon(){
		selectedweapon+=1;
		Check();
		Select();
	}

	public void SelectPreviousWeapon(){
		selectedweapon-=1;
		Check();
		Select();
	}

	public void SelectDefaultWeapon(){

	}

	public void SelectWeaponNo( int number ){

	}

	public Weapon GetSelectedWeapon(){
		return weapons[selectedweapon];
	}
	
	void Check(){//so not over or under the number of weapons

		if(selectedweapon > weapons.Count - 1){
			selectedweapon = weapons.Count - 1;	
		}	
		if(selectedweapon < 0){
			selectedweapon=0;
		}
		
	}
	
	void Select(){

		if(weapons.Count>0){
			for (var i = 0; i < weapons.Count ; i++) {
				//weapons[i].SetActive(false);
				weapons[i].BroadcastMessage("DisArm", SendMessageOptions.DontRequireReceiver);
			}
			//weapons[selectedweapon].SetActive(true);
			weapons[selectedweapon].BroadcastMessage("Arm", SendMessageOptions.DontRequireReceiver);

			if( uiWeaponImage )
				uiWeaponImage.sprite = weapons[selectedweapon].GetIcon();

			SetUIAmmo();
		}
		else{
			//weapons[selectedweapon].SetActive(true);
			//weapons[selectedweapon].BroadcastMessage("Arm", SendMessageOptions.DontRequireReceiver);
		}
	}

	public void AddWeapon(Weapon x){

		weapons.Add (x);
		x.DisArm();

	}

	public Text ammoText;
	public int ammo = 0;

	public void SetUIAmmo(){

		if( weapons.Count>0 ){
			ammo = weapons[selectedweapon].GetAmmo();

			if( ammoText ){
				ammoText.text = ammo.ToString();
			}
		}
	}

	public void DropAllWeapons(){

		foreach(Weapon w in weapons){
			w.Drop();
		}

		weapons.Clear();
	}

}












