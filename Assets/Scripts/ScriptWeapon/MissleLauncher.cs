﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/**
 * 
 * Requires a rigidbody at root of object
 * 
 * 
 * 
 * */
public class MissleLauncher : MonoBehaviour {

	private Rigidbody rb;
	private float lastShot;
	private bool canFire = true;



	public Missle projectile;
	public double reloadTime = 7.0;
	public int ammoCount=999;
	public float initialSpeed = 5;
	

	void Start () {
		lastShot = Time.time;
		rb=transform.root.GetComponent<Rigidbody>();
	}

	public void launch( Transform target ){
		if ( Time.time > reloadTime + lastShot && ammoCount > 0 ) {
			Missle missle;
			missle = Instantiate(projectile, transform.position, transform.rotation) as Missle;
			missle.SetTarget (target);
			missle.SetVelocity (transform.TransformDirection (Vector3.forward * initialSpeed) + rb.velocity);

			lastShot = Time.time;
			ammoCount--;
		}
	}

	//fire with partial lock

	//fire with full lock

	//fire with lock
	public void Fire(Transform target){
		if( canFire ){
			launch( target );
		}
	}

	//dumb fire
	public void Fire(){
		if( canFire ){
			launch( null );
		}
	}

	public void Arm(){
		canFire=true;
	}

	public void DisArm(){
		canFire=false;
	}



}
