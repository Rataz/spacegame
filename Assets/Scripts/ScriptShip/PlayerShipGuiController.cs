﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/*
 * Used to control GUI elements
 * 
 * Put this script on an empty in the scene, 
 * only 1 per scene
 * dont put it on a ship
 * Use a gui updater script on a ship to update the UI using this script
 * */
public class PlayerShipGuiController : MonoBehaviour {

	//GUI variables---------------------
	public Slider shield_f_Slider ;
	public Slider shield_b_Slider ;
	public Slider shield_distribSwitch_Slider ;

	public Slider engineSlider ;
	public Slider lazerSlider ;
	public Slider throttleSlider ;

	public Slider shieldRCslider ;
	public Slider lazerRCslider ;
	public Slider engineRCslider ;

	public Slider hpslider ;
	//----------------------------------



	public void SetLazerSliderValue( float value ){
		lazerSlider.value = value ;
	}

	public void SetLazerRCSliderValue( float value ){
		lazerRCslider.value = value ;
	}

	public void SetEngineRCSliderValue( float value ){
		engineRCslider.value = value ;
	}

	public void SetEngineSliderValue( float value ){
		engineSlider.value = value ;
	}

	public void SetShieldRCSliderValue( float value ){
		shieldRCslider.value = value ;
	}

	public void SetShieldBSliderValue( float value ){
		shield_b_Slider.value = value ;
	}

	public void SetShieldFSliderValue( float value ){
		shield_f_Slider.value = value ;
	}

	public void SetThrottleSliderValue( float value ){
		throttleSlider.value = value ;
	}

	public void SetShieldDistribSwitchSliderValue( float value ){
		shield_distribSwitch_Slider.value = value ;
	}

	public void SetHpSliderValue( float value ){
		hpslider.value = value ;
	}






	public void SetEngineSliderBounds( float top, float bottom ){
		if (engineSlider != null)
		{
			engineSlider.maxValue = top;
			engineSlider.minValue = bottom;
		}		
	}


	public void SetLazerSliderBounds( float top, float bottom ){
		if (lazerSlider != null)
		{
			lazerSlider.maxValue = top;
			lazerSlider.minValue = bottom;
		}
		
	}

	public void SetLazerRCSliderBounds( float top, float bottom ){
		if (lazerRCslider != null)
		{
			lazerRCslider.maxValue = top;
			lazerRCslider.minValue = bottom;
		}
	}

	public void SetShieldRCSliderBounds( float top, float bottom  ){
		shieldRCslider.maxValue =  top;
		shieldRCslider.minValue = bottom;

	}

	public void SetShieldFSliderBounds( float top, float bottom ){
		shield_f_Slider.minValue = bottom;
		shield_f_Slider.maxValue = top;
	}

	public void SetShieldBSliderBounds( float top, float bottom ){
		shield_b_Slider.minValue = bottom;
		shield_b_Slider.maxValue = top;
	}

	public void SetHpSliderBounds( float top , float bottom ){
		hpslider.minValue = bottom;
		hpslider.maxValue = top; //assuming spawn at full hp
	}





	public void SetLazerSlider( Slider slide ){
		lazerSlider = slide ;
	}

	public void SetLazerRCSlider( Slider slide ){
		lazerRCslider = slide ;
	}

	public void SetEngineRCSlider( Slider slide ){
		engineRCslider = slide ;
	}

	public void SetEngineSlider( Slider slide ){
		engineSlider = slide ;
	}

	public void SetShieldRCSlider( Slider slide ){
		shieldRCslider = slide ;
	}

	public void SetShieldBSlider( Slider slide ){
		shield_b_Slider = slide ;
	}

	public void SetShieldFSlider( Slider slide ){
		shield_f_Slider = slide ;
	}

	public void SetThrottleSlider( Slider slide ){
		throttleSlider = slide ;
	}

	public void SetShieldDistribSwitchSlider( Slider slide ){
		shield_distribSwitch_Slider = slide ;
	}

	public void SetHpSlider( Slider slide ){
		hpslider = slide ;
	}


}
