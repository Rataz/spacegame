﻿using UnityEngine;
using System.Collections;


/*
 * Put one in the scene on an empty with the name ControlsP1  and another with the name ControlsP2
 * 
 * Stores names of control input axises so scripts can find them,
 * no need to look in project settings all the time and type axis names for each script
 * also makes it easier to change controls between games
 * 
 * */
public class PlayerControls : MonoBehaviour {

	public int playerNo = 1; //1 or 2

	public string moveSideAxisP1 = "Horizontal";
	public string moveFwdAxisP1 =  "Vertical";
	public string lookUpAxisP1 = "Mouse Y";
	public string lookSideAxisP1 = "Mouse X";
	public string fire1P1 = "Fire1";
	public string jumpP1 = "Jump";
	public string crouchP1 = "Crouch";
	public string useP1 = "use";
	public string grabP1 = "grab2";
	public string selectFwdP1 = "selectfor";
	public string selectBackP1 = "selectback";
		
	public string moveSideAxisP2 = "Horizontal2";
	public string moveFwdAxisP2 = "Vertical2";
	public string lookUpAxsiP2 = "p2lx";
	public string lookSideAxisP2 = "p2ly";
	public string fire1P2 = "Fire1p2";
	public string jumpP2 = "Jump2";
	public string crouchP2 = "Crouchp2";
	public string useP2 = "use2";
	public string grabP2 = "grab";
	public string selectFwdP2 = "selectforp2";
	public string selectBackP2 = "selectbackp2";


	public string lockTargetInFrontP1 = "LockTargetInFrontP1";
	public string lockTargetInFrontP2 = "LockTargetInFrontP2";

	public string lazerPowerToggleP1 = "LazerPowerP1";
	public string lazerPowerToggleP2 = "LazerPowerP2";

	public string shieldPowerToggleP1 = "shieldPowerP1";
	public string shieldPowerToggleP2 = "shieldPowerP2";

	public string shieldDistributionP1 = "shieldDistributionP1";
	public string shieldDistributionP2 = "shieldDistributionP2";

	public string shield_to_lazerP1 = "shield_to_lazerP1";
	public string lazer_to_shieldP1 = "lazer_to_shieldP1";

	public string shield_to_lazerP2 = "shield_to_lazerP1";
	public string lazer_to_shieldP2 = "lazer_to_shieldP1";

	public string coastP2 = "CoastToggleP2";
	public string coastP1 = "CoastToggleP1";

	public string capitalShipRollP1 = "CapitalShipRollP1";
	public string capitalShipRollp2 = "CapitalShipRollP2";

	public string capitalShipThrP1 = "CapitalShipThrP1";
	public string capitalShipThrP2 = "CapitalShipThrP2";

	public string GetCapitalShipThrottle(){
		string s;
		if(playerNo ==1 )
			s = capitalShipThrP1 ;
		else
			s = capitalShipThrP2 ;
		return s ;
	}

	public string GetCapitalShipRoll(){
		string s;
		if(playerNo ==1 )
			s = capitalShipRollP1 ;
		else
			s = capitalShipRollp2;
		return s ;
	}

	public string GetShield2Laz(){
		string s;
		if(playerNo ==1 )
			s =shield_to_lazerP1 ;
		else
			s =shield_to_lazerP2;
		return s ;
	}

	public string GetLaz2Shield(){
		string s;
		if(playerNo ==1 )
			s =lazer_to_shieldP1;
		else
			s = lazer_to_shieldP2;
		return s ;
	}

	public string GetCoastToggle(){
		string s;
		if(playerNo ==1 )
			s = coastP1 ;
		else
			s = coastP2 ;
		return s ;
	}

	public string GetLazerPowerToggle(){
		string s;
		if(playerNo ==1 )
			s = lazerPowerToggleP1 ;
		else
			s = lazerPowerToggleP2 ;
		return s ;
	}


	public string GetShieldPowerToggle(){
		string s;
		if(playerNo ==1 )
			s = shieldPowerToggleP1 ;
		else
			s = shieldPowerToggleP2 ;
		return s ;
	}

	public string GetShieldDistributionSelector(){
		string s;
		if(playerNo ==1 )
			s = shieldDistributionP1 ;
		else
			s = shieldDistributionP2 ;
		return s ;
	}


	public string GetlockTargetInFront(){
		string s;
		if(playerNo==1)
			s=lockTargetInFrontP1;
		else
			s=lockTargetInFrontP2;
		return s;
	}

	/*
	public Animator anim;
	public Animator GetAnimator(){
		return anim;
	}
	*/

	public int GetPlayerNo(){
		return playerNo;
	}

	public string GetCrouch(){
		string s;
		if(playerNo==1)
			s=crouchP1;
		else
			s=crouchP2;
		return s;
	}

	public string GetMoveSideAxis(){
		string s;
		if(playerNo==1)
			s=moveSideAxisP1;
		else
			s=moveSideAxisP2;
		return s;
	}

	public string GetMoveFwdAxis(){
		string s;
		if(playerNo==1)
			s=moveFwdAxisP1;
		else
			s=moveFwdAxisP2;
		return s;
	}

	public string GetLookUpAxis(){
		string s;
		if(playerNo==1)
			s=lookUpAxisP1;
		else
			s=lookUpAxsiP2;
		return s;
	}

	public string GetLookSideAxis(){
		string s;
		if(playerNo==1)
			s = lookSideAxisP1;
		else
			s = lookSideAxisP2;
		return s;
	}

	public string GetFire1(){
		string s;
		if(playerNo==1)
			s = fire1P1;
		else
			s = fire1P2;
		return s;
	}

	public string GetJump(){
		string s;
		if(playerNo==1)
			s = jumpP1;
		else
			s = jumpP2;
		return s;
	}

	public string GetUse(){
		string s;
		if(playerNo==1)
			s = useP1;
		else
			s = useP2;
		return s;
	}

	public string GetGrab(){
		string s;
		if(playerNo==1)
			s = grabP1;
		else
			s = grabP2;
		return s;
	}

	public string GetSelectFwd(){
		string s;
		if(playerNo==1)
			s = selectFwdP1;
		else
			s = selectFwdP2;
		return s;
	}

	public string GetSelectBack(){
		string s;
		if(playerNo==1)
			s = selectBackP1;
		else
			s = selectBackP2;
		return s;
	}

}
