﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//sets UI variables, 
public class ShipRespawnPlayer : MonoBehaviour {
	
	public ShipBase prefab;
	public ShipBase prefab2;

	public CameraAttacher cam;

	//GUI variables---------------------
	//Set these in the inspector in the scene
	public Slider shield_f_Slider ;//forward
	public Slider shield_b_Slider ;//rear
	public Slider shield_distribSwitch_Slider ;//front or back or in between
	
	public Slider engineSlider ;//engine power
	public Slider lazerSlider ;//lazer power
	public Slider throttleSlider ;//throttle set by player
	
	public Slider shieldRCslider ;//recharge
	public Slider lazerRCslider ;//recharge
	public Slider engineRCslider ;//recharge

	public Slider hpslider ;//hitpoints
	//----------------------------------


	public ShipsList shiplist;

	public string enemyname = "CIS"; //label for top of screen
	public string friendlyname = "Republic";
	public int playerteam = 1;

	private int controlScheme = 1;


	void Awake(){
		
		Spawn( );
	}

	public void Start(){

	}


	public void Spawn(  ){

		select_persist persistent = GameObject.FindObjectOfType<select_persist>();


		if( persistent ){
			
			controlScheme  = persistent.controlScheme;

			playerteam = persistent.team;
		
			if( persistent.team == 1 ){//cis
				SpawnCIS();
			}
			else if( persistent.team == 0 ){//republic
				SpawnRep();
			}

		}
		else{
			//default to CIS
			SpawnCIS();
		}

	}

	public void SpawnCIS(){
		ShipBase clone;
		clone = Instantiate( prefab , transform.position , transform.rotation ) as ShipBase;
		clone.cam = cam;
		cam.AttachTo( clone.camPos );
		shiplist.SetPlayerShip( clone.transform );
		ResetUI( clone );
		ResetControlScheme( clone , controlScheme );
	}

	public void SpawnRep(){
		ShipBase clone;
		clone = Instantiate( prefab2 , transform.position , transform.rotation ) as ShipBase;
		clone.cam = cam;
		cam.AttachTo( clone.camPos );
		shiplist.SetPlayerShip( clone.transform );
		ResetUI( clone );
		ResetControlScheme( clone , controlScheme );
	}

	public void ResetControlScheme( ShipBase clone , int scheme ){
		PlayerControls x = clone.gameObject.GetComponent<PlayerControls>();
		x.playerNo = scheme;
	}

	//for setting the sliders when player has died and respawned
	public void ResetUI( ShipBase clone ){

		if( engineSlider )
			clone.SetEngineSlider( engineSlider );
		
		if( shield_f_Slider )
			clone.SetShieldFSlider( shield_f_Slider );
		
		if( shield_b_Slider )
			clone.SetShieldBSlider( shield_b_Slider );
		
		if( lazerSlider )
			clone.SetLazerSlider( lazerSlider );
		
		if( throttleSlider )
			clone.SetThrottleSlider( throttleSlider );
		
		if( shieldRCslider )
			clone.SetShieldRCSlider( shieldRCslider );
		
		if( lazerRCslider )
			clone.SetLazerRCSlider ( lazerRCslider ) ;
		
		if( engineRCslider )
			clone.SetEngineRCSlider( engineRCslider );

		if( shield_distribSwitch_Slider )
			clone.SetShieldDistribSwitchSlider( shield_distribSwitch_Slider );

		if( hpslider )
			clone.SetHpSlider( hpslider );
		
	}



}
