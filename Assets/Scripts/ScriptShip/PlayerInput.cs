﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SmallShip))]


/**
 * 
 * Should have this script on all small ships so a player can take control of an AI ship
 * movement only for now
 * 
 * Allows player control of a ship * 
 * 
 * */

public class PlayerInput : MonoBehaviour {

	private SmallShip ship;

	[Tooltip("//set true to allow player input, disable any AI first.")]
	public bool activated = false;
	
	[Tooltip("control inputs (-1 to 1) are multiplied by this value, increases sensitivity")]
	public float multiplier = 30;


	[Tooltip("primary weapon")]
	public ShipWeapon gun;

	[Tooltip("secondary wepon")]
	public MissleLauncher missleLauncher;





	void Awake ()
	{	
		ship = transform.GetComponent<SmallShip>();
	}
		
	void Update ()
	{
		if( activated ){

			float pitch = Input.GetAxis("Vertical");
			float yaw = Input.GetAxis( "Horizontal" );
			float roll = Input.GetAxis( "Aileron" );			
			float throttle = Input.GetAxis("Throttle");

			//ship.RotateLocal( pitch * multiplier , x *multiplier , -1*roll * multiplier  );
			ship.Torque( new Vector3( pitch  , yaw  , -1*roll   )*multiplier  );
						
			ship.ThrottleAdd(throttle * 10);


			if (Input.GetButton("XKey"))
			{
				Fire();
			}
		}		
	}

	public void Fire()
	{
		if( gun !=null)
			gun.Fire();
	}


}
