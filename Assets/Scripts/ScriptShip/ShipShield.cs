﻿using UnityEngine;
using System.Collections;

public class ShipShield : MonoBehaviour , IDamageable {

	public ShipBase ship;

	public bool front = true;


	public void ApplyDamage( float damage ){
		if( front ){
			ship.ApplyDamageFront( damage );
		}
		else {
			ship.ApplyDamageRear( damage );
		}
	}

	//compatability with int
	public void ApplyDamage( int damage ){
		float bob = damage;
		ApplyDamage( bob );
	}

	public int GetTeam( ){
		return 0;
	}
}
