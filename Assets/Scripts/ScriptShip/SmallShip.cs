﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(ShipMovement))]
[RequireComponent(typeof(ShipDestructor))]


/*
 * Hp of the ship itself
 * 
 * 
 * 
 * */
public class SmallShip : MonoBehaviour, IDamageable {

	private Rigidbody my_rb ;
	private ShipMovement shipMovement;


	[Tooltip("fire effect for when ship is defeated and drifting")]
	public ParticleSystem fire;

	public bool destroyed = false ;

	[Tooltip("Hitpoints")]
	public float hp = 100.0f ;

	[Tooltip("For observing only")]
	public float throttle ;//throttle input must be scaled to 0-100f

	private bool flight_controls_online = true;
	private bool engine_online = true;

	//private float final_lazer_pow = 100 ;
	//	private float final_shield_pow = 100 ;

	//	private float laz_recharge = 1 ;

	
	private float maxhp;



	private bool damageable = true;
	private int hitsSince = 0;
	private float nexthit;



	void Awake()
	{
		shipMovement = gameObject.GetComponent<ShipMovement>();
		my_rb = transform.GetComponent<Rigidbody>();
		my_rb.useGravity = false;
	}

	// Use this for initialization
	void Start()
	{
		maxhp = hp;
	}


	void Update()
	{
		if (!destroyed)
		{
			if (flight_controls_online)
			{

			}

			if (engine_online)
			{
				if (throttle > 0.01)
				{
					// Apply thrust to ship
					float totalThrust = (throttle / 100) * shipMovement.GetMaxThrust();
					shipMovement.Thrust(new Vector3(0, 0, totalThrust));
				}
			}
		}
	}


	public float GetMaxHp(){
		return maxhp;
	}

	

	public void Torque(Vector3 torque)
	{
		shipMovement.Torque(torque);
	}
	

	//-------------
	//Throttle Controls
	public void ThrottleAdd( float add ){
		throttle += add;	
		throttle = Mathf.Clamp( throttle , 0 , 100 );
	}

	public void ThrottleMax(){
		throttle = 100;
	}

	public void ThrottleZero(){
		throttle = 0;
	}

	public void ThrottleSet( float throttleNew ){
		throttle = throttleNew;
	}

	public void Coast(bool onoff)
	{
		shipMovement.Coast(onoff);
	}
	//------------------


	public bool GetDestroyed(){
		return destroyed;
	}
	



	public void Defeat()
	{
		if (damageable)
		{
			damageable = false;
			destroyed = true;

			my_rb.drag = 0.01f;
			my_rb.angularDrag = 0.01f;

			if (fire){
				var em = fire.emission;
				em.enabled = true;
			}

			//add random torque to ship
			my_rb.AddTorque(Random.Range(-2200,2200 ), Random.Range(-2200, 2200), Random.Range(-1200, 2200), ForceMode.Impulse);

			//notify ships roster
			StartCoroutine(WaitToDestroy());
		}

	}

	/**
	 * Lets the ship drift in space before begin deleted
	 * */
	public IEnumerator WaitToDestroy()
	{
		if (gameObject.GetComponent<ShipDestructor>())
			gameObject.GetComponent<ShipDestructor>().DetachCamera();


		yield return new WaitForSeconds(3.0f);
		Detonate();
	}

	/* 
	 * Actually destroy the ship
	 * */
	public void Detonate(){
		if( gameObject.GetComponent<ShipDestructor>() )
			gameObject.GetComponent<ShipDestructor>().Detonate();
	}


	/**
	 * Call this when shooting and hitting the ship
	 * */
	public void ApplyDamage(float damage)	{
		
		HpModify(-damage);

		if (hp <= 0)
		{
			hp = 0;
			Defeat();
		}
	}

	/* 
	 * Add/Subtract hp after it has gotten through the shield, also tracks frequency of being hit
	 * */
	public void HpModify(float add)
	{
		hp += add;

		if (Time.time < nexthit)
		{
			hitsSince++;
		}
		else
		{
			hitsSince = 0;
		}

		nexthit = Time.time + 2;
	}


	public void ApplyDamage(int damage)
	{ //int compatable
		float bob = damage;
		ApplyDamage(bob); //call float version
	}


	public int GetTeam()
	{
		return 0;
	}


	public void ApplyDamageFront(float damage)
	{
		if (damageable)
		{
			HpModify(damage);

			if (hp <= 0)
			{
				hp = 0;
				Defeat();
			}
		}
	}

	public void ApplyDamageRear(float damage)
	{
		if (damageable)
		{
			HpModify(damage);
			
			if (hp <= 0)
			{
				hp = 0;
				Defeat();
			}
		}
	}





	/*
	 * 
		private Rigidbody my_rb ;
	private ShipMovement shipMovement;

	[Tooltip("fire effect for when ship is defeated and drifting")]
	public ParticleSystem fire;

	public bool destroyed = false ;

	[Tooltip("Hitpoints")]
	public float hp = 100.0f ;

	[Tooltip("For observing only")]
	public float throttle ;//throttle input must be scaled to 0-100f

	//systems (damage/low power on/off stuff)
	private bool cannons_online = true;
	private bool shields_online = true;
	private bool engine_online = true;
	private bool flight_controls_online = true;

	public float eng_pow = 100.0f ;
	public float engineCapBase = 2.0f;

	private float shield_rate ;
	private float shield_division = .5f ; //0.5 is 50/50 , 0.25 is 25% front 75% rear;
	private float shield_can_div = .5f ;
	private float lazer_rate ;


	public float recharge_time = 0.2f ;

	private float shld_f_pow ;
	private float shld_b_pow ;

	public float lazer_powCap = 100 ;
	public float shield_powCap = 100 ;
	private float eng_powCap = 100 ;

	public float shield_drain = 2;
	public float lazer_drain = 2;

	public float lazer_pow = 100 ;
	public float shield_pow = 100 ;


	private float final_lazer_pow = 100 ;
	//	private float final_shield_pow = 100 ;

	//	private float laz_recharge = 1 ;

	//player selected rates
	public int selected_shield_div = 0 ;
	public int selected_shield_laz_div = 0 ;
	public int selected_shield_rate = 0 ;
	public int selected_lazer_rate = 0 ;


	public List <float> shield_divs ;
	public List <float> shield_laz_divs ;
	public List <float> shieldrates;
	public List <float> lazer_rates;
	
	
	private float maxhp;


	void Awake()
	{
		shipMovement = gameObject.GetComponent<ShipMovement>();
		my_rb = transform.GetComponent<Rigidbody>();

		my_rb.useGravity = false;
	}

	// Use this for initialization
	void Start()
	{
		maxhp = hp;

		float[] rates = { 0, 0.25f, 0.5f, 0.75f, 1.0f };

		shield_divs.AddRange(rates);
		shield_laz_divs.AddRange(rates);
		shieldrates.AddRange(rates);
		lazer_rates.AddRange(rates);
				
		StartCoroutine(ShieldRecharge());
		StartCoroutine(CannonRecharge());

		//init shields
		shld_f_pow = shield_pow / 2;
		shld_b_pow = shield_pow / 2;
	}


	void Update()
	{
		if (!destroyed)
		{
			if (flight_controls_online)
			{

			}

			if (engine_online)
			{
				if (throttle > 0.01)
				{
					// Apply thrust to ship
					float totalThrust = (throttle / 100) * shipMovement.GetMaxThrust();
					shipMovement.Thrust(new Vector3(0, 0, totalThrust));
				}
			}
		}
	}


	public float GetShieldPowCap(){
		return  shield_powCap ;
	}

	public float GetMinShieldRate(){
		return shieldrates[ 0 ];
	}

	public float GetMaxShieldRate(){
		return shieldrates[ shieldrates.Count-1 ];
	}

	public float GetMaxLazerRate(){
		return   lazer_rates[ lazer_rates.Count-1 ];
	}

	public float GetMinLazerRate(){
		return   lazer_rates[ 0 ];
	}

	public float GetEnginePowCap(){
		return eng_powCap;
	}

	public float GetShieldDistributionValue(){
		return shield_divs[ selected_shield_div ] * 100 ;
	}

	public float GetSelectedLazerRate(){
		return lazer_rates[ selected_lazer_rate ];
	}

	public float GetSelectedShieldRate(){
		return shieldrates[ selected_shield_rate ];
	}

	public float GetFinalLazerPow(){
		return final_lazer_pow;
	}

	public float GetShldFpow(){
		return shld_f_pow;
	}

	public float GetShldBpow(){
		return shld_b_pow;
	}

	public float GetMaxHp(){
		return maxhp;
	}

	public void Coast(bool onoff)
	{
		shipMovement.Coast(onoff);
	}

	public void Torque(Vector3 torque)
	{
		shipMovement.Torque(torque);
	}




	public IEnumerator CannonRecharge(){
		while( true ){
			if( cannons_online ){
				CannonAdd( lazer_rate * (1- shield_can_div) ) ;
			}
			yield return new WaitForSeconds( 0.1f ) ;
		}
	}

	public IEnumerator ShieldRecharge(){
		while( true ){
			if( shields_online ){
				ShieldAdd( shield_rate * shield_can_div ) ;
			}
			yield return new WaitForSeconds( 0.1f ) ;
		}
	}


	//increase/decrease shield power
	public void ShieldAdd( float add  ){

		shield_pow += add;

		//distribute power to front, back
		float f = ( shield_pow * shield_division );
		float b = ( shield_pow * ( 1-shield_division ) );

		shld_f_pow += f ;
		shld_b_pow += b ;

		shield_pow -= (f+b);

		//limit power minimum, maximum
		if( shld_b_pow < 0 )
			shld_b_pow = 0 ;

		if( shld_f_pow < 0 )
			shld_f_pow = 0 ;

		if( (shld_f_pow > shield_powCap) )
		{
			shld_f_pow = shield_powCap ;
		}

		if( (shld_b_pow > shield_powCap) )
		{
			shld_b_pow = shield_powCap ;
		}
		//---------
	}

	public void CannonAdd( float add  ){
		lazer_pow += add - lazer_drain;

		if( lazer_pow > lazer_powCap )
		{
			lazer_pow = lazer_powCap ;
		}

		if( lazer_pow < 0 )
		{
			lazer_pow = 0 ;
		}

//		final_lazer_pow = lazer_pow ;
		//final_lazer_pow = lazer_pow * (1-shield_can_div);

		if( engine_online )
		{
			eng_pow = eng_powCap -  lazer_rates[ selected_lazer_rate ] - shieldrates[ selected_shield_rate ];
		}

	}

	public void LazToShield( float pow ){
		shield_pow +=  pow ;
		lazer_pow -= pow ;
	}

	public void ThrottleAdd( float add ){
		throttle += add;	
		throttle = Mathf.Clamp( throttle , 0 , 100 );
	}

	public void ThrottleMax(){
		throttle = 100;
	}

	public void ThrottleZero(){
		throttle = 0;
	}

	public void ThrottleSet( float throttleNew ){
		throttle = throttleNew;
	}

	public void CannonRateToggle(){
		selected_lazer_rate ++ ;
		if( selected_lazer_rate > lazer_rates.Count-1 ){
			selected_lazer_rate = 0 ;
		}
		lazer_rate = lazer_rates[ selected_lazer_rate ] * (1-shield_can_div) ;
	}

	public void ShieldCannonRateToggle(){
		selected_shield_laz_div ++ ;
		if( selected_shield_laz_div > shield_laz_divs.Count-1 ){
			selected_shield_laz_div = 0 ;
		}
		shield_can_div = shield_laz_divs[ selected_shield_laz_div ] ;
	}

	public void ShieldRateToggle(){
		selected_shield_rate ++ ;
		if( selected_shield_rate > shieldrates.Count-1 ){
			selected_shield_rate = 0 ;
		}
		shield_rate = shieldrates[ selected_shield_rate ] * shield_can_div ;
	}

	public void ShieldDivisionToggle(){
		selected_shield_div ++ ;
		if( selected_shield_div > shield_divs.Count-1 ){
			selected_shield_div = 0 ;
		}
		shield_division = shield_divs[ selected_shield_div ] ;
	}

	//changes shield recharge rate
	public void ShieldRate( float rate ){
		shield_rate = rate ;
	}

	public bool GetDestroyed(){
		return destroyed;
	}
	



	private bool damageable = true;
	private int hitsSince = 0;
	private float nexthit;
	

	public void Defeat()
	{
		if (damageable)
		{
			damageable = false;
			destroyed = true;

			my_rb.drag = 0.01f;
			my_rb.angularDrag = 0.01f;

			if (fire){
				var em = fire.emission;
				em.enabled = true;
			}

			//add random torque to ship
			my_rb.AddTorque(Random.Range(-2200, 2200), Random.Range(-2200, 2200), Random.Range(-1200, 2200), ForceMode.Impulse);

			//notify ships roster
			StartCoroutine(WaitToDestroy());
		}

	}
	
	 * Lets the ship drift in space before begin deleted
	public IEnumerator WaitToDestroy()
	{
		if (gameObject.GetComponent<ShipDestructor>())
			gameObject.GetComponent<ShipDestructor>().DetachCamera();


		yield return new WaitForSeconds(3.0f);
		Detonate();
	}
	
	 * Actually destroy the ship
	public void Detonate()
	{
		if (gameObject.GetComponent<ShipDestructor>())
			gameObject.GetComponent<ShipDestructor>().Detonate();
	}



	
	 * Call this when shooting and hitting the ship
	public void ApplyDamage(float damage)
	{
		float remain = shield_pow - damage;
		shield_pow -= damage;

		if (shield_pow < 0)
		{
			shield_pow = 0;
		}

		if (remain < 0)
		{
			HpModify(remain);
		}

		if (hp < 0)
		{
			Defeat();
		}
	}
	
	 // Add/Subtract hp after it has gotten through the shield, also tracks frequency of being hit
	 
	public void HpModify(float add)
	{
		hp += add;

		if (Time.time < nexthit)
		{
			hitsSince++;
		}
		else
		{
			hitsSince = 0;
		}

		nexthit = Time.time + 2;
	}


	public void ApplyDamage(int damage)
	{ //int compatable
		float bob = damage;
		ApplyDamage(bob); //call float version
	}


	public int GetTeam()
	{
		return 0;
	}


	public void ApplyDamageFront(float damage)
	{
		if (damageable)
		{
			float remain = shld_f_pow - damage;
			shld_f_pow -= damage;

			if (shld_f_pow < 0)
			{
				shld_f_pow = 0;
			}

			if (remain < 0)
			{
				HpModify(remain);
			}

			if (hp < 0)
			{
				Defeat();
			}
		}
	}

	public void ApplyDamageRear(float damage)
	{
		if (damageable)
		{
			float remain = shld_b_pow - damage;
			shld_b_pow -= damage;

			if (shld_b_pow < 0)
			{
				shld_b_pow = 0;
			}
			if (remain < 0)
			{
				HpModify(remain);
			}

			if (hp < 0)
			{
				Defeat();
			}
		}
	}

	*/




}









//for reference

	
