﻿using UnityEngine;
using System.Collections;



[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(SmallShip))]
[RequireComponent(typeof(ShipUtils))]
[RequireComponent(typeof(ShipMovement))]
[RequireComponent(typeof(PlayerInput))]
[RequireComponent(typeof(PlayerGuiUpdater))]


/*
 * 
 * To begin making a ship just add this script to a gameobject's root
 * 
 * Mediates between scripts on the ship
 * 
 * use for getting info about a ship including cluster it belongs to,
 * communication between ships
 * 
 * 
 * high drag and angular drag reccommended on rigidbody
 * 
 * */

public class SmallShipFacade : MonoBehaviour {

	[Tooltip("Team this ship is on")]
	public int team = 0;

	[Tooltip("This ship is controlled by the player?")]
	public bool playerControlled = false;

	//assign this in inspector
	[Tooltip("The point the main camera will be attached to when piloting")]
	public Transform cameraMount;


	//auto assigned--------
	private PlayerGuiUpdater guiUpdater;
	private PlayerInput playerinput;
	private SmallShip ship;
	private Transform tr;
	private Rigidbody rb;
	private ShipMovement shipMovement;
	private ShipUtils shipUtils;
	//----------------



	//optional for AI control of ship
	public SpaceFighterAI spaceFighterAi;
	public BomberAI bomberAi;


	private ShipCluster clust;
	public bool hasCluster = false;



	void Awake(){
		tr = transform;
		rb = transform.GetComponent<Rigidbody> ();
		shipMovement = gameObject.GetComponent<ShipMovement>();
		playerinput = gameObject.GetComponent <PlayerInput>();
		ship = gameObject.GetComponent<SmallShip>();
		guiUpdater = gameObject.GetComponent<PlayerGuiUpdater>();
		shipUtils = gameObject.GetComponent<ShipUtils>();
	}



	void Start()
	{
		if (playerControlled)
		{
			SetAIorPlayer(2);
		}
		else
		{
			SetAIorPlayer(1);
		}
	}


	//----------------------------------------------
	//Movement
	public void ThrottleAdd(float add)
	{
		ship.ThrottleAdd(add);
	}

	public void ThrottleMax()
	{
		ship.ThrottleMax();
	}

	public void ThrottleZero()
	{
		ship.ThrottleZero();
	}

	public void ThrottleSet( float throttleNew)
	{
		ship.ThrottleSet(throttleNew);
	}

	public void Coast(bool onoff)
	{
		ship.Coast(onoff);
	}

	public void Torque(Vector3 torque)
	{
		shipMovement.Torque(torque);
	}
	//---------------------------------------------


	
	

	public bool CheckFov(Vector3 targetPos, float angle, Vector3 myPos)
	{
		return shipUtils.CheckFov(targetPos, angle, myPos);
	}

	public Vector3 CalculateLead(Transform tar, float projectileSpeed)
	{
		return shipUtils.CalculateLead(tar, projectileSpeed);
	}





	//has the ship been destroyed?
	public bool GetDestroyedStatus()
	{
		return ship.GetDestroyed();
	}


	public void SetClust(ShipCluster clust)	{
		this.clust = clust;
		clust.AddElement(this);
		hasCluster = true;
	}

	public ShipCluster GetCluster()	{
		return clust;
	}
	

	public Transform GetTransform(){
		return tr;
	}

	public Rigidbody GetRigidbody(){
		return rb;
	}

	public Vector3 GetVelocity(){
		return rb.velocity;
	}

	public Vector3 GetPosition(){
		return transform.position;
	}

	public void SetSquadron( Squadron squad ){
		if (spaceFighterAi) {
			spaceFighterAi.SetSquadron (squad);
		}

		if (bomberAi) {
			bomberAi.SetSquadron (squad);
		}
	}


	public Squadron GetSquadron()
	{
		Squadron sq = null;

		if (spaceFighterAi)
		{
			sq=spaceFighterAi.GetSquadron();
		}
		return sq;
	}

	//get target of this craft
	public Transform GetTarget(){
	
		Transform ret = null;

		if (spaceFighterAi) {
			ret = spaceFighterAi.target;
		}
		return ret;
	}


	public bool initUI = true;

	//WIP
	public void SetAIorPlayer( int option ){
		//options are:
		//1 AI
		//2 player

		if( option == 1){
			//turn off player things, turn on AI things

			if (spaceFighterAi) {
				spaceFighterAi.activated = true;
			}

			guiUpdater.activated = false;
			playerControlled = false;

			if( playerinput ){
				playerinput.activated = false;
			}

		}

		if( option == 2 ){
			//turn off AI things, turn on player things

			if (spaceFighterAi) {
				spaceFighterAi.activated = false;
			}

			if( playerinput ){
				playerinput.activated = true;
				playerControlled = true;
			}

			if(initUI)
			{
				guiUpdater.InitSliders();
				guiUpdater.activated = true; 
			}
			

		}

	}

	/*
	//Sets team variables for this and other scripts
	Needed for spawning
	*/
	public void SetTeam( int newTeam ){
		team = newTeam;

		if( spaceFighterAi ){

			if( newTeam == 1){
				spaceFighterAi.ChangeTeam( "EnemyTeam1" , "EnemyTeam2" );
			}

			if( newTeam == 2){
				spaceFighterAi.ChangeTeam( "EnemyTeam2" , "EnemyTeam1" );
			}

		}
			
	}


	//-----------------
	//WIP
	public string teamTag = "EnemyTeam2";
	public string teamTagEnemy = "EnemyTeam1";

	public string GetTeamTag()
	{
		return teamTag;
	}

	public string GetTeamTagEnemy()
	{
		return teamTagEnemy;
	}

	public void SetTeamTag(string newtag)
	{
		tr.tag = newtag;
	}
	
	public void ChangeTeam(string teamTag, string enemyTag)
	{
		SetTeamTag(teamTag);
		teamTagEnemy = enemyTag;
	}








	//True if status is destroyed
	public bool CheckDestroyed(){
		return ship.GetDestroyed();
	}


	public void Delete(){
		
	}

	public float GetHp(){
		float hp = ship.hp;

		return hp;
	}

	public int GetTeam(){
		return team;
	}

	public void Fire(){
		
	}

	//WIP
	//Tells roster that this ship is dead, remove from list
	public void notifyDead(){
		ShipRosterTeam[] rosters = GameObject.FindObjectsOfType<ShipRosterTeam>();

		foreach( ShipRosterTeam r in rosters ){
			r.NotifyDead( team );
		}
	}


}
