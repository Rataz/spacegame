﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]


/*
 * Applies forces to the ship rigidbody
 * 
 * Other scripts should use this to move the ship
 * */
 

public class ShipMovement : MonoBehaviour {

	private Rigidbody my_rb;

	private Vector3 moveDirection;
	//private Vector3 torqueDirection;


	private bool thrustersOn = false;

	public float speedlimit = 100;

	private float normdrag = .1f ;
	public float CoastDrag = .1f ;
	public float CoastRotDrag = .1f ;

	public bool isCoasting = false;

	public float maxThrust = 10;

	public float maxTorque = 1;

	public float GetMaxThrust(){
		return maxThrust;
	}

	void Start () {
		my_rb = transform.GetComponent<Rigidbody>();
		normdrag = my_rb.drag;
	}

	public float GetSpeed(){
		Vector3 velo = my_rb.velocity;
		float percent = Vector3.Dot( transform.forward , velo.normalized );
		float currentSpeed = velo.magnitude * percent;
		return currentSpeed;
	}

	public void CoastToggle(  ){
		isCoasting = !isCoasting;
		Coast( isCoasting );
	}



	public void Coast( bool onoff ){
		isCoasting = onoff;

		if( isCoasting ){
			my_rb.drag = CoastDrag;
			//my_rb.angularDrag = CoastRotDrag;
		}
		else{
			my_rb.drag = normdrag;
			//my_rb.angularDrag = normRotdrag;
		}
	}

	/**
	 * Only SmallShip should use this
	 * */
	public void Thrust( Vector3 dir ){
		moveDirection = dir;
		thrustersOn = true;
	}


	/**
	 * Turn the ship
	 * 
	 * Vector should not have value more than 1,1,1
	 * 
	 * */
	public void Torque( Vector3 torque ){
		
		//torqueDirection = torque;

		if (torque.x > 1)
			torque.x = 1;

		if (torque.x < -1)
			torque.x = -1;


		if (torque.y > 1)
			torque.y = 1;

		if (torque.y < -1)
			torque.y = -1;


		if (torque.z > 1)
			torque.z = 1;

		if (torque.z < -1)
			torque.z = -1;

		my_rb.AddRelativeTorque( torque*maxTorque , ForceMode.Acceleration );
		

		//transform.Rotate( torque );
	}


	public void RotateLocal( float x, float y, float z ){
		transform.Rotate( x * Time.deltaTime , y * Time.deltaTime , z * Time.deltaTime );
	}


	void FixedUpdate(){

		if( thrustersOn ){
			if( GetSpeed()<speedlimit ){
				my_rb.AddRelativeForce( moveDirection );
			}
			thrustersOn = false;
		}

	}

}
