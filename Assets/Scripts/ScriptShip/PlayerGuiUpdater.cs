﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(SmallShip))]

/**
 * 
 * Do not use
 * 
 * Keeps UI updated with latest stats from the ship
 * 
 * Should go on all small ships the player might take control of
 * 
 * */

public class PlayerGuiUpdater : MonoBehaviour {

	public bool activated = false;


	[Tooltip("Use this to disable gui updating if causing errors")]
	public bool doupdate = true;


	//auto assigned-------
	private PlayerShipGuiController gui;
	//private SmallShip ship;
	//---------------------------





	private void Awake()
	{
		//ship = gameObject.GetComponent<SmallShip>();
	}

	void Start () {
		
		gui = GameObject.FindObjectOfType<PlayerShipGuiController>();

		if( activated && doupdate)
		{
			InitSliders();
		}
	}

	void Update () {

		if( activated && doupdate)
		{

			if( gui !=null ){
				/*
				gui.SetEngineSliderValue( ship.eng_pow );
				gui.SetShieldBSliderValue( ship.GetShldBpow() );
				gui.SetShieldFSliderValue( ship.GetShldFpow() );
				gui.SetLazerSliderValue( ship.GetFinalLazerPow() );
				gui.SetThrottleSliderValue( ship.throttle );
				gui.SetShieldRCSliderValue( ship.GetSelectedShieldRate() );
				gui.SetLazerRCSliderValue( ship.GetSelectedLazerRate() );
				gui.SetEngineRCSliderValue( ship.eng_pow );
				gui.SetShieldDistribSwitchSliderValue( ship.GetShieldDistributionValue() );
				gui.SetHpSliderValue( ship.hp );
				*/
			}


		}
	}


	//init UI
	public void InitSliders(){
		if (doupdate)
		{
			/*
			gui.SetEngineSliderBounds(ship.GetEnginePowCap(), 0);
			gui.SetLazerRCSliderBounds(ship.GetMaxLazerRate(), ship.GetMinLazerRate());
			gui.SetShieldRCSliderBounds(ship.GetMaxShieldRate(), ship.GetMinShieldRate());
			gui.SetLazerSliderBounds(ship.lazer_powCap, 0);
			gui.SetShieldFSliderBounds(ship.GetShieldPowCap(), 0);
			gui.SetShieldBSliderBounds(ship.GetShieldPowCap(), 0);
			gui.SetHpSliderBounds(ship.GetMaxHp(), 0);
			*/
		}
		

	}



}
