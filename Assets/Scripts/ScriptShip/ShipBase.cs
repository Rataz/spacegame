﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


[RequireComponent(typeof(Rigidbody))]


/**
 * 
 * -- For reference only
 * 
 * Handles ship taking damage, shields, ui for shields
 * 
 * */

public class ShipBase : MonoBehaviour , IDamageable {

	private Rigidbody my_rb ;

	public bool destroyed = false ;
	public float hp = 100.0f ;
	
	public float throttle ;
	public float maxThrust = 200 ;//newtons
	public float maxSpeed = 100 ;//newtons

	public float pitchmod = 1.0f ;
	public float yawmod = 1.0f ;
	public float rollmod = 1.0f ;

	//systems (damage/low power on/off stuff)
	private bool cannons_online = true;
	private bool shields_online = true;
	private bool engine_online = true;
	private bool flight_controls_online = true;

	public float eng_pow = 100.0f ;
	public float engineCapBase = 2.0f;

	private float shield_rate ;
	private float shield_division = .5f ; //0.5 is 50/50 , 0.25 is 25% front 75% rear;
	private float shield_can_div = .5f ;
	private float lazer_rate ;

	public float recharge_time = 0.2f ;

	private float shld_f_pow ;
	private float shld_b_pow ;

	public float lazer_powCap = 100 ;
	public float shield_powCap = 100 ;
	private float eng_powCap = 100 ;

	public float shield_drain = 2;
	public float lazer_drain = 2;

	public float lazer_pow = 100 ;
	public float shield_pow = 100 ;

	private float final_lazer_pow = 100 ;
//	private float final_shield_pow = 100 ;

//	private float laz_recharge = 1 ;

	public float turnMultip = 2f ;

//	private float normRotdrag = .1f ;
	private float normdrag = .1f ;
	public float CoastDrag = .1f ;
	public float CoastRotDrag = .1f ;

	public bool isCoasting = false;

	//player selected rates
	public int selected_shield_div = 0 ;
	public int selected_shield_laz_div = 0 ;
	public int selected_shield_rate = 0 ;
	public int selected_lazer_rate = 0 ;


	public List <float> shield_divs ; //= { 0 , 0.25f , 0.5f , 0.75f , 1.0f };
	public List <float> shield_laz_divs ;
	public List <float> shieldrates;
	public List <float> lazer_rates;


	//GUI variables---------------------
	public Slider shield_f_Slider ;
	public Slider shield_b_Slider ;
	public Slider shield_distribSwitch_Slider ;

	public Slider engineSlider ;
	public Slider lazerSlider ;
	public Slider throttleSlider ;
	
	public Slider shieldRCslider ;
	public Slider lazerRCslider ;
	public Slider engineRCslider ;

	public Slider hpslider ;
	//----------------------------------


	public Transform camPos;
	public CameraAttacher cam;

	private bool damageable = true;
	
	void Start(){

		my_rb = transform.GetComponent<Rigidbody>() ;

		normdrag = my_rb.drag;
//		normRotdrag = my_rb.angularDrag ;

		eng_powCap = engineCapBase + lazer_rates[ lazer_rates.Count-1 ] + shieldrates[ shieldrates.Count-1 ];

		//----------
		//init UI
		if( engineSlider ){
			engineSlider.minValue = 0;
			engineSlider.maxValue = eng_powCap;
		}

		if( lazerRCslider ){
			lazerRCslider.minValue = lazer_rates[ 0 ];
			lazerRCslider.maxValue = lazer_rates[ lazer_rates.Count-1 ];
		}

		if( shieldRCslider ){
			shieldRCslider.minValue = shieldrates[ 0 ];
			shieldRCslider.maxValue = shieldrates[ shieldrates.Count-1 ];
		}

		if( lazerSlider ){
			lazerSlider.maxValue = lazer_powCap;
		}

		if( shield_f_Slider ){
			shield_f_Slider.minValue = 0;
			shield_f_Slider.maxValue = shield_powCap;
		}

		if( shield_b_Slider ){
			shield_b_Slider.minValue = 0;
			shield_b_Slider.maxValue = shield_powCap;
		}

		if( hpslider ){
			hpslider.minValue = 0;
			hpslider.maxValue = hp; //assuming spawn at full hp
		}
		//Init UI
		//-------------------

		StartCoroutine( ShieldRecharge() );
		StartCoroutine( CannonRecharge() );
	}

	void FixedUpdate () {

		if( !destroyed ){
			if( flight_controls_online ){
				if( rotVect != Vector3.zero ){
					my_rb.AddRelativeTorque( rotVect * turnMultip ) ;
					rotVect = Vector3.zero ;
				}
			}

			if( engine_online ){
				if( throttle > 0.01 && !isCoasting ){
					if( my_rb.velocity.magnitude < maxSpeed ){
						float fraction = eng_pow / eng_powCap ;
						my_rb.AddRelativeForce( new Vector3( 0 , 0 , throttle * eng_pow * fraction * maxThrust ) ) ;
					}
				}
			}
		}
	}
	
	void Update(){
		RefreshGUI();
	}

	public ParticleSystem fire;
	public void Defeat(){

		transform.tag = "Untagged";

		engine_online = false;
		cannons_online = false;
		shields_online = false;
		flight_controls_online = false;
		my_rb.drag = 0.01f;
		my_rb.angularDrag = 0.01f;

		if (fire){
			var em = fire.emission;
			em.enabled = true;
		}
			
			//fire.enableEmission = true;

		cam.DeAttach();
		ShipRespawnPlayer p = GameObject.FindObjectOfType< ShipRespawnPlayer >();
		p.Spawn(  );

	}

	public void ApplyDamageFront(  float damage ){

		if( damageable ){
			float remain =  shld_f_pow - damage;
			shld_f_pow-=damage;
			
			if( shld_f_pow < 0 ){
				shld_f_pow =0;
			}
			if( remain < 0 ){
				hp += remain;
			}

			if( hp<0 ){
				damageable = false;
				destroyed = true ;
				Defeat();
			}
		}
	}


	public void ApplyDamageRear(  float damage ){

		if( damageable ){
			float remain =  shld_b_pow - damage;
			shld_b_pow-=damage;
			
			if( shld_b_pow < 0 ){
				shld_b_pow =0;
			}
			if( remain < 0 ){
				hp += remain;
			}

			if( hp<0 ){
				damageable = false;
				Defeat();
			}
		}
	}

	public void ApplyDamage( int damage ){
		float remain =  shield_pow - damage;
		shield_pow-=damage;
		
		if( shield_pow < 0 ){
			shield_pow =0;
		}
		if( remain < 0 ){
			hp += remain;
		}
	}

	public void ApplyDamage( float damage ){
		float remain =  shield_pow - damage;
		shield_pow-=damage;

		if( shield_pow < 0 ){
			shield_pow =0;
		}
		if( remain < 0 ){
			hp += remain;
		}
	}

	public int GetTeam( ){
		return 0;
	}

	public void CoastToggle(  ){
		isCoasting = !isCoasting;
		Coast( isCoasting );
	}
	
	public void Coast( bool onoff ){
		isCoasting = onoff;

		if( isCoasting ){
			my_rb.drag = CoastDrag;
			//my_rb.angularDrag = CoastRotDrag;
		}
		else{
			my_rb.drag = normdrag;
			//my_rb.angularDrag = normRotdrag;
		}
	}

	public IEnumerator CannonRecharge(){
	
		while( true ){
			if( cannons_online ){
				CannonAdd( lazer_rate * (1- shield_can_div) ) ;
			}
			yield return new WaitForSeconds( 0.1f ) ;
		
		}
//		yield return 0 ;
	}

	public IEnumerator ShieldRecharge(){
		while( true ){
			if( shields_online ){
				ShieldAdd( shield_rate * shield_can_div ) ;
			}
				yield return new WaitForSeconds( 0.1f ) ;
		}
//		yield return 0 ;
	}

	public void CannonRateToggle(){
		selected_lazer_rate ++ ;
		if( selected_lazer_rate > lazer_rates.Count-1 ){
			selected_lazer_rate = 0 ;
		}
		lazer_rate = lazer_rates[ selected_lazer_rate ] * (1-shield_can_div) ;
	}

	public void ShieldCannonRateToggle(){
		selected_shield_laz_div ++ ;
		if( selected_shield_laz_div > shield_laz_divs.Count-1 ){
			selected_shield_laz_div = 0 ;
		}
		shield_can_div = shield_laz_divs[ selected_shield_laz_div ] ;
	}

	public void ShieldRateToggle(){
		selected_shield_rate ++ ;
		if( selected_shield_rate > shieldrates.Count-1 ){
			selected_shield_rate = 0 ;
		}
		shield_rate = shieldrates[ selected_shield_rate ] * shield_can_div ;
	}

	public void ShieldDivisionToggle(){
		selected_shield_div ++ ;
		if( selected_shield_div > shield_divs.Count-1 ){
			selected_shield_div = 0 ;
		}
		shield_division = shield_divs[ selected_shield_div ] ;
	}

	//changes shield recharge rate
	public void ShieldRate( float rate ){
		shield_rate = rate ;
	}

	//increase/decrease shield power
	public void ShieldAdd( float add  ){

		shield_pow += add;

		//distribute power to front, back
		float f = ( shield_pow * shield_division );
		float b = ( shield_pow * ( 1-shield_division ) );

		shld_f_pow += f ;
		shld_b_pow += b ;

		shield_pow -= (f+b);

		//limit power minimum, maximum
		if( shld_b_pow < 0 )
			shld_b_pow = 0 ;

		if( shld_f_pow < 0 )
			shld_f_pow = 0 ;

		if( (shld_f_pow > shield_powCap) ){
			shld_f_pow = shield_powCap ;
		}
		if( (shld_b_pow > shield_powCap) ){
			shld_b_pow = shield_powCap ;
		}
		//---------
	}

	public void CannonAdd( float add  ){
		lazer_pow += add - lazer_drain;

		if( lazer_pow > lazer_powCap ){
			lazer_pow = lazer_powCap ;
		}
		if( lazer_pow < 0 ){
			lazer_pow = 0 ;
		}

		final_lazer_pow = lazer_pow ;
		//final_lazer_pow = lazer_pow * (1-shield_can_div);

		if( engine_online )
			eng_pow = eng_powCap -  lazer_rates[ selected_lazer_rate ] - shieldrates[ selected_shield_rate ];
	}
	
	public void LazToShield( float pow ){
		shield_pow +=  pow ;
		lazer_pow -= pow ;
	}

	public void ThrottleAdd( float add ){
		throttle += add;	
		throttle = Mathf.Clamp( throttle , 0 , 100 );
	}

	private Vector3 rotVect;
	public void RotatePhys( Vector3 inp ){
		rotVect = inp;
	}
			
	public void RefreshGUI(){

		if( !destroyed ){

			if( engineSlider )
				engineSlider.value = eng_pow;
			
			if( shield_f_Slider )
				shield_f_Slider.value = shld_f_pow;
			
			if( shield_b_Slider )
				shield_b_Slider.value = shld_b_pow;
			
			if( lazerSlider )
				lazerSlider.value = final_lazer_pow ;
			
			if( throttleSlider )
				throttleSlider.value = throttle;
			
			if( shieldRCslider )
				shieldRCslider.value = shieldrates[ selected_shield_rate ];//show desired rate
				//shieldRCslider.value = shield_rate;//shows effective rate
			
			if( lazerRCslider )
				lazerRCslider.value = lazer_rates[ selected_lazer_rate ] ;//show desired rate
			//lazerRCslider.value = lazer_rate;//shows effective rate
			
			if( engineRCslider )
				engineRCslider.value = eng_pow ;

			if( shield_distribSwitch_Slider )
				shield_distribSwitch_Slider.value = shield_divs[ selected_shield_div ] * 100 ; //percent * 100

			if( hpslider )
				hpslider.value = hp;

		}
	}

	public void SetLazerSlider( Slider slide ){
		lazerSlider = slide ;
	}
	
	public void SetLazerRCSlider( Slider slide ){
		lazerRCslider = slide ;
	}
	
	public void SetEngineRCSlider( Slider slide ){
		engineRCslider = slide ;
	}
	
	public void SetEngineSlider( Slider slide ){
		engineSlider = slide ;
	}
	
	public void SetShieldRCSlider( Slider slide ){
		shieldRCslider = slide ;
	}
	
	public void SetShieldBSlider( Slider slide ){
		shield_b_Slider = slide ;
	}
	
	public void SetShieldFSlider( Slider slide ){
		shield_f_Slider = slide ;
	}
	
	public void SetThrottleSlider( Slider slide ){
		throttleSlider = slide ;
	}

	public void SetShieldDistribSwitchSlider( Slider slide ){
		shield_distribSwitch_Slider = slide ;
	}

	public void SetHpSlider( Slider slide ){
		hpslider = slide ;
	}




	/*
	public void ShieldAdd( float add  ){

		shield_pow += add - shield_drain;

		if( (shield_pow > shield_powCap) ){
			shield_pow = shield_powCap ;
		}
		if( shield_pow < 0 ){
			shield_pow = 0 ;
		}

		final_shield_pow = shield_pow ;
		//final_shield_pow = shield_pow * shield_can_div ;

		shld_f_pow += ( add * shield_division )     - shield_drain;
		shld_b_pow += ( add*( 1-shield_division ) ) - shield_drain;

		if( (shld_f_pow > shield_powCap) ){
			shld_f_pow = shield_powCap ;
		}

		if( (shld_b_pow > shield_powCap) ){
			shld_b_pow = shield_powCap ;
		}

		if( shld_b_pow < 0 )
			shld_b_pow = 0 ;

		if( shld_f_pow < 0 )
			shld_f_pow = 0 ;

		//shld_f_pow  = final_shield_pow * shield_division ;
		//shld_b_pow  = final_shield_pow * ( 1-shield_division ) ;

		if( engine_online )
		eng_pow = eng_powCap -  lazer_rates[ selected_lazer_rate ] - shieldrates[ selected_shield_rate ];
	}*/


	/*
	public void EnginePowAdd( float pow ){
		if( pow < unusedPower ){
			unusedPower -= pow ;
			eng_pow += pow ;
		}
		eng_pow = unusedPower;
	}
	*/

}
