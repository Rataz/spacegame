﻿using UnityEngine;
using System.Collections;

public class CapitalShipMovement : MonoBehaviour {

	private Rigidbody my_rb ;

	public float hp = 100.0f ;
	public bool destroyed = false ;

	public bool isCoasting = false;

	public float throttle ;
	public float maxThrust = 200 ;//newtons
	public float maxSpeed = 100 ;//newtons

	public float pitchmod = 1.0f ;
	public float yawmod = 1.0f ;
	public float rollmod = 1.0f ;
	public float turnMultip = 2f ;
	//	private float normRotdrag = .1f ;

	//private float normdrag = .1f ;
	public float CoastDrag = .1f ;
	public float CoastRotDrag = .1f ;


	//systems (damage/low power on/off stuff)
	//private bool cannons_online = true;
//	private bool shields_online = true;
	private bool engine_online = true;
	private bool flight_controls_online = true;

	public float eng_pow = 100.0f ;
	public float engineCapBase = 2.0f;
	private float eng_powCap = 100 ;

	private PlayerControls controls;


	private string yaw ;
	private string pitch ;
	private string roll ;
	private string throt ;


	// Use this for initialization
	void Start () {

		my_rb = transform.GetComponent<Rigidbody>() ;

		my_rb.centerOfMass = Vector3.zero;

		if( !controls ){
			controls = gameObject.GetComponent<PlayerControls>();
		}


		pitch = controls.GetMoveFwdAxis();

		yaw = controls.GetMoveSideAxis();

		roll = controls.GetCapitalShipRoll();;

		throt = controls.GetCapitalShipThrottle();
	}
	
	// Update is called once per frame
	void Update () {

		float x  = Input.GetAxis( pitch );
		float y  = Input.GetAxis( yaw );
		float z = Input.GetAxis( roll )*-1;


		RotatePhys( new Vector3(  x , y , z ) );

		ThrottleAdd( Input.GetAxis( throt )*4 );


	}


	public void ThrottleAdd( float add ){
		throttle += add;	
		throttle = Mathf.Clamp( throttle , 0 , 100 );
	}

	private Vector3 rotVect;
	public void RotatePhys( Vector3 inp ){
		rotVect = inp;
	}

	void FixedUpdate () {

		if( !destroyed ){
			
			if( flight_controls_online ){
				if( rotVect != Vector3.zero ){
					my_rb.AddRelativeTorque( rotVect * turnMultip ) ;
					rotVect = Vector3.zero ;
				}
			}

			if( engine_online ){
				if( throttle > 0.01 && !isCoasting ){
					if( my_rb.velocity.magnitude < maxSpeed ){
						float fraction = eng_pow / eng_powCap ;
						my_rb.AddRelativeForce( new Vector3( 0 , 0 , throttle * eng_pow * fraction * maxThrust ) ) ;
					}
				}
			}
		}

	}

}
