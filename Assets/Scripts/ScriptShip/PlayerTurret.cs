﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTurret : MonoBehaviour {

	public float sensitivityX = 15F;
	public float sensitivityY = 15F;

	public float minimumX = -360F;
	public float maximumX = 360F;
	public float minimumY = -60F;
	public float maximumY = 60F;

	private float rotationX = 0;
	private float rotationY = 0;
	private Quaternion originalRotation;

	[Space(20)]

	[Tooltip("Render texture cam that will create te display in cocpit")]
	public Transform targetCam;

	[Tooltip("Camera of turret on ship")]
	public Camera gunnerCam;

	private Transform target;

	public string teamTagEnemy = "EnemyTeam1";

	[Tooltip("Distance camera should stay away from the target, ex 10 or less for small targets")]
	public float targetCamDistance = 10;

	public Texture crosshairTex;

	public float scale = 0.1f;

	public bool limitX = true;

	// Use this for initialization
	void Start () {
		originalRotation = transform.localRotation;
	}
	


	// Update is called once per frame
	void Update () {
		float x = Input.GetAxis("Mouse X");
		float y = Input.GetAxis("Mouse Y");

		rotationX += x * sensitivityX;
		rotationY += y * sensitivityY;

		if( limitX )
			rotationX = ClampAngle(rotationX, minimumX, maximumX);


		rotationY = ClampAngle(rotationY, minimumY, maximumY);
		Quaternion xQuaternion = Quaternion.AngleAxis(rotationX, Vector3.up);
		Quaternion yQuaternion = Quaternion.AngleAxis(rotationY, -Vector3.right);
		transform.localRotation = originalRotation * xQuaternion * yQuaternion;


		CameraTargetting();
	}


	void OnGUI()
	{
		if( target != null)
		{
			DrawTargetLead(transform, target, 2);
		}
	}


	private void DrawTargetLead(Transform playership, Transform playerTarget, int screensector)
	{
		if (crosshairTex && (playerTarget != null))
		{
			Transform tar = playerTarget;
			Vector3 fwd = playership.forward;
			Vector3 ppos = playership.position;

			float dot = Vector3.Dot(tar.position - ppos, fwd);
			
			if (dot > 0)
			{ //target is in front

				Vector3 leadVector = TargettingUtils.CalculateLead(tar, playership, 400);
				Rect rectangle = UiUtils.GetRectangleOverWorldPosition(gunnerCam, leadVector, scale, crosshairTex);

				bool xrange = false;

				if (screensector == 1)
				{
					//pilot >
					xrange = rectangle.x > Screen.width * 0.5;
				}
				else if (screensector == 2)
				{
					//gunner <
					xrange = rectangle.x < Screen.width * 0.5;
				}

				if (xrange)
					GUI.DrawTexture(rectangle, crosshairTex);

			}

		}
	}

	public UiTextChanger textChanger;

	public void CameraTargetting()
	{
		//set a new target
		if (Input.GetButton("Fire2"))
		{
			GameObject[] set = GameObject.FindGameObjectsWithTag(teamTagEnemy);

			List<Transform> ships = new List<Transform>();
			foreach (GameObject g in set)
				ships.Add(g.transform);

			target = TargettingUtils.ClosestToForwards(ships, transform);
		}

		//set camera on target
		if (target != null)
		{
			Quaternion rotation = Quaternion.Euler(  transform.position - target.position );
			targetCam.position = rotation *  (new Vector3(0.0f, 0.0f, -targetCamDistance)) + target.position;
			targetCam.rotation = rotation;


			if( target.GetComponent<SmallShipFacade>())
			{

				SmallShipFacade ssf = target.GetComponent<SmallShipFacade>();
				float hp = ssf.GetHp();
				if (hp < 0) hp = 0;
				textChanger.SetText(hp.ToString());
			}
		}
	}


	public static float ClampAngle(float angle, float min, float max)
	{
		if (angle < -360F)
			angle += 360F;
		if (angle > 360F)
			angle -= 360F;
		return Mathf.Clamp(angle, min, max);
	}
}
