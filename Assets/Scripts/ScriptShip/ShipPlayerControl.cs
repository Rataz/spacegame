﻿using UnityEngine;
using System.Collections;


/**
 * Replaces AI input to a ship with player input
 * 
 * 
 * 
 * */
public class ShipPlayerControl : MonoBehaviour {

	public ShipBase ship;
	public PlayerControls controls;
	private ShipsList list;

	private string yaw ;
	private string pitch ;
	private string roll ;
	private string throt ;

	// keys, old
	/*
	private string shield_to_laz = "n";
	private string laz_to_shield = "m";
	private string adjust_laz_rc = "r";
	private string adjust_shld_rc = "y";
	private string adjust_shld_pos = "b";
	private string coast = "c" ;
	*/

	//Inputs
	private string targetInRiticuleBtn = "Fire2";
	private string toggleLazerPowerBtn = "Fire2";
	private string toggleSheildPowerBtn = "Fire2";
	private string toggleSheildPostionBtn = "Fire2";
	private string toggleCoastBtn = "Fire2";
	private string shield_to_lazer_Btn = "Fire2";
	private string lazer_to_shield_Btn = "Fire2";


	void Start () {

		if( !controls ){
			controls = gameObject.GetComponent<PlayerControls>();
		}


		list = GameObject.FindObjectOfType<ShipsList>();

		pitch = controls.GetLookUpAxis();
		yaw = controls.GetMoveSideAxis();
		roll = controls.GetLookSideAxis();
		throt = controls.GetMoveFwdAxis();

		targetInRiticuleBtn = controls.GetlockTargetInFront();
		toggleLazerPowerBtn = controls.GetLazerPowerToggle();
		toggleSheildPowerBtn = controls.GetShieldPowerToggle();
		toggleSheildPostionBtn = controls.GetShieldDistributionSelector();
		toggleCoastBtn = controls.GetCoastToggle();

		shield_to_lazer_Btn =controls.GetShield2Laz();
		lazer_to_shield_Btn = controls.GetLaz2Shield();


	}
	
	// Update is called once per frame
	void Update () {

		float x  = Input.GetAxis( pitch );
		float y  = Input.GetAxis( yaw );
		float z = Input.GetAxis( roll )*-1;


		ship.RotatePhys( new Vector3(  x , y , z ) );
		ship.ThrottleAdd( Input.GetAxis( throt )*4 );
		
		HandleInput();

	}
	
	public void HandleInput(){

		//Target ship most in front
		if( Input.GetButtonDown( targetInRiticuleBtn) ){
			if( list ){
				list.TargetInReticule();
			}
		}

		//toggle lazer power
		if( Input.GetButtonDown( toggleLazerPowerBtn) ){
			ship.CannonRateToggle();
		}

		//toggle shield power
		if( Input.GetButtonDown( toggleSheildPowerBtn ) ){
			ship.ShieldRateToggle(); 
		}

		//toggle shield distribution
		if( Input.GetButtonDown( toggleSheildPostionBtn ) ){
			ship.ShieldDivisionToggle();
		}


		//transfer power
		if( Input.GetButtonDown( shield_to_lazer_Btn ) ){
			ship.ShieldCannonRateToggle();
		}
		if( Input.GetButtonDown( lazer_to_shield_Btn ) ){
			ship.ShieldCannonRateToggle();
		}

		//toggle coasting
		if( Input.GetButtonDown(toggleCoastBtn ) ){
			ship.CoastToggle();
		}

		/*
		if(Input.GetKeyDown( shield_to_laz ) ){
			ship.ShieldCannonRateToggle();
		}
		
		if(Input.GetKeyDown( laz_to_shield ) ){
			ship.ShieldCannonRateToggle();
		}
		
		if(Input.GetKeyDown( adjust_laz_rc ) ){
			ship.CannonRateToggle();
		}
		if( Input.GetKeyDown( adjust_shld_rc ) ){
			ship.ShieldRateToggle(); 
		}
		if(Input.GetKeyDown(  adjust_shld_pos ) ){
			ship.ShieldDivisionToggle();
		}
		if(Input.GetKeyDown(  coast ) ){
			ship.CoastToggle();
		}
		*/

	}
}
