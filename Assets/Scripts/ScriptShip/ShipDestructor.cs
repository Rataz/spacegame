﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SmallShipFacade))]



/**
 * 
 * Handles destruction of ship when all hp used up
 * 
 * */
public class ShipDestructor : MonoBehaviour {
		

	//auto assigned
	private SmallShipFacade shipFacade;





	//-------------------------------
	//Assign these in inspector------

	//explosion prefab to instantiate on final death
	[Tooltip("explosion prefab to instantiate on final death")]
	public GameObject explosionObj;

	//parent of main camera if camera is attached to this ship
	[Tooltip("parent of main camera if camera is attached to this ship")]
	public Transform camPos;
	//-------------------------------


	[Space(20)]
	public float explosionRadius = 5.0f;
	public float explosionDamage = 100.0f;

	private Transform cam;

	void Start(){
		shipFacade = gameObject.GetComponent<SmallShipFacade>();
		cam = Camera.main.transform;
	}

	public void Detonate()
	{
		Transform tr = transform;
		print( transform.name + " destroyed" );
		var explosionPosition = tr.position;

		Collider[] colliders = Physics.OverlapSphere(explosionPosition, explosionRadius);

		foreach (Collider hit in colliders)
		{

			var closestPoint = hit.ClosestPointOnBounds(explosionPosition);  // Calculate distance from the explosion position to the closest point on the collider
			var distance = Vector3.Distance(closestPoint, explosionPosition);
			var hitPoints = 1.0 - Mathf.Clamp01(distance / explosionRadius);   // The hit points we apply fall decrease with distance from the explosion point
			hitPoints *= explosionDamage;

			hit.BroadcastMessage("ApplyDamage", hitPoints, SendMessageOptions.DontRequireReceiver);

		}

		DetachCamera();

		if( explosionObj )
			Instantiate(explosionObj, explosionPosition, tr.rotation);

		NotifyRoster();
		NotifyGeneric();

		Destroy(gameObject);
	}


	public void NotifyRoster()
	{
		ShipRosterTeam[] rosters = GameObject.FindObjectsOfType<ShipRosterTeam>();

		int team = shipFacade.team;

		for( int i=0; i<rosters.Length; i++){
			if( rosters[i].team == team ){
				rosters[i].RemoveShip( shipFacade );
			}
		}		
	}

	public void NotifyGeneric()
	{
		GameObject x = GameObject.Find("Campeign");
		if(x != null)
		{
			string str = transform.name + " destroyed";
			x.BroadcastMessage("GameEvent", str, SendMessageOptions.DontRequireReceiver);
		}
	}


	public void DetachCamera()
	{
		if( cam.parent == transform ){
			cam.parent = null;	
		}

		if (cam.parent == camPos)
		{
			cam.parent = null;
		}

	}


}
