﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ship_control_1 : MonoBehaviour {

	//left stick
	public string throttle_up = "w";
	public string throttle_down = "s";
	public string rudder_left = "a";
	public string rudder_right = "d";

	//right stick
	public string elevator_up = "u";
	public string elevator_down = "j";
	public string roll_left = "h";
	public string roll_right = "k";

	// keys
	public string transfer_s_to_l = "n";
	public string transfer_l_to_s = "m";
	public string adjust_can_rc = "r";
	//public string adjust_eng_rc = "t";
	public string adjust_shld_rc = "y";
	public string adjust_shld_pos = "b";

	//general
	private Rigidbody my_rigidbody ;
	public float control_multiplier;
	public float throttle_multiplier;

	[Range(0.0f, 100.0f)]
	public float throttle ;


	//ship power levels stuff
	//public int power_bank_;

	[Range(0, 4)]
	public int shield_rc ;

	[Range(0, 4)]
	public int cannon_rc ;

	[Range(0, 8)]
	public int engine_rc;

	private float throttle_multiplier_rc;

	public float can_pow;
	public float eng_pow;
	public float shld_pow;
	public float redirect_amount;
	public float shld_f_pow;
	public float shld_b_pow;
	[Range(0, 2)]
	public int shld_pos; //0 is full back, 1 is even and 2 is full forward
	public float shld_overflow;
	//[Range(0, 6)]
	//public int power_bank;
	public float can_power_cap;
	public float shld_pow_cap;
	public float power_drain_x;

	//systems (damage/low power on/off stuff)
	public bool cannons_online;
	public bool shields_online;
	public bool engine_online;
	//public bool flight_controls_online;

	//GUI variables
	public Slider engineSlider;
	public Slider shield_f_Slider;
	public Slider shield_b_Slider;
	public Slider lazerSlider;
	public Slider throttleSlider;

	public Slider shieldRCslider;
	public Slider lazerRCslider;
	public Slider engineRCslider;

	void Start(){
		my_rigidbody = transform.GetComponent<Rigidbody>();
	}

	void FixedUpdate () {

		/*	if(Input.GetKey("t")){
				transform.GetComponent<Rigidbody>().angularDrag = sas_force;
				energy = energy-0.5f;
			}
			else{
					transform.GetComponent<Rigidbody>().angularDrag = 0;
			}
		*/

		//power systems stuff

		engine_rc = 8 - shield_rc - cannon_rc;

		if(Input.GetKeyUp(adjust_can_rc))
		{
			if(cannon_rc != 4)
			{
				cannon_rc = cannon_rc+1;
			}
			else{cannon_rc = 0;}
		}

		if(Input.GetKeyUp(adjust_shld_rc))
		{
			if(shield_rc != 4)
			{
				shield_rc = shield_rc+1;
			}
			else{shield_rc = 0;}
		}

		if(Input.GetKeyUp(adjust_shld_pos))
		{
			if(shld_pos != 2)
			{
				shld_pos = shld_pos+1;
			}
			else{shld_pos = 0;}
		}




		if(cannon_rc > 2)
			can_pow = can_pow+(cannon_rc+1)*4f;
		
		if(cannon_rc < 2)
			can_pow = can_pow-(cannon_rc+1)*2.5f;
		//can_pow = can_pow+cannon_rc*5;
		if(can_pow>can_power_cap){can_pow=can_power_cap;}

		if(can_pow-0.5f <=0f){
			cannons_online = false;
		}
		else {cannons_online = true;}



		if(shield_rc > 2)
			shld_pow = shld_pow+(shield_rc+1);

		if(shield_rc < 2)
			shld_pow = shld_pow-(shield_rc+1)*0.5f;

		if(shld_pow>shld_pow_cap){shld_pow=shld_pow_cap;}

		if(shld_pow-0.5f <=0f){
			shields_online = false;
		}
		else {shields_online = true;}

		
		//eng_pow = eng_pow+engine_rc*5-throttle*power_drain_x;
		//if(eng_pow>power_cap){eng_pow=power_cap;}
	//	if(eng_pow-0.5f <=0f){
			//throttle = 0;
	//		engine_online = false;
		//	eng_pow = eng_pow+engine_rc*5;
	//	}
		//else 
	//	{
	//		engine_online = true;
			throttle_multiplier_rc = engine_rc-3;
		
		my_rigidbody.AddRelativeForce(new Vector3(0,0,throttle*throttle_multiplier*throttle_multiplier_rc));
			//eng_pow = eng_pow+engine_rc*5-throttle*power_drain_x;
	//	}

		//power redirecting
		if(Input.GetKeyDown(transfer_l_to_s) && can_pow-redirect_amount>=0)
		{
			can_pow = can_pow-redirect_amount;
			shld_pow = shld_pow+redirect_amount/10;
		}
		if(Input.GetKeyDown(transfer_s_to_l) && shld_pow-redirect_amount/10>=0)
		{
			can_pow = can_pow+redirect_amount;
			shld_pow = shld_pow-redirect_amount/10;
		}


		//shield positioning
		if(shld_pow > 200)
		{
			shld_overflow = shld_pow-200;
		}
		else{shld_overflow = 0;}

		if(shld_pos == 1)
		{
			shld_f_pow = shld_pow/2;
			shld_b_pow = shld_pow/2;
		}
		if(shld_pos == 0)
		{
			shld_f_pow = shld_overflow;
			if(shld_pow > 200)
				shld_b_pow = 200;
			
			else{shld_b_pow = shld_pow;}

		}
		if(shld_pos == 2)
		{
			shld_b_pow = shld_overflow;

			if(shld_pow > 200)
				shld_f_pow = 200;

			else{shld_f_pow = shld_pow;}
		}




		//new flight control systems
		if(Input.GetKey(throttle_up)){
			throttle = throttle+1;
		}
		else if(Input.GetKey(throttle_down)){
			throttle = throttle-1;
		}

		throttle = Mathf.Clamp(throttle,0,100);

		if(Input.GetKey(elevator_up)){
			my_rigidbody.AddRelativeTorque( new Vector3(3,0,0) );
		}
		else if(Input.GetKey(elevator_down)){
			my_rigidbody.AddRelativeTorque(new Vector3(-3,0,0));
		}

		if(Input.GetKey(rudder_left)){
			my_rigidbody.AddRelativeTorque(new Vector3(0,-3,0));
		}
		else if(Input.GetKey(rudder_right)){
			my_rigidbody.AddRelativeTorque(new Vector3(0,3,0));
		}

		if(Input.GetKey(roll_left)){
			my_rigidbody.AddRelativeTorque(new Vector3(0,0,3));
		}
		else if(Input.GetKey(roll_right)){
			my_rigidbody.AddRelativeTorque(new Vector3(0,0,-3));
		}

		//my_rigidbody.AddRelativeForce(new Vector3(0,0,throttle*throttle_multiplier));
		//my_rigidbody.AddRelativeTorque(new Vector3());

		//GUI

		if( engineSlider )
			engineSlider.value = eng_pow;

		if( shield_f_Slider )
			shield_f_Slider.value = shld_f_pow;

		if( shield_b_Slider )
			shield_b_Slider.value = shld_b_pow;

		if( lazerSlider )
			lazerSlider.value = can_pow;

		if( throttleSlider )
			throttleSlider.value = throttle;

		if( shieldRCslider )
			shieldRCslider.value = shield_rc;

		if( lazerRCslider )
			lazerRCslider.value = cannon_rc;

		if( engineRCslider )
			engineRCslider.value = engine_rc ;


	}




}