﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;
using UnityEngine.UI;

//targetting crosshairs
//keeps list of ships
//respawns AI
//scoreboard

public class ShipsList : MonoBehaviour {

	public List<SpaceAItest1> shipsAI;

	public SpaceAItest1 ship ;

	public string targettedTag = "target" ;

	public Texture crosshairTex;
	public Texture crosshairTex2;
	public float scale = 1.0f;

	private int selected = 0;
	private Transform playership;

	public float sphere = 5; //range of spawning


	public VictoryDetector victoryD;
	public GameManage gameManage;



//	int playerteam = 1;



	public void SetPlayerShip( Transform ship ){
		playership = ship;
	}

		

	public GameObject Spawn(int team, GameObject man){

		Vector3 spot = Random.insideUnitSphere * sphere + transform.position;
		GameObject bob	= Instantiate (man, spot, transform.rotation) as GameObject;

		//Instantiate (man, spot, transform.rotation);
		if( bob.GetComponent<SpaceAItest1>() ){
			
			SpaceAItest1 t = bob.GetComponent<SpaceAItest1>();
			
			if( team == 2 ){//Republic
				t.teamTag = "EnemyTeam2";
				t.teamTagEnemy = "EnemyTeam1";
				t.SetTeamTag("EnemyTeam2");
			}
			
			if( team == 1 ){//CIS
				t.teamTag = "EnemyTeam1";
				t.teamTagEnemy = "EnemyTeam2";
				t.SetTeamTag("EnemyTeam1");
			}

		}

		//Decrease Reinforcement counts
		if( gameManage ){

			//stored player selection
			select_persist persistent = GameObject.FindObjectOfType<select_persist>();

			if( persistent ){
				if( persistent.team == 0 ){ //player's team index
					if( team==1 )
						gameManage.AddReinforceNPC( -1 );
					if( team==2 )
						gameManage.AddReinforcePlayer( -1 );
				}
				else if( persistent.team == 1 ){
					if( team==2 )
						gameManage.AddReinforceNPC( -1 );
					if( team==1 )
						gameManage.AddReinforcePlayer( -1 );
					}
			}
			else{//default

				if( team==2 )//republic
					gameManage.AddReinforceNPC( -1 );
				if( team==1 )//CIS
					gameManage.AddReinforcePlayer( -1 );

			}
		}

		return  man;

	}

	public float alpha=0.75f;
	private Color guiColor;


	void Start(){
		RefreshShips(  );

		//object storing players selections
		select_persist persistent = GameObject.FindObjectOfType<select_persist>();

		if( persistent ){
//			playerteam = persistent.team;
		}

	}

	void Update () {

		if( shipsAI.Count > 0 ){

			//Switch Radar target by changing tags
			if( Input.GetKeyDown( KeyCode.V ) ) {

				SpaceAItest1 x = shipsAI[ selected ];

				if( x != null ){
					x.RevertTag();

					selected ++;

					if( selected > shipsAI.Count-1 ){
						selected = 0;
					}

					x.ChangeTag( targettedTag );

					playerTarget = x.transform;
				}

			}

			//Switch Riticule Target
			//if( Input.GetKeyDown( KeyCode.G ) ) {
			//	TargetInReticule();
			//}

		}

	}

	public Transform playerTarget;

	//target ship that is closest to fwd
	//fwd of playership
	public void TargetInReticule(  ){

		float closest = Mathf.Infinity;

		Transform newTar;

		for( int i=0; i<shipsAI.Count; i++ ){
			//if( Vector3.Angle( playership.forward, 
			if( shipsAI[i] != null ){
				Vector3 pos = shipsAI[i].transform.position;
				float ang = Vector3.Angle( playership.forward,  pos - playership.position  );

				if( ang<closest ){
					newTar = shipsAI[i].transform;
					closest = ang;
					playerTarget = newTar;
				}
			}
		}

		/*ic bool CheckFov( Vector3 pos , float angle ){
		float ang = Vector3.Angle(   pos - tr.position , tr.forward );
		bool ret = false;
		if( ang < angle ){
			ret = true;
		}*/
	}

	public bool drawAllMarkers = true;

	void OnGUI(){
		if( shipsAI.Count > 0 ){
			if( crosshairTex && (playerTarget!=null) ){

				//Transform tar = shipsAI[ selected ].transform;

				Transform tar = playerTarget;
				Vector3 fwd =  playership.forward;
				Vector3 ppos = playership.position;
				float dot = Vector3.Dot( tar.position - ppos , fwd );

				guiColor = Color.white;
				guiColor.a = 1;

				if( dot>0 ){ //target is in front
					GUI.DrawTexture(new Rect(Camera.main.WorldToScreenPoint( tar.position ).x- crosshairTex.width/2 * scale,
	                         Screen.height-(Camera.main.WorldToScreenPoint( tar.position ).y)- crosshairTex.height/2 * scale,
	                         crosshairTex.width*scale,crosshairTex.height*scale),crosshairTex);

					Vector3 bob = TargettingUtils.CalculateLead( tar , playership , 300 );

					GUI.DrawTexture(new Rect(Camera.main.WorldToScreenPoint( bob ).x- crosshairTex.width/2 * scale,
	                         Screen.height-(Camera.main.WorldToScreenPoint( bob ).y)- crosshairTex.height/2 * scale,
	                         crosshairTex.width*scale,crosshairTex.height*scale),crosshairTex2);
				}

				if( drawAllMarkers ){
					guiColor = Color.green;
					guiColor.a = alpha;
					for( int i=0; i<shipsAI.Count; i++){

						SpaceAItest1 h = shipsAI[i];

						if( h!=null ){
							Transform othership = h.transform;
							Vector3 po = othership.position;
							float dot2 = Vector3.Dot( po - ppos , fwd );

							if( dot2>0 ){
								GUI.DrawTexture(new Rect(Camera.main.WorldToScreenPoint( po ).x- crosshairTex.width/2 * scale,
								                         Screen.height-(Camera.main.WorldToScreenPoint( po ).y)- crosshairTex.height/2 * scale,
								                         crosshairTex.width*scale,crosshairTex.height*scale),crosshairTex);
							}

						}
					}
				}

			}
		}
	}

	public GameObject ship1;
	public GameObject ship2;

	public void ShipDestroyed( int team , int type ){

		if( type == 1 )
			Spawn ( team , ship1 );

		if( type == 2 )
			Spawn ( team , ship2 );

		RefreshShips();
	}

	public void RefreshShips(  ){
		SpaceAItest1[] t = GameObject.FindObjectsOfType<SpaceAItest1>();
		shipsAI.Clear();
		shipsAI.AddRange( t );
	}


	public void RemoveShip( SpaceAItest1 ship ){

	}

	public void AddShip(  SpaceAItest1 ship ){

	}

	public Color colour = Color.magenta;
	void OnDrawGizmosSelected() {
		Gizmos.color = colour;
		Gizmos.DrawWireSphere(transform.position, sphere );//spawning area
	}

}
