﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


/*
 * 
 * Put this on an empty in the scene
 * 
 * Gives player control of an AI ship
 * 
 * */
public class PlayerShipHijacker : MonoBehaviour {

	[Tooltip("Assign this to roster of player's team")]
	public ShipRosterTeam roster;

	

	void Update () {
		if( Input.GetKeyUp( KeyCode.P )){
			getNewShip();
		}
	}

	public void getNewShip(){
		roster.GivePlayerAShip();
	}



}
