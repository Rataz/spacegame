﻿using UnityEngine;
using System.Collections;

public class ParentTo : MonoBehaviour {

	public Transform parentToMe;

	public bool matchRotation = false;
	public bool matchPosition = false;

	void Awake () {

		if( parentToMe )
			transform.parent = parentToMe;

		if( matchPosition ){
			transform.localPosition = Vector3.zero;
		}

		if( matchRotation ){
			transform.localRotation = Quaternion.identity;
		}

	}

}
