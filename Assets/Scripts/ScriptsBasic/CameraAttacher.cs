﻿using UnityEngine;
using System.Collections;

public class CameraAttacher : MonoBehaviour {

	public void DeAttach(){
		transform.parent = null;
	}

	public void AttachTo( Transform tar ){

		transform.parent = tar;
		transform.localPosition = Vector3.zero;
		transform.localRotation = Quaternion.identity;

	}



}
