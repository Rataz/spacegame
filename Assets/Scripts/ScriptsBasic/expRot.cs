﻿using UnityEngine;
using System.Collections;

public class expRot : MonoBehaviour {

	public Transform target;

	public Transform target2;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		Vector3 relativePos = target.position - transform.position;
		Quaternion rotation = Quaternion.LookRotation(relativePos , target.forward );
		transform.rotation = rotation;

	}
}
