﻿using UnityEngine;
using System.Collections;

//Freeze or unfreeze time
public class LevelPauser : MonoBehaviour {


	public void Pause(){
		
		Time.timeScale = 0.0000001f;
	}

	public void UnPause(){
		Time.timeScale = 1.0f;
	}

}
