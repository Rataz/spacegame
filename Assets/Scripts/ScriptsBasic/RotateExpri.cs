﻿using UnityEngine;
using System.Collections;

public class RotateExpri : MonoBehaviour {

	public Transform target;

	public	Vector3 to;

	public float rot = 60;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		BanknYank2();
	}


	private void BanknYank2(){
		Vector3 tar = transform.InverseTransformPoint( target.position );

		if(tar.x>2){
			transform.Rotate( 0, 0, -rot* Time.deltaTime  ) ;
		}
		else if( tar.x<-2 ){
			transform.Rotate( 0 , 0 , rot* Time.deltaTime );
		}
		if(tar.x>0.1){
			transform.Rotate(0, 10* Time.deltaTime, 0  ) ;
		}
		else if( tar.x<-0.1){
			transform.Rotate( 0 , -10* Time.deltaTime , 0 );
		}		
		else if(tar.y>0.1){
			transform.Rotate( -rot* Time.deltaTime , 0 , 0 );
		}
		else if( tar.y<-0.1){
			transform.Rotate( rot* Time.deltaTime , 0 , 0 );
		}


	}
	
	private void BanknYank(){
		Vector3 targetRelative = transform.InverseTransformPoint( target.position );

		float roll = Mathf.Atan( targetRelative.y / targetRelative.x ) * Mathf.Rad2Deg;
//		float pitch = Mathf.Atan( targetRelative.y / targetRelative.z )* Mathf.Rad2Deg;

		to = new Vector3( 0, 0, roll );

		if (Vector3.Distance(transform.eulerAngles, to) > 0.01f)  {
			transform.eulerAngles = Vector3.Lerp( transform.rotation.eulerAngles, to, Time.deltaTime);
		}
		else {
			transform.eulerAngles = to;
		}

	}

}
