﻿using UnityEngine;
using System.Collections;

public class DontRenderMe : MonoBehaviour {

	void Start(){
		
		Renderer ren = gameObject.GetComponent<Renderer>();
		
		if(ren)
			ren.enabled=false;
		
	}
}
