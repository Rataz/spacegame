﻿using UnityEngine;
using System.Collections;

public class CamSwitcher : MonoBehaviour {

	public Camera cam0 ;
	public Camera cam1 ; 

	private int tog = 0 ;

	void Update () {

		if( Input.GetKeyDown( KeyCode.O )) {

			tog++ ;

			if( tog > 1){
				tog = 0;
			}
			
			if( tog == 1 ){
				cam0.enabled = false;
				cam1.enabled = true;
			}
			else{
				cam0.enabled = true;
				cam1.enabled = false;
			}


		}

	}


}
