﻿using UnityEngine;
using System.Collections;

public class DisableInGame : MonoBehaviour {

	// Use this for initialization
	void Start () {
		gameObject.SetActive(false);
	}

}
