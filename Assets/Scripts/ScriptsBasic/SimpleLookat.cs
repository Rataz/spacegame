﻿using UnityEngine;
using System.Collections;

public class SimpleLookat : MonoBehaviour {

	public Transform target;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(target)
			transform.LookAt(target);
	}
}
