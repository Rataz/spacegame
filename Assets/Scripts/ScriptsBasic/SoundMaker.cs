﻿using UnityEngine;
using System.Collections;

public class SoundMaker : MonoBehaviour {

	public AudioSource source;

	public void Use(){
		source.Play();
	}

}
