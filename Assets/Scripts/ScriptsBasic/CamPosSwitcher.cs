﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CamPosSwitcher : MonoBehaviour {
	
	public List < Transform > positions;

	public int indx = 0;


	public void NextPos(){

		indx++;

		if( indx > positions.Count-1 )
			indx = 0;

		transform.position = positions[ indx ].position;
		transform.rotation = positions[ indx ].rotation;

	}

	public void PrevPos(){

		indx--;
		
		if( indx < 0 )
			indx = positions.Count-1;

		transform.position = positions[ indx ].position;
		transform.rotation = positions[ indx ].rotation;

	}

}
