﻿using UnityEngine;
using System.Collections;

public class LookAtMouseFlat : MonoBehaviour {

	public GameObject particle;
	public float rSpeed = 1.0f; // Scale. Speed of the movement 



	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		Vector3 MousePosition = Input.mousePosition; 
		
		MousePosition.x = (Screen.height/2) - Input.mousePosition.y;
		
		MousePosition.y = -(Screen.width/2) + Input.mousePosition.x;
		
		transform.Rotate(MousePosition * Time.deltaTime * rSpeed, Space.Self);

		/*
		if (Input.GetButtonDown("Fire1")) {
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			if (Physics.Raycast(ray))
				Instantiate(particle, ray.transform.position, ray.transform.rotation);

			
		}
		*/
	}
}

/*var rSpeed = 1.0; // Scale. Speed of the movement 

 

function FixedUpdate () 

{ 

    MousePosition = Input.mousePosition; 

    MousePosition.x = (Screen.height/2) - Input.mousePosition.y;

    MousePosition.y = -(Screen.width/2) + Input.mousePosition.x;

    transform.Rotate(MousePosition * Time.deltaTime * rSpeed, Space.Self);

 

}*/
