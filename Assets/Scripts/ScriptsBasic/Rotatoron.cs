﻿using UnityEngine;
using System.Collections;

public class Rotatoron : MonoBehaviour {

	public float rotx;
	public float roty;
	public float rotz;

	public float multiplier = 10f;

	void Update () {

		transform.Rotate( rotx*multiplier*Time.deltaTime , roty*multiplier*Time.deltaTime  , rotz*multiplier*Time.deltaTime );

	}
}
