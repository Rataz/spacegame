﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Remove hierarchy pane clutter during runtime.  Disabling things only greys them out
//Deletes things without ruining the carefully built scene


public class StartupDeleter : MonoBehaviour {

	public List<GameObject> deleteThem;


	void Start () {
	
		foreach(GameObject x in deleteThem){
			Destroy(x);
		}

	}

}
