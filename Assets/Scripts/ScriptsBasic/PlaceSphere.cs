﻿using UnityEngine;
using System.Collections;

//place a sphere in front of the camera
public class PlaceSphere : MonoBehaviour {

	public Transform sphere;

	private Transform cam;

	// Use this for initialization
	void Start () {
		
		CreateZone();

		cam = Camera.main.transform;
	}
	
	// Update is called once per frame
	void Update () {

		if( Input.GetKey( KeyCode.T ) ){
			sphere.position = cam.TransformPoint( new Vector3( 0,0,100) );
		}

	}

	void CreateZone(){
		GameObject spheree = GameObject.CreatePrimitive(PrimitiveType.Sphere);

		sphere = spheree.transform;
	}

}
